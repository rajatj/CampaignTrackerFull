
angular.module('myApp', ['ui.bootstrap','staticVars' ]).controller('namesCtrl',function($scope, $http, $filter, $modal,$rootScope,$window,$location,$timeout,CONFIG) {
	
	//$scope.fnName='data1.json';
	$rootScope.apiUrl= CONFIG.apiUrl;/* 'http://10.97.85.76:8090/api/'; */
	$rootScope.fnName='jsondata/estimate';
	$scope.CampaignLoadUrl=$rootScope.apiUrl+$rootScope.fnName;
	
	$rootScope.errorDisplay=function(data)
	{
		$timeout(function()
							{
							if (data.status == '500')
								ohSnap(data.responseText,'blue');
						else if (data.status == '404')
								ohSnap('API not responding','red');
						else
								ohSnap('Sorry! Please Try Again Later','red');
							},500);
	}
	
	
	/* //Code for Calculating No of width
	setTimeout(function() {
		$scope.element=document.querySelector('.side-border').offsetWidth;
		console.log("External: "+$scope.element);
		$scope.flipperElem=document.querySelector('.side-border .col-md-3').offsetWidth;
	
		console.log("Flipper: "+$scope.flipperElem);
		$scope.varWidth=parseInt($scope.element/$scope.flipperElem);
		console.log("Count of Flipper: "+$scope.varWidth);
		$scope.tilesNo=$scope.varWidth-1;
		console.log($scope.tilesNo);
	},500);
	 */
	$rootScope.printIt = function()
	{
		//console.log( CONFIG.parent_id);
			//$http.get('http://10.97.85.76:8090/api/jsondata/estimate')
		//	$http.get($scope.CampaignLoadUrl)
		var tokenKey = "accessToken";
			                $scope.token = $window.localStorage.getItem(tokenKey);
	                $rootScope.headers = {};
					$rootScope.UserName=$window.localStorage.getItem("CTuser");
	
	if ($scope.token) {
                     $rootScope.headers.Authorization =  $scope.token;
                }
				else 
				{
				$window.location.href= '../index.html';
				return false;
				}
				
				
		$http({
  url:$scope.CampaignLoadUrl,
  method: 'GET',
   //isArray: false,
  headers: $rootScope.headers
  }).success(function (response)
		{
				console.log($scope.CampaignLoadUrl);
			$scope.names = response;
			$scope.CheckBurndown=function(y)
			{
					if($rootScope.fnName=='jsondata/development'&& y.burndown==false)
					{
						return true;
					}
					if($rootScope.fnName=='jsondata/live' && y.SiteDown == true)
					{
						return true;
					}
			}
						
			$scope.StartDatefunc=function(y)
			{
				$scope.StartDateF=y.development_start_date;
				$scope.StartDateF=$filter('date')($scope.StartDateF, "yyyy-MM-dd");
				return $scope.StartDateF; 				
			}
			
			$scope.LaunchDatefunc=function(y)
			{
				$scope.LaunchDateF=y.launch_date;
				$scope.LaunchDateF=$filter('date')($scope.LaunchDateF, "yyyy-MM-dd");
				return $scope.LaunchDateF; 				
			}
			
			$rootScope.DisplayStartDate=true;
			
	
	if($rootScope.fnName=='jsondata/estimate')
	{
		$rootScope.DisplayStartDate=false;
		$rootScope.ShowEstimateFields=true;
	}
	else
	{
		$rootScope.DisplayStartDate=true;
		$rootScope.ShowEstimateFields=false;
	}
	
			var counter = 4;
			$scope.data = {};
			for(var i=0; i<=$scope.names.length; i++)
				{
					$scope.data[i] = counter;
					if(i % 5 == 4)
					{
					counter = counter + 5;
					}
				}		
			$scope.type=function(y,index)
			{
				/* if(y.burndown==false && y.status!="Live" || y.SiteDown==false)
					return 'danger';
				else
					return 'success'; */
				if($rootScope.fnName=='jsondata/development' && y.burndown == false)
					{
						return 'danger';
					}
						
					else if($rootScope.fnName=='jsondata/live' && y.SiteDown == true)
					{
						return 'danger';
					}
					else
						return 'success';
				
				
			};
		}).error(function(response){
				$rootScope.displayOverlay=false;				
				$rootScope.errorDisplay(response);
			});
			
	};	
		
	$rootScope.printIt();
	//console.log(config.apiUrl);
	/* $scope.itemsPerPage = 10;
	$scope.currentPage = 1;
	$scope.pageSize = 10;
		$scope.names.$promise.then(function () {
    //$scope.totalItems = $scope.friends.length;
    $scope.$watch('currentPage + itemsPerPage', function() {
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage),
        end = begin + $scope.itemsPerPage;

      $scope.filteredFriends = $scope.names.slice(begin, end);
    });
  }); */
	
	
				$rootScope.Logout=function()
				{
							var tokenKey = "accessToken";
							var param="";
		
					var req = {
					method: 'POST',
					url: $rootScope.apiUrl+'Accounts/logout',
					dataType: 'json',
					headers: $rootScope.headers,
					data:param
					}

					$http(req).success(function()
						{
							$window.localStorage.clear();
							$window.location.href= '../index.html';
						}).error(function(data)
						{
								$window.localStorage.setItem(tokenKey, "");
						});
				}			

			$scope.cancelTest = function()
			{
				var modalInstance = $modal.open(
				{
				animation: $scope.animationsEnabled,
				templateUrl: 'ActionModalContent',
				controller: 'ModalInstanceCtrl'
				});
			}
			
			
			$scope.animationsEnabled = true; 
			
			$http.get('images.json')
			.success(function (response)
		{
				$scope.imgname = response;
		});
			
		 $scope.openPopup = function () 
		 {

				var modalInstance = $modal.open(
				{
				animation: $scope.animationsEnabled,
				templateUrl: 'RequestModalContent',
				backdrop : 'static',
				controller: 'RequestModalInstanceCtrl',
				resolve: 
							{
									items: function ()
									{
										return $scope.imgname;
									}					
							}
				});
		};

	
		
		$scope.detailSection= function (y,index,size)
		{
			$scope.jsonObj=y;
			$rootScope.displayOverlay=true;
			//$http.get('http://10.97.85.76:8090/api/issue/'+$scope.jsonObj.id)
			
			$http({
  url:$rootScope.apiUrl+'issue/'+ $scope.jsonObj.id,
  method: 'GET',
  headers: $rootScope.headers
  }).success(function (response)
			{
				$rootScope.displayOverlay=false;
					$scope.TrackerData = response;
					var modalInstance = $modal.open(
				{
					
				animation: $scope.animationsEnabled,
				templateUrl: 'DetailModalContent',
				backdrop : 'static',
				controller: 'DetailModalInstanceCtrl',
				size:size,
				//windowClass: 'center-modal',
						resolve: 
							{
									items: function ()
									{
										return y;
									},
									value:function ()
									{
										return $scope.TrackerData;
									}
							}	
			
				});	
			}).error(function(response){
				$rootScope.displayOverlay=false;
				//alert("There is Some Problem in API");				
				$rootScope.errorDisplay(response);
			});
			
		};
		
		$scope.refreshScreen=function()
		{
			/* $window.location.reload(); */
			$rootScope.printIt();
		}
	
		$scope.orderNo='name';
		$scope.class="act";
		
		$scope.orderAssign=function(value,file)
		{
			$rootScope.fnName = file;
			$scope.CampaignLoadUrl=$rootScope.apiUrl+$rootScope.fnName;
			//console.log($scope.CampaignLoadUrl);
			$scope.printIt();
			//console.log($scope.fnName);
			var order=value;
			$scope.orderNo=order;
			if ($scope.class === "act")
            $scope.class = "active";
         /* else
            $scope.class = "act"; */
			$scope.myVar = true;
			$scope.displayThis = 0;

		}	
		
		 $rootScope.PlatformList=[
        {id:1, name:'Static'},
         {id:2, name:'PimCore'},
		  {id:3, name:'SiteCore'}
    ];
		 
    /*   $scope.$watch("search", function(query){
        $scope.counted = $filter("filter")($scope.names, query).length;
      }); */
	  
		$scope.myVar = true;
		$scope.slideUp=function()
		{
			$scope.displayThis=0;			
		}
		
		$scope.sldeUp=function()
		{
			$scope.myVar = true;
			
		}
	
		$scope.toggle = function(y,index) 
		{
			/*
			if(index<3) {
				console.log(index);
				$scope.displayThis = 3;
			}else{
				$scope.displayThis = 0;
			}
			*/
			$scope.myVar = true;
			$scope.displayThis = $scope.data[index];
			
			$scope.arrowLoc=index%5;
 			//console.log($scope.arrowLoc); 
			
			/* if(index==$scope.names.length-1)
			{
			console.log(index);
			console.log($scope.names.length-1);
			$scope.myVar = false;
			$scope.displayThis=0;
			
			}
			else
			{
				$scope.myVar = true;
			}  */
	
			if($scope.names.length%5==0)
			{
				$scope.myVar = true;
			}

			if($scope.names.length%5>0)
			{
				$scope.iNo=$scope.names.length%5;
				for(i=1;i<=$scope.iNo;i++)
				{
					if(index==$scope.names.length-i)
					{
							$scope.myVar = false;
							$scope.displayThis=0;
					}
				}
			
			}
				
				 $scope.counted = $scope.names.length; 
      $scope.$watch("search", function(query){
        $scope.counted = $filter("filter")($scope.names, query).length;
		$scope.showHidden= $scope.counted;
		$scope.myVar = true;
		if($scope.showHidden%5>0)
			{
				$scope.indNo=$scope.showHidden%5;
				for(i=1;i<=$scope.indNo;i++)
				{
					if(index==$scope.showHidden-i)
					{
							$scope.myVar = true;
							$scope.myVar = false;
							$scope.displayThis=0;
					}
				}
			
			}

      });
				
			$scope.proj=y.name;
			$scope.prod=y.producer_name;
			$scope.status=y.status;
			$scope.plat=y.hosting_platform;
			$scope.date=y.launch_date;
			$scope.mrkt=y.market;
			
		}
				        		
});
 
/* Not Linked to ng-repeat */

angular.module('myApp') .directive('numbersOnly', function(){
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
       modelCtrl.$parsers.push(function (inputValue) {
           /* this next if is necessary for when using ng-required on your input. 
           In such cases, when a letter is typed first, this parser will be called
           again, and the 2nd time, the value will be undefined */
           if (inputValue == undefined) return '' 
           var transformedInput = inputValue.replace(/[^0-9]/g,''); 
		   element.bind('keypress', function(event) {
        if(event.keyCode === 32)
			{
				event.preventDefault();
			}
      });	
           if (transformedInput!=inputValue) {
              modelCtrl.$setViewValue(transformedInput);
              modelCtrl.$render();
           }    
 		   
           return transformedInput; 
	   
       });
     }
   };

}) .controller('RequestModalInstanceCtrl', function ($scope, $modalInstance, $modal, items, $http, $filter, $timeout,$rootScope,$window,CONFIG)
 {
	//console.log($rootScope.apiUrl);
	 $scope.JsonImage=items;
	 $scope.CampaignImage="";
	 //console.log($scope.JsonImage);
	 $scope.CampaignImage=$scope.JsonImage.name;
	 $scope.NewCampaignUrl=$rootScope.apiUrl+'project';
	 $scope.New_Platform="";
	
	$scope.MarketList=[
        {id:1, name:'Europe'},
         {id:2, name:'Asia'}
    ];
	
	$scope.ProducerList=[
        "Jeanett Buch",
        "Mia Jappe"
    ];
	
	$scope.clickme=function()
	{
		console.log($scope.New_Platform);
	};
	
	$scope.clickmarket=function()
	{
		console.log($scope.marketListSelected);
	};
	
	$scope.formats = ['yyyy-MM-dd','dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
	$scope.DisableDate=true;
	$scope.minDate = new Date();
	$scope.minduDate=new Date();
	$scope.minDate=$scope.minDate.setDate($scope.minduDate.getDate()+1);
	 $scope.minDueDate = $scope.minDueDate ? null : new Date();
	
	
	$scope.openCal=function($event)
	{
		$event.preventDefault();
		$event.stopPropagation();
		$scope.OpenDs=true;
		$scope.openIt = false;
		$scope.openThis=false;
	};
	
	 $scope.open = function($event)
	 {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.openIt = true;
		$scope.openThis=false;
		$scope.OpenDs=false;
		//console.log($scope.minDate);
	 };
	 
	 
	  $scope.EnableExitDate=function()
	  {
		  $scope.DisableDate=false;
		  $scope.act=new Date();
		  $scope.actexit=new Date();
		  $scope.maxDueD=new Date($scope.Prod_Date);
		  $scope.maxDueDate=$scope.maxDueD.setDate( $scope.maxDueD.getDate()-1);
		  if($scope.act.getDate()>=$scope.actexit.getDate())
	{
		//console.log('hi');
		$scope.Exit_Date='';
		
	}
	  }
	  
	  $scope.enablLaunchDate=function()
	  {
		  $scope.minLaunch=new Date($scope.Due_Date);
		  $scope.minDate=$scope.minLaunch.setDate($scope.minLaunch.getDate()+1);	
		  $scope.FDueDate=$filter('date')($scope.Due_Date, "yyyy-MM-dd");		  
	  }
	  
  $scope.opened = function($event)
  {
    
	$event.preventDefault();
    $event.stopPropagation();
	$scope.openIt=false;
    $scope.openThis = true;
	$scope.OpenDs=false;
	//console.log($scope.Prod_Date.getDate()+1);
	$scope.ExitMinDateee= new Date();
	$scope.act=new Date($scope.Prod_Date);
	console.log('Prod_Date : '+$scope.Prod_Date);
	$scope.ExitMinDatee=$scope.act.setDate($scope.act.getDate()+1);
	$scope.ExitMinDate=$scope.ExitMinDatee;
	$scope.actexit=new Date($scope.ExitMinDate);
	console.log($scope.Exit_Date);
	
	//console.log(new Date($scope.ExitMinDatee));
  };
	 
	 //$scope.ExitMinDate=new Date($scope.ExitMinDatee);
		var currentDate =  new Date();
	  //$scope.ExitMinDate= new Date();
	 $scope.ExitMinDate= currentDate.setDate(currentDate.getDate()+1);
	 
	 console.log($scope.NewCampaignUrl)
	 	$scope.obj = {};

	$scope.datestring=new Date();
		
	$scope.todayDate=$filter('date')($scope.datestring, "yyyy-MM-dd"); 
	//change format which is acceptable in min field of datepicker.
	
	$scope.genCampaign= function()
 {
	 $modalInstance.dismiss();
	 $rootScope.displayOverlay=true;
	 
	 
	 /* if($scope.camp_name.length==0)
	 {
		 alert("fill it");
	 } */
	 
	 $scope.obj.name=$scope.camp_name.replace(/\s\s+/g, " ");
	 $scope.obj.response_put= CONFIG.response_put_f;
	 var i= Math.floor((Math.random() * 100000) + 1);
 // var identifier = $scope.camp_name.replace(/ /g,"")+'-'+i;
	var identifier =$scope.obj.name.replace(/ /g,"")+'-'+i;
  $scope.obj.identifier =identifier.toLowerCase();
  i++;
  $scope.obj.description=$scope.proj_description;//.replace(/\s\s+/g, " ");
	 var todaysdate=new Date();
	 $scope.obj.created_on=todaysdate;
	 $scope.response_put=CONFIG.response_put_f;
	 $scope.FDueDate=$filter('date')($scope.Due_Date, "yyyy-MM-dd");
	  $scope.FProd_Date=$filter('date')($scope.Prod_Date, "yyyy-MM-dd");	//Working
	  	  
	 // $scope.obj.Exit_Date=$filter('date')($scope.Exit_Date, "yyyy-MM-dd");
	    $scope.Exit_Date=$filter('date')($scope.Exit_Date, "yyyy-MM-dd");
		
 var customlist=[/* objCustom,objCust,objCust1,objCust2,objCust3,objCust4,objCust5,objCust6 */
 
	{
		  "id" :2 , 
		  "name": "Launch Date" ,
		  "value":$scope.FProd_Date
    },

	{
		"id": 3,
		"name" : "Estimates Shared with Client",
		"value":$scope.New_Estimate
	},
  
	{
		"id" : 29,
		"name" : "Market",
		"value":$scope.marketListSelected.replace(/\s\s+/g, " ")
	},
  
	{
			"id" : 17,
			"name" : "Project Technology",
			"value":$scope.New_Platform
	},
	
	{
		"id" : 28,
		"name" : "Producer",
		"value":$scope.New_Producer.replace(/\s\s+/g, " ")
	},
  
	{
		"id" :31,
		"name" : "Live URL",
		"value":$scope.New_url
	},
  
	{
		"id" : 16,
		"name" : "Project Type",
		"value":"Campaign Factory"
	},
	
	{
		"id" : 45,
		"name" : "Acronis Link",
		"value":$scope.New_Alink
	},
	{
		"id" : 35,
		"name" : "Exit Date",
		"value": $scope.Exit_Date
	},
	{
	"id":60,
	"name":"Due Date",
	"value":$scope.FDueDate
	}

 ];
  
     $scope.obj.custom_fields=customlist;
	 $scope.obj.parent_id= CONFIG.parent_id;
	 $scope.strFinal = JSON.stringify($scope.obj);

	 console.log($scope.strFinal);
		var req = {
		method: 'POST',
		url: $scope.NewCampaignUrl,
		data: $scope.strFinal,
		dataType: 'json',
		headers: $rootScope.headers,
		contentType:'application/json'
 
		}

		$http(req).success(function()
			{
				//$modalInstance.dismiss();	Has Been Declared For It
				$rootScope.displayOverlay=false;
			$timeout(function()
					{
						//alert("New Campaign Created Successfully");
						ohSnap('New Campaign Created Successfully','green');
						/* $window.location.reload(); */
						$rootScope.printIt();
					},500);
					
				
	
			}).error(function(data)
				{
					//$modalInstance.dismiss();
					$rootScope.displayOverlay=false;
					$rootScope.errorDisplay(data);
				});
 };				


			$scope.ok = function () 
			{
					$modalInstance.close();
			};

			$scope.cancel = function () 
			{
				$modalInstance.dismiss();
			};
		
			/* $scope.Onfocusout= function()
				{
					if($scope.camp_name=='a')
					{
						alert('This Field Cant Blank');
						 //element[0].focus();
					}
				}; */
	
});



angular.module('myApp').controller('ModalInstanceCtrl', function ($scope, $modalInstance, items,imagedetail,modalvalue, $modal, $filter, $http, $timeout,$rootScope,$window,CONFIG)
 {	 
	 /* $scope.DeployCampaignUrl='http://10.97.85.76:8090/api/copymaster';
	 $scope.UpdateCampaignUrl='http://10.97.85.76:8090/api/jsondata';
	 $scope.ProcurementUrl='http://10.97.85.76:8090/api/project'; */
	 
	// for images modal
	$scope.DeployCampaignUrl=$rootScope.apiUrl+'copymaster';
	$scope.UpdateCampaignUrl=$rootScope.apiUrl+'project';
	$scope.ProcurementUrl=$rootScope.apiUrl+'project';
	
	//console.log($scope.DeployCampaignUrl);
	//console.log($scope.UpdateCampaignUrl);
	//console.log($scope.ProcurementUrl);
		
	

	//console.log($scope.imgselect);
	$scope.JsonImageObject="";
	$scope.items=items;
	$scope.ImageUrLData=imagedetail;
	$scope.EstimatesData=imagedetail;
	var detail=$scope.items;
	$scope.ProjectDetail=detail;
	$scope.EstModal=modalvalue;
	//console.log(detail);
	//$scope.selected='';
	
	/*Calender Code */
	
	
	
	$scope.formats = ['yyyy-MM-dd','dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
	$scope.MinDevDate = $scope.MinDevDate ? null : new Date();
	$scope.MinEDate = $scope.MinEDate ? null : new Date();
	$scope.MinExitDate==new Date();
	$scope.MinExitDate=$scope.MinEDate.setDate($scope.MinEDate.getDate()+1);
	$scope.MinDeployDate = $scope.MinDeployDate ? null : new Date();
	
		
	 $scope.openLaunchDate = function($event)
	 {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.openLaunchCal = true;
		$scope.openExitCal = false;
		//console.log($scope.Datee);		
	 };
	 
	 $scope.openExitDate = function($event)
	 {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.openExitCal = true;
		$scope.openLaunchCal = false;
	 };
	
	$scope.openDeployDate = function($event)
	 {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.openDepCal = true;
	 };
	
	/*Calender Code Ends*/
	
	$scope.clickon=function()
	{
		console.log($scope.Platform);
	}
	
	$scope.selImage = function(data,index) {
		$scope.JsonImageObject=data;
		$scope.imgselect = data.name;
		$scope.imageurl=data.url;
		$scope.selected=index;
	}
	
	$scope.datestring=new Date();
	$scope.todayDate=$filter('date')($scope.datestring, "yyyy-MM-dd"); 
	
	$scope.updateobj={};
	$scope.deployobj={};
	$scope.procobj={};
	$scope.ProjectId=detail.id;
	$scope.DateDev=detail.development_start_date;
	$scope.DateDev=$filter('date')($scope.DateDev, "yyyy-MM-dd");
	//$scope.DateLaunch=detail.launch_date;
	$scope.Datee=detail.development_start_date;
	$scope.LDate=new Date(detail.launch_date);
	//$scope.FormatedDevDate=$filter('date')($scope.Datee, "yyyy-MM-dd");
	//$scope.FormatedLaunchDate=$filter('date')($scope.LDate, "yyyy-MM-dd"); 
	$scope.CampaignName=detail.name;
	$scope.DevDate=$filter('date')($scope.Datee, "yyyy-MM-dd");//detail.development_start_date;//
	//$scope.LaunchDate=detail.launch_date;
	//$scope.DateLaunch;//$scope.FormatedLaunchDate.toString();
	$scope.Platform=detail.hosting_platform;
	$scope.Market=detail.market;
	$scope.DateLaunch=detail.launch_date;
	$scope.DateLaunch=$filter('date')($scope.DateLaunch, "yyyy-MM-dd");
	$scope.UpdateFormExit=detail.exit_date;
	$scope.UpdateFormExit=$filter('date')($scope.UpdateFormExit, "yyyy-MM-dd");
	
			
	$scope.DisplayLDate=function()
	{
		$scope.DateLaunch=$filter('date')($scope.DateLaunch, "yyyy-MM-dd");
		console.log($scope.DateLaunch);
		$scope.UpdateFormL=new Date($scope.DateLaunch);
		$scope.MinExitDate=$scope.UpdateFormL.setDate($scope.UpdateFormL.getDate()+1);
		$scope.CheckLDate=new Date($scope.DateLaunch);
		$scope.CheckEDate=new Date($scope.UpdateFormExit);
		if($scope.CheckLDate.getYear()>=$scope.CheckEDate.getYear())
		{
			if($scope.CheckLDate.getMonth()>=$scope.CheckEDate.getMonth())
			{
				if($scope.CheckLDate.getDate()>$scope.CheckEDate.getDate())
				{
					console.log('hii');
				$scope.UpdateFormExit=$scope.CheckLDate.setDate($scope.CheckLDate.getDate()+1);
				$scope.UpdateFormExit=$filter('date')($scope.UpdateFormExit, "yyyy-MM-dd");
				$scope.UpdateFormE=new Date($scope.UpdateFormExit);
				$scope.maxLaunchDate=$scope.UpdateFormE.setDate($scope.UpdateFormE.getDate()-1);
				console.log($scope.UpdateFormExit);
				}			
			}	
		}
	}
	
	$scope.DisplayDate=function()
	{
		$scope.UpdateFormExit=$filter('date')($scope.UpdateFormExit, "yyyy-MM-dd");
		console.log($scope.UpdateFormExit);
		$scope.UpdateFormE=new Date($scope.UpdateFormExit);
		$scope.maxLaunchDate=$scope.UpdateFormE.setDate($scope.UpdateFormE.getDate()-1);
	}
	if($scope.DateLaunch!="" /* || $scope.UpdateFormExit!="" */)
	{
		$scope.DisplayLDate();
		//$scope.DisplayDate();
	}
	
	$scope.Burndown=detail.burndown;
	$scope.Producer=detail.producer_name;
	$scope.ProjectCost=detail.project_cost;
	$scope.estimateTime=detail.EstimatedTime;
	$scope.projectID=detail.id;
	
	
	$scope.updateCampaign = function()
	{
		
		$modalInstance.dismiss();
		$rootScope.displayOverlay=true;
	 
	var customlist=[
 
    {
		"id" : CONFIG.development_startdate_id,
		"name" : "Development Start Date",
		"value" : $scope.DevDate
	},
  
    {
		"id" : CONFIG.launchdate_id,
		"name" : "Launch Date",	
		"value" : $scope.DateLaunch
	},
  
	{
		"id" : CONFIG.exitdate_id,
		"name" : "Exit Date",
		"value" : $scope.UpdateFormExit
	},
  
	{
		"id" : CONFIG.estimates_id,
		"name" : "Estimates Shared with Client",
		"value" : $scope.estimateTime
	},
      
	{
		"id" : CONFIG.producer_id,
		"name" : "Producer",
		"value" : $scope.Producer
	},
	{
		"id" : CONFIG.market_id,
		"name" : "Market",
		"value" : $scope.Market
	},
	{
		"id" : CONFIG.project_tech_id,
		"name" : "Project Technology",
		"value" : $scope.Platform
	}
	
	];
	//console.log($scope.UpdateFormExitDatee);
	//console.log($scope.LaunchDate);
	
	  $scope.updateobj.name =$scope.CampaignName;
	  $scope.updateobj.response_put= CONFIG.response_put;
	  $scope.updateobj.id = $scope.projectID;   // get it from project
	  /* $scope.updateobj.description="HCL Test Description";
	  $scope.updateobj.created_on = $scope.DevDate;
	  $scope.updateobj.exit_date=$scope.LaunchDate; */
	  
		//console.log($scope.DevDate);
	  $scope.updateobj.custom_fields=customlist;
	 // $scope.strFinal = JSON.stringify($scope.updateobj);
		$scope.updateobj.parent_id=CONFIG.parent_id; /* 23; */
		$scope.UpdatedString=JSON.stringify($scope.updateobj);
	 console.log($scope.UpdatedString);

	var req = {
		method: 'POST',
		url: $scope.UpdateCampaignUrl,
		data: $scope.UpdatedString,
		dataType: 'json',
		headers: $rootScope.headers,
		contentType:'application/json'
 
		}

		$http(req).success(function()
			{
				//$modalInstance.dismiss();
				$rootScope.displayOverlay=false;
				$timeout(function()
					{
						ohSnap('Campaign Updated Successfully','green');
						/* $window.location.reload(); */
						$rootScope.printIt();
					},500);
				
	
			}).error(function(data)
				{
					//$modalInstance.dismiss();
					$rootScope.displayOverlay=false;
					$rootScope.errorDisplay(data);
				});

	};	 	
	/* Update Campaign Function Ends */

	
	$scope.SlotList=[
        {id:1, name:'0800HRS-1100HRS'},
         {id:2, name:'1500HRS-1800HRS'}
    ];
	
	$scope.EnvList=[
        {id:1, name:'Live'},
         {id:2, name:'QA'}
    ];
	
	$scope.PriorityList=[
        {id:1, name:'LOW'},
         {id:2, name:'HIGH'}
    ];	
	
	$scope.changedSlot=function()
	{
		console.log($scope.DeploymentSlot);
	};
	
	$scope.changedEnv=function()
	{
		console.log($scope.Environment);
	};
	
	$scope.changedPri=function()
	{
		console.log($scope.Priority);
	};
	
	$scope.deployRequest = function()
	{
		$modalInstance.dismiss();
		$rootScope.displayOverlay=true;
		var customlist=[
		{
			"id":CONFIG.deployment_environment_id,
			"name":"Environment",
			"value":$scope.Environment			
		},
		{
			"id":CONFIG.deployment_slot_id,
			"name":"Deployment Slot (IST)",
			"value":$scope.DeploymentSlot		
		}
		];
		
		var pro_parent={};
		pro_parent.id= 23;
		pro_parent.name="Campaign Factory";

		$scope.deployobj.subject = $scope.items.name;//done
		  $scope.deployobj.project_id = CONFIG.project_id; //fixed for deployment 
		  $scope.deployobj.tracker_id = CONFIG.tracker_id;//fixed
		  $scope.deployobj.status_id = CONFIG.status_id;
		  $scope.deployobj.category_id = CONFIG.category_id;
		  $scope.deployobj.fixed_version_id  = CONFIG.fixed_version_id;
		  $scope.deployobj.assigned_to_id  = CONFIG.assigned_to_id;
		  $scope.deployobj.parent_issue_id = CONFIG.parent_issue_id;
		  $scope.deployobj.is_private  = CONFIG.is_private;
		  $scope.deployobj.estimated_hours  = CONFIG.estimated_hours;
		  $scope.deployobj.due_date = "2015-12-12";//greater than current  date 
		  $scope.deployobj.description= "test description";//done
		  $scope.deployobj.priority_id = $scope.Priority=='Low'?1:2;
		  $scope.deployobj.custom_fields=customlist;

		$scope.strDeployFinal = JSON.stringify($scope.deployobj);
	console.log($scope.strDeployFinal);

	var req = {
		method: 'POST',
		url: $scope.DeployCampaignUrl,
		data: $scope.strDeployFinal,
		dataType: 'json',
		headers: $rootScope.headers,
		contentType:'application/json'
 
		}

		$http(req).success(function()
			{
				//$modalInstance.dismiss();
				$rootScope.displayOverlay=false;
				$timeout(function()
					{
						ohSnap('Data Added Successfully','green');
						/* $window.location.reload(); */
						$rootScope.printIt();
					},500);
				
	
			}).error(function(data)
				{
					//$modalInstance.dismiss();
					$rootScope.displayOverlay=false;
					$rootScope.errorDisplay(data);
				});
			
	};
	/* deoployRequest Function Ends */ 
	
	
	$scope.addProcurement = function()
	{
		$modalInstance.dismiss();
			$rootScope.displayOverlay=true;
		var customlist=[	/* objCust3,objCust4,objCust5,objCust6,objCust7,objCust8,objCust9 */
		{
			"id":CONFIG.procurement_aws_id,
			"name":"Aws Account Link",
			"value":$scope.AwsLink
		},
		{
			"id":CONFIG.procurement_name_id,
			"name":"User Name",
			"value":$scope.Proc_Username
		},
		{
			"id":CONFIG.procurement_password_id,
			"name":"Password",
			"value":$scope.Proc_Pass
		},
		{
			"id":CONFIG.procurement_livecname_id,
			"name":"Live CName",
			"value":$scope.Proc_Live_Name
		},
		{
			"id":CONFIG.procurement_liveelastic_id,
			"name":"Live Elastic IP",
			"value":$scope.Proc_Elastic
		},
		{
			"id":CONFIG.procurement_QAcname_id,
			"name":"QA CName",
			"value":$scope.Proc_Cname
		},
		{
			"id":CONFIG.procurement_QAelastic_id,
			"name":"QA Elastic IP",
			"value":$scope.Proc_ElasticIP
		}
		
		];
		
		
		  $scope.procobj.name = $scope.CampaignName;
		  $scope.procobj.response_put= CONFIG.response_put;
		  $scope.procobj.id =$scope.ProjectId;   // get it from project
		  /* $scope.procobj.description="HCL Test Description";
		  $scope.procobj.created_on = "2013-12-21";
		  $scope.procobj.exit_date="2014-12-21" */
		  $scope.procobj.parent_id=CONFIG.parent_id;/* 23; */
		  $scope.procobj.custom_fields=customlist;


		$scope.strProcFinal = JSON.stringify($scope.procobj);

		console.log($scope.strProcFinal);
		
		var req = {
		method: 'POST',
		url: $scope.ProcurementUrl,
		data: $scope.strProcFinal,
		dataType: 'json',
		headers: $rootScope.headers,
		contentType:'application/json'
 
		}

		$http(req).success(function()
			{
				//$modalInstance.dismiss();
				$rootScope.displayOverlay=false;
				$timeout(function()
					{
						ohSnap('Procurement Details Added Successfully','green');
						/* $window.location.reload(); */
						$rootScope.printIt();
					},500);
				
	
			}).error(function(data)
				{
					//$modalInstance.dismiss();
					$rootScope.displayOverlay=false;
					$rootScope.errorDisplay(data);
				});
		
	};
	/* addProcurement Function Ends */
	
	/* updateEstimate Function Starts */
	if($scope.EstModal==2)
	{
						$scope.tabShowerChk='EstimatD';
				 
				  
				$scope.RateCost=315;
				$scope.AccountDigit=$scope.ProjectManager=$scope.EstProducer=$scope.Wireframes=$scope.EstAssistance=$scope.EsttDevelopment=$scope.EstDevelopment=$scope.EstQA=$scope.EstMaintenance=$scope.EstSetup=$scope.EstAmazon=$scope.EstOutofPocket=$scope.EstOther=$scope.EstRate=$scope.EstRisk=$scope.EstId=$scope.EstComment="";
				
	
				if($scope.EstimatesData.length!=0)
				{
					$scope.EstId=$scope.EstimatesData[0].id;
					$scope.EstRate=$scope.EstimatesData[0].custom_fields[0].value;
					$scope.RateCost=$scope.RateCost==''?0:$scope.RateCost;
					$scope.RateCost=$scope.EstimatesData[0].custom_fields[1].value;
					$scope.AccountDigit=$scope.EstimatesData[0].custom_fields[9].value;									
					$scope.ProjectManager=$scope.EstimatesData[0].custom_fields[2].value;					
					$scope.EstProducer=$scope.EstimatesData[0].custom_fields[10].value;					
					$scope.Wireframes=$scope.EstimatesData[0].custom_fields[3].value;					
					$scope.EstAssistance=$scope.EstimatesData[0].custom_fields[11].value;					
					$scope.EsttDevelopment=$scope.EstimatesData[0].custom_fields[4].value;
					$scope.EsttDevelopment=$scope.EsttDevelopment==''?0:$scope.EsttDevelopment;					
					$scope.EstDevelopment=$scope.EstimatesData[0].custom_fields[12].value;
					$scope.EstDevelopment=$scope.EstDevelopment==''?0:$scope.EstDevelopment;					
					$scope.EstQA=$scope.EstimatesData[0].custom_fields[5].value;
					$scope.EstQA=parseInt($scope.EstQA, 10);
					$scope.EstMaintenance=$scope.EstimatesData[0].custom_fields[13].value;					
					$scope.EstSetup=$scope.EstimatesData[0].custom_fields[6].value;						
					$scope.EstAmazon=$scope.EstimatesData[0].custom_fields[14].value;						
					$scope.EstOutofPocket=$scope.EstimatesData[0].custom_fields[7].value;						
					$scope.EstOther=$scope.EstimatesData[0].custom_fields[15].value;				
					$scope.EstRisk=$scope.EstimatesData[0].custom_fields[16].value;	
					$scope.EstRisk=$scope.EstRisk==''?0:$scope.EstRisk;
					$scope.EstRisk=parseInt($scope.EstRisk, 10);
										
				}
				
				
				$scope.RateCost=$scope.RateCost==''?0:parseInt($scope.RateCost,10);
				
				$scope.AccountDigit=$scope.AccountDigit==''?parseInt(0,10):parseInt($scope.AccountDigit,10);
				$scope.ProjectManager=$scope.ProjectManager==''?parseInt(0,10):parseInt($scope.ProjectManager,10);
				$scope.EstProducer=$scope.EstProducer==''?parseInt(0,10):parseInt($scope.EstProducer,10);
				$scope.Wireframes=$scope.Wireframes==''?parseInt(0,10):parseInt($scope.Wireframes,10);
				$scope.EstAssistance=$scope.EstAssistance==''?parseInt(0,10):parseInt($scope.EstAssistance,10);
				$scope.EsttDevelopment=$scope.EsttDevelopment==''?parseInt(0,10):parseInt($scope.EsttDevelopment,10);
				$scope.EstDevelopment=$scope.EstDevelopment==''?parseInt(0,10):parseInt($scope.EstDevelopment,10);
				$scope.EstQA=$scope.EstQA==''?parseInt(0,10):parseInt($scope.EstQA,10);
				$scope.EstMaintenance=$scope.EstMaintenance==''?parseInt(0,10):parseInt($scope.EstMaintenance,10);
				$scope.EstSetup=$scope.EstSetup==''?parseInt(0,10):parseInt($scope.EstSetup,10);
				$scope.EstAmazon=$scope.EstAmazon==''?parseInt(0,10):parseInt($scope.EstAmazon,10);
				$scope.EstOutofPocket=$scope.EstOutofPocket==''?parseInt(0,10):parseInt($scope.EstOutofPocket,10);
				$scope.EstOther=$scope.EstOther==''?parseInt(0,10):parseInt($scope.EstOther,10);
				$scope.EstRisk=$scope.EstRisk==''?parseInt(0,10) :parseInt($scope.EstRisk,10);
				$scope.EstTotal=$scope.EstTotalCost=$scope.EstRiskCost=0;
				
				
				if($scope.EstQA == undefined)
				{
					$scope.EstQA=0;
				}
			
				$scope.CalcTotal=function()
				{
					$scope.EstTotal=$scope.AccountDigit+$scope.ProjectManager+$scope.EstProducer+$scope.Wireframes+$scope.EstAssistance+$scope.EsttDevelopment+$scope.EstDevelopment+$scope.EstQA+$scope.EstMaintenance+$scope.EstSetup+$scope.EstAmazon+$scope.EstOutofPocket+$scope.EstOther;
					$scope.EstTotalCost=$scope.EstTotal*$scope.RateCost;
					$scope.EstRiskCost=$scope.EstTotal*$scope.RateCost*$scope.EstRisk/100;
				}
				
				$scope.CalcTotal();
				
				//$scope.Wireframes=parseInt($scope.Wireframes, 10);
				
				$scope.updateEstimate=function()
				{
					
					$modalInstance.dismiss();
					$rootScope.displayOverlay=true;
					$scope.strEst='{"subject":"'+$scope.CampaignName+'","project_id":386,"priority_id":1,"tracker_id":'+CONFIG.tracker_id_estimate+','+ ($scope.EstId==0?'':('"id":'+$scope.EstId+','))+
				'"custom_fields":[{ id: 63, name: "Rate Dkk Hrs", value: '+($scope.EstRate==""?0:$scope.EstRate)+' },'+
				'{ id: 64, name: "Rate Dkk Cost", value: '+($scope.RateCost==""?0:$scope.RateCost)+' },'+
				'{ id: 65, name: "Project Manager", value: '+($scope.ProjectManager==""?0:$scope.ProjectManager)+' },'+
				'{ id: 66, name: "UX (Wireframes)", value: '+($scope.Wireframes==""?0:$scope.Wireframes)+' },'+
				'{ id: 67, name: "3D Development", value: '+($scope.EsttDevelopment==""?0:$scope.EsttDevelopment)+' },'+
				'{ id: 68, name: "QA", value: '+($scope.EstQA==""?0:$scope.EstQA)+' },'+
				'{ id: 69, name: "Setup and Deployment", value: '+($scope.EstSetup==""?0:$scope.EstSetup)+' },'+
				'{ id: 70, name: "Out of Pocket", value: '+($scope.EstOutofPocket==""?0:$scope.EstOutofPocket)+' },'+
				'{ id: 71, name: "Total ", value: '+($scope.EstTotal==""?0:$scope.EstTotal)+' },'+
				'{ id: 72, name: "Account Management", value: '+($scope.AccountDigit==""?0:$scope.AccountDigit)+' },'+
				'{ id: 73, name: "Producer", value: '+($scope.EstProducer==""?0:$scope.EstProducer)+' },'+
				'{ id: 74, name: "Design Assistance", value: "'+($scope.EstAssistance==""?0:$scope.EstAssistance)+'" },'+
				'{ id: 75, name: "Development", value: '+($scope.EstDevelopment==""?0:$scope.EstDevelopment)+' },'+
				'{ id: 76, name: "Maintenance", value: '+($scope.EstMaintenance==""?0:$scope.EstMaintenance)+' },'+
				'{ id: 77, name: "Amazon Hosting Cost", value: '+($scope.EstAmazon==""?0:$scope.EstAmazon)+' },'+
				'{ id: 78, name: "Other", value: '+($scope.EstOther==""?0:$scope.EstOther)+' },'+
				'{ id: 79, name: "Risk(+/- %)", value: '+($scope.EstRisk==""?0:$scope.EstRisk)+' },'+
				'{ id: 81, name: "p_id" , value: '+($scope.ProjectId==""?0:$scope.ProjectId)+' }]}';

	console.log($scope.strEst);
	
				$scope.EstDesc="";
				$scope.EstDesc=$scope.EstComment;
				
	if($scope.EstId ==0){
						
											var req = {
													method: 'POST',
													url: $rootScope.apiUrl+'estimate',
													data: $scope.strEst,
													dataType: 'json',
													headers: $rootScope.headers,
													contentType:'application/json'
											 
														}
		
		$http(req).success(function()
			{
				$scope.updateCampaignOnEstimate();
				if($scope.EstDesc!="")
				{
					$http({
							  url:$rootScope.apiUrl+'estimate/?pid_Id='+CONFIG.project_pid+'&pid_Value='+$scope.ProjectId,
							  method: 'GET',
							  headers: $rootScope.headers
							  }).success(function (response)
								{
										$scope.EstId=response[0].id;
										console.log($scope.EstId);
										$scope.postcomment($scope.EstDesc,$scope.EstId);
								});		
								
				

				}
					
			}).error = function(data) 
				{
								$rootScope.displayOverlay=false;
								$rootScope.errorDisplay(data);
				};

	}
	
	else{

					var req = {
								method: 'PUT',
								url: $rootScope.apiUrl+'estimate',
								data: $scope.strEst,
								dataType: 'json',
								headers: $rootScope.headers,
								contentType:'application/json'						 
							}
							$http(req).success(function()
									{
											$scope.updateCampaignOnEstimate();
											if($scope.EstDesc!="")
											$scope.postcomment($scope.EstDesc,$scope.EstId);
									}).error = function(data) 
								{
										$rootScope.displayOverlay=false;
										$rootScope.errorDisplay(data);
								};

		  
		}

					 
 	}
			$scope.updateCampaignOnEstimate= function()
					{
						$scope.StringEst='{"name":"'+$scope.CampaignName+'","response_put":'+CONFIG.response_put
						+',"parent_id":'+CONFIG.parent_id+',"id":'+$scope.ProjectId+',"custom_fields":[{ id: 3, name: "Estimates Shared with Client", value: '+$scope.EstTotal+' }]}';
				console.log($scope.StringEst);
				
					var req = {
								method: 'POST',
								url: $rootScope.apiUrl+'project',
								data: $scope.StringEst,
								dataType: 'json',
								headers: $rootScope.headers,
								contentType:'application/json'						 
							};
							
							$http(req).success(function()
									{
										$rootScope.displayOverlay=false;
											ohSnap('Estimates added successfully','green');
											$rootScope.printIt();
									}).error = function(data) 
								{
									$rootScope.displayOverlay=false;
									$rootScope.errorDisplay(data);
								};
				}; 
			
			 $scope.tabShower=function(tab)
				  {
					  $scope.tabShowerChk=tab;
					  if(tab=='Comment')
					  {
						  $scope.getComment($scope.EstId);
					  }
				  }
			
		/* function */
				
				$scope.CommentBtn=false;
				
				$scope.postEstComment=function()
				{
					$scope.postcomment($scope.EstDesc,$scope.EstId);
				}
				
				
 
 }			/* Main If For Estimate Form */
				
				$scope.postcomment=function(passcmt,estid)
				{
						$scope.PassComment=passcmt;
						$scope.EstId=estid;
					if($scope.EstId!=0)
					{
						$scope.CommentBtn=true;
						
						
					var req = {
								
								method: 'POST',
								url: $rootScope.apiUrl+'notes?issueId='+$scope.EstId+'&message='+$scope.PassComment,
								headers: $rootScope.headers,
								contentType:'application/json'
							}
		
							$http(req).success(function()
								{
											
									$timeout(function()
										{
											$scope.EstDesc="";
											//alert("Comment Posted");
											$scope.getComment($scope.EstId);
										},300);
									
						
								}).error(function(data)
									{
										$rootScope.displayOverlay=false;
										$rootScope.errorDisplay(data);
									});
									
					}				
					else
					{
						$modalInstance.dismiss();
						ohSnap('Add Estimate First','orange');						
					}
					
				}		/* PostComment Ends */
		
		$scope.getComment=function(estid)
		{
			$scope.CommentBtn=false;
			$scope.EstId=estid;
		if($scope.EstId!=0)
			{
										$http({
						  url:$rootScope.apiUrl+'notes?issueId='+$scope.EstId,
						  method: 'GET',
						  headers: $rootScope.headers
						  }).success(function (response)
									{
										
										$scope.EstComments=response;
										//console.log($scope.EstComments);
									}).error(function(data)
									{
										$rootScope.displayOverlay=false;
										$rootScope.errorDisplay(data);
									});
									
			}						
		}		/* GetComment Ends */
				
	/* updateEstimate Module Ends */
	/* Approve Est Function Starts */
	if($scope.EstModal==3)
	{
		if($scope.EstimatesData.length!=0)
				{
					$scope.approveEst=function()
					{
						$modalInstance.dismiss();
						$rootScope.displayOverlay=true;
						$scope.EstId=$scope.EstimatesData[0].id;
						$scope.postcomment($scope.Approvecmt,$scope.EstId);
						var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; 
	var yyyy = today.getFullYear();
	var d = yyyy+'-'+(mm<10?('0'+mm):mm)+'-'+(dd<10?('0'+dd):dd);
	$scope.stringappr='{"name":"'+$scope.items.name+'","response_put":'+CONFIG.response_put+',"parent_id":'+CONFIG.parent_id+
	',"id":'+$scope.items.id+',"custom_fields":[{ "id": '+CONFIG.development_startdate_id+', "name": "Development Start Date", "value": "'+d+'" }]}';
	
	var req = {
								method: 'POST',
								url: $rootScope.apiUrl+'project',
								data: $scope.stringappr,
								dataType: 'json',
								headers: $rootScope.headers,
								contentType:'application/json'
						 
							}
		
		$http(req).success(function()
			{
				$rootScope.displayOverlay=false;
				
				$timeout(function()
					{
						console.log($scope.stringappr);
						ohSnap('Estimates Approved Successfully. Campaign moved to development tab.','green');
						$rootScope.printIt();
					},500);
				
	
			}).error(function(data)
				{
					$rootScope.displayOverlay=false;
					$rootScope.errorDisplay(data);
				});
						
					}
				}
				
	}
	/* Approve Est Function Ends */
  $scope.ok = function () {
    $modalInstance.close();
  };

  $scope.cancel = function () {
    $modalInstance.dismiss();
  };
  
  
   $scope.SelectImgModal=function()
  {
	$modalInstance.dismiss();				
	$rootScope.displayOverlay=true;
	
			$scope.stringCmp='{"name":"'+$scope.ProjectDetail.name+'","response_put":'+CONFIG.response_put+',"parent_id":'+CONFIG.parent_id+'23,"id":'+$scope.ProjectDetail.id+',"custom_fields":[{ id:59 , name: "image_url", value: "'+$scope.imageurl+'" }]}';
				console.log($scope.stringCmp);
				
				var req = {
								method: 'POST',
								url: $rootScope.apiUrl+'project',
								data: $scope.stringCmp,
								dataType: 'json',
								headers: $rootScope.headers,
								contentType:'application/json'
						 
							}
		
		$http(req).success(function()
			{
				
				$rootScope.displayOverlay=false;
				
				$timeout(function()
					{
						ohSnap('Image Updated Successfully','green');
						$rootScope.printIt();
					},500);
				
	
			}).error(function(data)
				{
					$rootScope.displayOverlay=false;
					$rootScope.errorDisplay(data);
				});
	
  }

    
 $scope.cancelModal = function(size)
			{
				
					$scope.jsonObj=items;
					$rootScope.displayOverlay=true;
			$modalInstance.dismiss();
			$http({
  url:$rootScope.apiUrl+'issue/'+ $scope.jsonObj.id,
  method: 'GET',
  headers: $rootScope.headers
  }).success(function (response)
			{
				$rootScope.displayOverlay=false;
					$scope.TrackerData = response;
						
					var modalInstance = $modal.open(
				{
				animation: $scope.animationsEnabled,
				templateUrl: 'DetailModalContent',
				backdrop : 'static',
				controller: 'DetailModalInstanceCtrl',
				size:size,
				//windowClass: 'center-modal',
						resolve: 
							{
									items: function ()
									{
										return items;
									},
									value:function ()
									{
										return $scope.TrackerData;
									}
							}	
			
				});	/* open */
			}).error(function(data){
				$rootScope.displayOverlay=false;
				$rootScope.errorDisplay(data);
			}).done;
			
			}
	/* Cancel Modal Ends */
  
});


angular.module('myApp'/* , ['chart.js'] */ ).controller('DetailModalInstanceCtrl', function ($scope, $http, $modalInstance, items, $modal,value,$rootScope,$timeout,CONFIG)
{

$scope.items=items;
$scope.ProjectId='';
$scope.ProjectId=$scope.items.id;
$scope.EstimateTime=$scope.items.EstimatedTime;
$scope.StartDate=$scope.items.development_start_date;
$scope.GenValue=1;

var detail=$scope.items;

if($rootScope.fnName=='jsondata/live' || $rootScope.fnName=='jsondata/archived')
	$scope.Analytics='Analytics';
else
	$scope.Analytics='Development Status';

$http.get('images.json')
			.success(function (response)
		{
				$scope.imgname = response;
		});
		
	$scope.TrackerData=value;
		
		console.log($scope.TrackerData);

	var barChartData = {
		labels : ["Feature","Bug","Feedback","Test Case","Test Scenarios"],
		datasets : [
			{
				label: "Close",
				fillColor : "rgba(237, 89, 78,0.5)",
				strokeColor : "rgba(237, 89, 78,0.8)",
				highlightFill: "rgba(237, 89, 78,0.75)",
				highlightStroke: "rgba(237, 89, 78,1)",
				data : [$scope.TrackerData.total_feature_New,$scope.TrackerData.total_bug_New,$scope.TrackerData.total_feedback_New,$scope.TrackerData.total_testScenarios_New,$scope.TrackerData.total_testcase_New]
			},
			{
				label: "Open",
				fillColor : "rgba(78, 226, 236,0.5)",
				strokeColor : "rgba(78, 226, 236,0.8)",
				highlightFill : "rgba(78, 226, 236,0.75)",
				highlightStroke : "rgba(78, 226, 236,1)",
				data : [$scope.TrackerData.total_feature_Closed,$scope.TrackerData.total_bug_Closed,$scope.TrackerData.total_feedback_Closed,$scope.TrackerData.total_testScenarios_Closed,$scope.TrackerData.total_testcase_Closed]
			}
		]

	}

	setTimeout(function() {
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myBar = new Chart(ctx).Bar(barChartData, {
			responsive : true
		});
		
		
			$http({
		  url:$rootScope.apiUrl+'estimate/?pid_Id='+CONFIG.project_pid+'&pid_Value='+$scope.ProjectId,
		  method: 'GET',
		  headers: $rootScope.headers
		  }).success(function (response)
			{
				$scope.EstimateVal=response;
				if($scope.EstimateVal.length!=0)
					$scope.EnableApprove=true;
				else
					$scope.EnableApprove=false;
			});		
		
	},500);

	$scope.approveCampaign=function()
	{
		$rootScope.displayOverlay=true;
		$scope.GenValue=3;
		$modalInstance.dismiss();
		$http({
		  url:$rootScope.apiUrl+'estimate/?pid_Id='+CONFIG.project_pid+'&pid_Value='+$scope.ProjectId,
		  method: 'GET',
		  headers: $rootScope.headers
		  }).success(function (response)
			{
				$rootScope.displayOverlay=false;
				$scope.EstimateData=response;
						var modalInstance = $modal.open(
						{
						animation: $scope.animationEnabled,
						templateUrl: 'ApproveModalTemplate',
						backdrop : 'static',
						controller: 'ModalInstanceCtrl',
						resolve:
								{
									items: function ()
													{
														return $scope.items;
													},
													imagedetail: function()
													{
														return $scope.EstimateData;
													},
													modalvalue: function()
													{
														return $scope.GenValue;
													}
								}
						
						});
			});
	}
	
$scope.CampaignName=detail.name;
$scope.DevDate=detail.development_start_date;
$scope.LaunchDate=detail.launch_date;
$scope.Platform=detail.hosting_platform;
$scope.Market=detail.market;
$scope.Producer=detail.producer_name;
$scope.ProjectCost=detail.project_cost;

  $scope.ok = function ()
  {
   $modalInstance.close();
  };

  $scope.cancel = function ()
  {
   $modalInstance.dismiss();
    $modalInstance.dismiss();
  };
  

   $scope.animationEnabled = true; 
  $scope.procurementModal = function () 
		 {
			$modalInstance.dismiss();
				var modalInstance = $modal.open(
				{
				animation: $scope.animationEnabled,
				templateUrl: 'ActionModalContent',
				backdrop : 'static',
				controller: 'ModalInstanceCtrl',
				resolve:
				{
									items: function ()
									{
										return $scope.items;
									},
									imagedetail: function()
									{
										return $scope.imgname;
									},
									modalvalue: function()
									{
										return $scope.GenValue;
									}
				}
				});
		 };
	
	$scope.deploymentModal = function () 
		 {
			$modalInstance.dismiss();
				var modalInstance = $modal.open(
				{
				animation: $scope.animationEnabled,
				templateUrl: 'DeployModalContent',
				backdrop : 'static',
				controller: 'ModalInstanceCtrl',
				resolve:
				{
					items: function ()
									{
										return $scope.items;
									},
									imagedetail: function()
									{
										return $scope.imgname;
									},
									modalvalue: function()
									{
										return $scope.GenValue;
									}
				}
				});
		};

		$scope.updateCampaignModal = function () 
		 {
			$modalInstance.dismiss();
				//console.log($scope.ProjectCost);
				var modalInstance = $modal.open(
				{
				animation: $scope.animationEnabled,
				templateUrl: 'UpdateModalContent',
				backdrop : 'static',
				controller: 'ModalInstanceCtrl',
				resolve:
				{
					items: function ()
									{
										return $scope.items;
									},
									imagedetail: function()
									{
										return $scope.imgname;
									},
									modalvalue: function()
									{
										return $scope.GenValue;
									}
				}
				});
		};
	
		$scope.securityScan = function () 
		 {
			$modalInstance.dismiss();
				var modalInstance = $modal.open(
				{
				animation: $scope.animationEnabled,
				templateUrl: 'SecurityModalContent',
				backdrop : 'static',
				controller: 'ModalInstanceCtrl',
				resolve:
				{
					items: function ()
									{
										return $scope.items;
									},
									imagedetail: function()
									{
										return $scope.imgname;
									},
									modalvalue: function()
									{
										return $scope.GenValue;
									}
				}
				});
		};
		
		
		
		$scope.addEstimateModal=function(size)
		{
			$rootScope.displayOverlay=true;
			$scope.GenValue=2;
			$modalInstance.dismiss();
						
			$http({
		  url:$rootScope.apiUrl+'estimate/?pid_Id='+CONFIG.project_pid+'&pid_Value='+$scope.ProjectId,
		  method: 'GET',
		  headers: $rootScope.headers
		  }).success(function (response)
			{
				$rootScope.displayOverlay=false;
				$scope.EstimateData=response;
				
				var modalInstance = $modal.open(
				{
				animation: $scope.animationEnabled,
				templateUrl: 'EstimateModalTemplate',
				controller: 'ModalInstanceCtrl',
				backdrop : 'static',
				size:size,
				windowClass: 'center-modal',
				resolve:
				{
								items: function ()
									{
										return $scope.items;
									},
								imagedetail: function()
									{
										return $scope.EstimateData;
									},
								modalvalue: function()
								{
									return $scope.GenValue;
								}
				}
				});
				
			});	/* http */
		};
		
		 $scope.ImageSelection = function (size) 
		 {
				$modalInstance.dismiss();
				var modalInstance = $modal.open(
				{
				animation: true,
				templateUrl: 'ImageModalContent',
				controller: 'ModalInstanceCtrl',
				backdrop : 'static',
				size:size,
				windowClass: 'center-img-modal',
				resolve: 
							{
									/* items: function ()
									{
										return $scope.imgname;
									} */
									
									items: function ()
									{
										return $scope.items;
									},
									imagedetail: function()
									{
										return $scope.imgname;
									},
									modalvalue: function()
									{
										return $scope.GenValue;
									}
							}
				});
		};

	
});  
