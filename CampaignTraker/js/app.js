var count = 1;
var length = 0;
var datatable;
var archived;
var dev;
var live;
var estmt;
var tab = 'Live';
var campexitdate;
var campstartdate;
var currdate = new Date();
var dt_pointerl = '';
var dt_pointera = '';
var dt_pointerd = '';
var dt_pointere = '';
$(".divOverlay").hide();
var estdata;
$(document).ready(function() {
    $(".divOverlay").hide();
    $(".userName").html("<span >Welcome " + localStorage.getItem("CTuser") + "</span><span class='caret'></span> ");
    getDataNewLive();
});

function tablecreate(tab, pointer, dataString) {
    if ($.fn.dataTable.isDataTable(tab)) {

        pointer.fnDestroy(false);
    }
    var str = tab + ' .table-body';
    $(str).empty();
    $(str).append(dataString);
    pointer = $(tab).dataTable({
        paging: true
    });
    return pointer;
}

function tokenaccess() {
    var tokenKey = "accessToken";
    var token = localStorage.getItem(tokenKey);
    var headers = {};
    if (token) {
        headers.Authorization = token;
        return headers;
    } else {
        window.location = 'index.html';
    }

}

function errordisplay(data) {
    if (data.status == '500')
        ohSnap(data.responseText,'red');
    else if (data.status == '404')
        ohSnap("API not responding",'red');
    else if (data.status == '401')
        ohSnap(data.responseText,'red');
    else
        ohSnap("Sorry! Please Try Again Later",'blue');

}
$(document).ready(function() {
    $('.logout').click(function() {
        var param = "";
        var tokenKey = "accessToken";
        $.ajax({
            type: 'POST',
            url: apiURL + '/Accounts/logout',
            headers: tokenaccess(),
            dataType: "json",
            data: param,
            error: function(data) {
                localStorage.setItem(tokenKey, "");
                errordisplay(data);
            }
        }).done(function(data) {
            localStorage.clear();
            window.location = 'index.html';
        });
    });
});

function getDataNewLive() {


    $('#loaderlive').show();
    $('#Live_dataTable').hide();
    $.ajax({
        url: apiURL + '/jsondata/live',
        type: "GET",
        headers: tokenaccess(),
        success: function(data) {
            live = '';
            if (data == false) {
                $('#loaderlive').hide();
                dt_pointerl = tablecreate("#Live_dataTable", dt_pointerl, '');
                return;
            }
            for (var i = 0; i < data.length; i++) {
                var market = data[i].market;
                if (market.split(',').length > 1) {
                    market = '<span title="' + market + '"><img src="images/market.png" height="30px" width="30px" /> </span>';
                }
                var disStatus = 'block';

                if (!data[i].live_url || data[i].live_url == "#" || data[i].live_url == "NA") {
                    disStatus = 'none';
                }
                live = live + '<tr style="font-size:small;height:40px">' +
                    '<td><a href="javascript:;" class="analytic_status" data-id="' + data[i].id.toString() + '" data-status="' + data[i].status + '" data-project="' + data[i].name +
                    '" data-launchdate="' + data[i].launch_date + '" data-exitdate="' + data[i].exit_date + '" data-projectcost="' + data[i].project_cost +
                    '" data-producername="' + data[i].producer_name + '" data-hostingplatform="' + data[i].hosting_platform + '" data-market="' + data[i].market + '"data-estimate="' + data[i].EstimatedTime + '" data-startdate="' + data[i].development_start_date + '" data-Burndown="' + data[i].burndown +
                    '"><b>' + data[i].name + '</b></a></td>' +
                    '<td width="110px">' + check_null(data[i].launch_date) + '</td>' +
                    '<td width="110px"> ' + check_null(data[i].exit_date) + ' </td>' +
                    '<td width="110px"><a  data-id="' + data[i].id + '" data-name="' + data[i].name + '" href="javascript:;" id="addEstimatebtn' + data[i].id + '" class="estimatebudget">DKK ' + data[i].project_cost + '</a></td>' +
                    '<td>' + data[i].producer_name + '</td><td>' + market + '</td>' +
                    '<td>' + data[i].hosting_platform + '</td>' +
                    '<td style="width: 1%;"><span style="display:' + disStatus + '" ' + ' class="label ' + site_down_color(data[i].SiteDown) + '">' + SiteDown(data[i].SiteDown) + '</span></td>' +
                    '<td onclick="stop()"><a href="' + data[i].live_url + '" target="_blank" style="text-decoration: none;font-weight:normal; onclick="stop()"><span class="glyphicon glyphicon-new-window" aria-hidden="true" style="display:' + disStatus + '" title="' + data[i].live_url + '"> </span></a></td></tr>';

            }
            dt_pointerl = tablecreate("#Live_dataTable", dt_pointerl, live);

        },
        error: function(data) {

            errordisplay(data);
            $('#loaderlive').hide();
        }
    }).done(function(data) {
        $('#Live_dataTable').show();
        $('#loaderlive').hide();
        if ($("a[aria-controls='Live']").attr("aria-expanded") == "true") {
            tab = 'live';
        }
    }).fail("errorlive");
}

function getDataNewArchived() {
    tab = '';


    $('#loaderarchive').show();
    $('#Archived_dataTable').hide();
    $.ajax({
        url: apiURL + '/jsondata/archived',
        type: "GET",
        dataType: 'json',
        headers: tokenaccess(),
        success: function(data) {
            archived = '';
            if (data == false) {
                $('#loaderarchive').hide();
                dt_pointera = tablecreate("#Archived_dataTable", dt_pointera, '');
                return;
            }
            for (var i = 0; i < data.length; i++) {
                var market = data[i].market;
                if (data[i].market.split(',').length > 1) {
                    market = '<span title="' + data[i].market + '"><img src="images/market.png" height="30px" width="30px" /	> </span>';

                }
                archived = archived + '<tr style="font-size:small;height:40px"><td>' +
                    '<a href="javascript:;" class="analytic_status" data-id="' + data[i].id.toString() + '" data-status="' + data[i].status.toString() + '" data-project="' + data[i].name +
                    '" data-launchdate="' + data[i].launch_date + '" data-exitdate="' + data[i].exit_date + '" data-projectcost="' + data[i].project_cost +
                    '" data-producername="' + data[i].producer_name + '" data-hostingplatform="' + data[i].hosting_platform + '" data-market="' + data[i].market +
                    '" data-estimate="' + data[i].EstimatedTime + '" data-startdate="' + data[i].development_start_date + '" data-Burndown="' + data[i].burndown +
                    '"><b>' + data[i].name + '</b></a></td>' +
                    '<td width="110px">' + check_null(data[i].launch_date) + '</td>' +
                    '<td width="110px"> ' + check_null(data[i].exit_date) + ' </td><td width="110px"><a  data-id="' + data[i].id + '" data-name="' + data[i].name + '" href="javascript:;" id="addEstimatebtn' + data[i].id + '" class="estimatebudget">DKK ' + data[i].project_cost + '</a></td>' +
                    '<td>' + data[i].producer_name + '</td><td>' + market + '</td>' +
                    '<td>' + data[i].hosting_platform + '</td>' +
                    '</tr>';
            }

            dt_pointera = tablecreate("#Archived_dataTable", dt_pointera, archived);
        },
        error: function(data) {
            errordisplay(data);
            $('#loaderarchive').hide();
        }
    }).done(function() {
        $('#Archived_dataTable').show();
        $('#loaderarchive').hide();
        if ($("a[aria-controls='Archived']").attr("aria-expanded") == "true") {
            tab = 'archived';
        }
    }).fail("errorarchived");
}

function getDataNewDevelopment() {
    tab = '';

    $('#loaderdev').show();
    $('#Process_dataTable').hide();
    $.ajax({
        url: apiURL + '/jsondata/development',
        type: "GET",
        dataType: 'json',
        headers: tokenaccess(),
        success: function(data) {
            if (data == false) {

                $('#loaderdev').hide();
                dt_pointerd = tablecreate("#Process_dataTable", dt_pointerd, '');
                return;
            }
            dev = '';

            for (var i = 0; i < data.length; i++) {
                var pgsColor;
                if (parseInt(data[i].progress, 10) == 100) {
                    pgsColor = '#4cae4c';
                    pgsTxt = 'Done';
                } else {
                    pgsColor = '#009dd8';
                }
                var cName = unescape(data[i].name);
                var market = data[i].market;
                if (data[i].market.split(',').length > 1) {
                    market = '<span title="' + data[i].market + '"><img src="images/market.png" height="30px" width="30px" /	> </span>';

                }
                var disStatus = 'block';
                if (!data[i].qa_url || data[i].qa_url == "#" || data[i].qa_url == "NA") {
                    disStatus = 'none';
                }
                dev = dev + '<tr style="font-size:small;height:40px">' +
                    '<td><a href="javascript:;" data-id="' + data[i].id.toString() + '" class="project_status" data-status="' + data[i].status.toString() + '" data-project="' + cName +
                    '" data-launchdate="' + data[i].launch_date + '" data-exitdate="' + data[i].exit_date + '" data-projectcost="' + data[i].project_cost +
                    '" data-producername="' + data[i].producer_name + '" data-hostingplatform="' + data[i].hosting_platform + '" data-market="' + data[i].market + '"data-estimate="' + data[i].EstimatedTime + '" data-startdate="' + data[i].development_start_date + '" data-Burndown="' + data[i].burndown + '"><b>' + cName + '</b></a></td>' +
                    '<td width="110px">' + check_null(data[i].development_start_date) + '</td><td width="110px">' + check_null(data[i].launch_date) + '</td><td width="110px"> ' + check_null(data[i].exit_date) + ' </td>' +
                    '<td width="110px"><a  data-id="' + data[i].id + '" data-name="' + data[i].name + '" href="javascript:;" id="addEstimatebtn' + data[i].id + '" class="estimatebudget">DKK ' + data[i].project_cost + '</a></td>' +
                    '<td class="burndown">' + select_img(data[i].burndown, data[i].progress) + '</td>' +
                    '<td>' + data[i].producer_name + '</td><td>' + market + '</td>' +
                    '<td>' + data[i].hosting_platform + '</td><td><div class="uk-progress uk-progress-striped uk-active" style="width:auto;"><div class="uk-progress-bar" style="width:' + parseInt(data[i].progress, 10) + '%; background-color:' + pgsColor + ';"></div></div></td>' +
                    '<td onclick="stop()"><a href="' + data[i].qa_url + '" target="_blank" style="text-decoration: none;font-weight:normal; onclick="stop()"><span class="glyphicon glyphicon-new-window" aria-hidden="true" style="display:' + disStatus + '" title="' + data[i].qa_url + '"> </span></a></td></tr>';
            }
            dt_pointerd = tablecreate("#Process_dataTable", dt_pointerd, dev);
        },
        error: function(data) {
            errordisplay(data);
            $('#loaderdev').hide();
        }
    }).done(function() {
        $('#Process_dataTable').show();
        $('#loaderdev').hide();
        if ($("a[aria-controls='Process']").attr("aria-expanded") == "true") {
            tab = 'development';
        }
    }).fail("errordevelopment");
}

function getDataNewEstimate() {
    tab = '';

    $('#loaderestimate').show();
    $('#Estimate_dataTable').hide();
    $.ajax({
        url: apiURL + '/jsondata/estimate',
        type: "GET",
        dataType: 'json',
        headers: tokenaccess(),
        success: function(data) {
            if ($("a[aria-controls='Estimate']").attr("aria-expanded") == "true") {
                tab = 'Estimate';
            }
            if (data == false) {

                $('#loaderestimate').hide();
                dt_pointere = tablecreate("#Estimate_dataTable", dt_pointere, '');
                return;
            }
            estmt = '';

            for (var i = 0; i < data.length; i++) {
                var market = data[i].market;
                if (data[i].market.split(',').length > 1) {
                    market = '<span title="' + data[i].market + '"><img src="images/market.png" height="30px" width="30px" /	> </span>';

                }
                estmt = estmt + '<tr style="font-size:small;height:40px">' +
                    '<td data-id="' + data[i].id.toString() + '" class="project_status"  data-status="' + data[i].status.toString() + '" data-project="' + data[i].name +
                    '" data-launchdate="' + data[i].launch_date + '" data-exitdate="' + data[i].exit_date + '" data-projectcost="' + data[i].project_cost +
                    '" data-producername="' + data[i].producer_name + '" data-hostingplatform="' + data[i].hosting_platform + '" data-market="' + data[i].market + '"data-estimate="' + data[i].EstimatedTime + '" data-startdate="' + data[i].development_start_date + '" data-Burndown="' + data[i].burndown +
                    '"><b>' + data[i].name + '</b></td>' +
                    '<td width="110px">' + check_null(data[i].due_date) + '</td><td width="110px">' + check_null(data[i].launch_date) + '</td><td width="110px"> ' + check_null(data[i].exit_date) + ' </td>' +
                    '<td width="110px"><a  data-id="' + data[i].id + '" data-name="' + data[i].name + '" href="javascript:;" id="addEstimatebtn' + data[i].id + '" class="estimatebudget">DKK ' + data[i].project_cost + '</a></td>' +
                    '<td>' + data[i].producer_name + '</td><td>' + market + '</td>' +
                    '<td>' + data[i].hosting_platform + '</td>' +
                    '<td><div class="btn-group">' +
                    '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="padding: 3px 3px;">Action</button>' +
                    '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="padding: 3px 5px;">' +
                    '<span class="caret"></span>' +
                    '<span class="sr-only">Toggle Dropdown</span>' +
                    '</button>' +
                    '<ul class="dropdown-menu dropdown-menu-position" style="right:0px;left:-113px;" role="menu">' +
                    '<li><a data-id="' + data[i].id + '" data-name="' + data[i].name + '" href="javascript:;" id="procurementBtn" class="procurementcmp">Add Procurement Details</a></li>' +
                    '<li><a data-id="' + data[i].id + '" data-name="' + data[i].name + '" href="javascript:;" id="addEstimatebtn' + data[i].id + '" class="addestimate">Add Estimate</a></li>' +
                    '<li><a data-tab="estimate" data-id="' + data[i].id + '" href="javascript:;" id="UpdateCampaign" class="updatecmp">Update Campaign</a></li>' +
                    '<li><a data-id="' + data[i].id + '" data-name="' + data[i].name + '" href="javascript:;" id="approve-' + data[i].id + '" class="approveEstimate" onclick=approveEstimate("' + data[i].id + '","' + data[i].name + '")>Approve</a></li>' +
                    '<li><a href="javascript:;" id="referback">Refer Back</a></li>' +
                    '<li><a data-id="' + data[i].id + '" data-name="' + data[i].name + '" href="./featureng/features.html#/?id=' + data[i].id + '&tab=' + tab +
                    '" id="featurebtn">More Details</a></li>' +
                    '</ul>' +
                    '</div></td>';
            }
            dt_pointere = tablecreate("#Estimate_dataTable", dt_pointere, estmt);
        },
        error: function(data) {
            errordisplay(data);
            $('#loaderestimate').hide();
        }
    }).done(function() {
        $('#loaderestimate').hide();
        $('#Estimate_dataTable').show();

    }).fail("errorestimate");
}

function SiteDown(str) {

    if (str == true) {
        return 'Down';
    } else {
        return 'Running';
    }
}

function site_down_color(str) {

    if (str == true) {
        return 'label-danger down-btn';
    } else {
        return 'label-success running-btn';
    }

}

function dateformat(date) {
    if (date != null) {
        var dateparse = /(\d\d\d\d-\d+-\d+).*/.exec(date);
        if (dateparse.length > 0) {
            return dateparse[1];
        } else {
            return ""
        }
    } else {
        return ""
    };

}

function check_null(str) {
    if ((str == null) || (str == undefined)) {
        str = ' ';
        return str;
    } else {
        str = dateformat(str);
        return str;
    }
}

function select_img(burndown, progress) {
    var image;

    switch (burndown) {

        case true:
            image = '<img src="images/no.png">';
            break;
        case false:
            image = '<img src="images/yes.png">';
            break;
        default:
            break;
    }
    return image;
}

function stop() {
    event.stopPropagation();
}
//---------------------------------------------------------------------------------------------------------------------------------------
//phase representation dashboard
function status_display(project_status, p_name) {

    var project_name = unescape(p_name);

}

function stepOne(project_name) {
    $('#current-campaign').empty();
    $('#current-campaign').append(project_name);
    $(window).scrollTop($('#status-panel').offset().top);
    $('.circle-image').removeClass('active');
    $('.circle-image').addClass('disabled-server');
    $(this).removeClass('disabled-server');
    $(this).addClass('enabled-server');
    $('div#step_one .circle-image').addClass('active');
}

function stepTwo(project_name) {
    $('#current-campaign').empty();
    $('#current-campaign').append(project_name);
    $(window).scrollTop($('#status-panel').offset().top);
    $('.circle-image').removeClass('active');
    $('.circle-image').addClass('disabled-server');
    $(this).removeClass('disabled-server');
    $(this).addClass('enabled-server');
    $('div#step_two .circle-image').addClass('active');
}

function stepThree(project_name) {
    $('#current-campaign').empty();
    $('#current-campaign').append(project_name);
    $(window).scrollTop($('#status-panel').offset().top);
    $('.circle-image').removeClass('active');
    $('.circle-image').addClass('disabled-server');
    $(this).removeClass('disabled-server');
    $(this).addClass('enabled-server');
    $('div#step_three .circle-image').addClass('active');
}

function stepFour(project_name) {
    $('#current-campaign').empty();
    $('#current-campaign').append(project_name);
    $(window).scrollTop($('#status-panel').offset().top);
    $('.circle-image').removeClass('active');
    $('.circle-image').addClass('disabled-server');
    $(this).removeClass('disabled-server');
    $(this).addClass('enabled-server');
    $('div#step_four .circle-image').addClass('active');
}
//phase representation accordion
function status_displayAcc(project_status, project_name) {
    switch (project_status) {
        case 'Development':
            stepTwoAcc(project_name);
            break;
        case 'Testing':
            stepThreeAcc(project_name);
            break;
        case 'Design':
            stepOneAcc(project_name);
            break;
        case 'Live':
            stepFourAcc(project_name);
            break;
        default:
            break;
    }
}

function stepOneAcc(project_name) {
    $('#current-campaign').empty();
    $('#current-campaign').append(project_name);
    $('.circle-image').removeClass('active');
    $('.circle-image').addClass('disabled-server');
    $(this).removeClass('disabled-server');
    $(this).addClass('enabled-server');
    $('div#step_one .circle-image').addClass('active');
}

function stepTwoAcc(project_name) {
    $('#current-campaign').empty();
    $('#current-campaign').append(project_name);
    $('.circle-image').removeClass('active');
    $('.circle-image').addClass('disabled-server');
    $(this).removeClass('disabled-server');
    $(this).addClass('enabled-server');
    $('div#step_two .circle-image').addClass('active');
}

function stepThreeAcc(project_name) {
    $('#current-campaign').empty();
    $('#current-campaign').append(project_name);
    $('.circle-image').removeClass('active');
    $('.circle-image').addClass('disabled-server');
    $(this).removeClass('disabled-server');
    $(this).addClass('enabled-server');
    $('div#step_three .circle-image').addClass('active');
}

function stepFourAcc(project_name) {
    $('#current-campaign').empty();
    $('#current-campaign').append(project_name);
    $('.circle-image').removeClass('active');
    $('.circle-image').addClass('disabled-server');
    $(this).removeClass('disabled-server');
    $(this).addClass('enabled-server');
    $('div#step_four .circle-image').addClass('active');
}

function page1() {
    $('#table-body').empty();
    $("#table-body tr:even").css("background-color", "#fff");
    $("#table-body tr:odd").css("background-color", "#f8f8f8");
}

function GetDate(str) {
    var arr = str.split("-");
    var months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
    var month = months.indexOf(arr[1].toLowerCase());
    return new Date(parseInt(arr[2]), month, parseInt(arr[0]));
}

function compareDate(currdate, livedate) {
    if (currdate > livedate) {
        return "Archived";
    }
    if (currdate < livedate) {
        return "process";
    }
}

function closeStatusRow(btn) {
    $(btn).closest('tr.pro_status').remove();
}

function escapeclose(str) {
    $(str).modal({
        keyboard: false,
        backdrop: 'static'
    });
}


$(document).on('click', '.updatecmp', function(ele) {
    var dataEle = null;
    if ($(ele.target).data('tab') == 'estimate') {
        $('#startdategroup').hide();
        dataEle = $(ele.target).closest("tr").find("td:eq( 0 )");
    } else {
        $('#startdategroup').show();
        $('#fldStartDate').prop('disabled', true);
        dataEle = $(ele.target).closest("tr").prev().find("td:eq( 0 )").find("a");
    }
    var thisId = $(dataEle).data('id');
    var thisName = $(dataEle).data('project');
    var thisExitdate = $(dataEle).data('exitdate');
    var thisLaunchdate = $(dataEle).data('launchdate');
    var thisProducername = $(dataEle).data('producername');
    var thisMarket = $(dataEle).data('market');
    var thisPlatform = $(dataEle).data('hostingplatform');
    var thisEstimate = $(dataEle).data('estimate');
    var thisStartdate = $(dataEle).data('startdate');
    var thisBurndown = $(dataEle).data('Burndown');
    var thisProjectcost = $(dataEle).data('projectcost');
    $('#updatecampaignform').modal({
        backdrop: 'static',
        keyboard: false
    });
    resetform("updatecampaignform");
    $("#fldLaunchDate").datepicker("option", "maxDate", null);
    $("#fldExit").datepicker("option", "minDate", 1);

    fillCampaignForm(thisId, thisName, thisExitdate, thisLaunchdate, thisStartdate, thisProducername, thisMarket, thisPlatform, thisBurndown, thisEstimate, thisProjectcost);
    $('#updateloader').hide();
});
$(document).on('click', '.procurementcmp', function() {
    $('#procurementform').modal({
        backdrop: 'static',
        keyboard: false
    });
    resetform("procurementform");
    $('#procurementform').find('#hdnProcurementProjectId').val($(this).data('id'));
    $('#procurementform').find('#hdnProcurementProjectName').val($(this).data('name'));
});
$(document).on('click', 'a.project_status', function(e) {
    e.preventDefault();
    $('#status_popup').modal({
        backdrop: 'static',
        keyboard: false
    });
    /*  if ($("a[aria-controls='Estimate']").attr("aria-expanded") == "true") {
         return;
     } */
    var project = {};
    project.name = $(this).data('project');
    project.status = $(this).data('status');
    project.statusdata = $('#status-Design').html();
    project.id = $(this).data('id');

    var statusTr =
        '<div class="col-md-12 col-xs-12 proheading">' + project.name +
        '  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div>' +
        '<div class="col-md-6 col-xs-12 sHeading">' +
        '<h4>Pipeline Overview</h4>' +
        '<div class="col-md-12">' + project.statusdata + '</div></div>' +
        '<div class="col-md-6 col-xs-12 sHeading">' +
        '<h4>Development Status</h4>' +
        '<div class="actioncenter">' +
        '<div class="btn-group">' +
        '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action</button>' +
        '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">' +
        '<span class="caret"></span>' +
        '<span class="sr-only">Toggle Dropdown</span>' +
        '</button>' +
        '<ul class="dropdown-menu dropdown-menu-position" style="right:0px;left:-113px;" role="menu">' +
        '<li><a data-id="' + project.id + '" data-name="' + project.name + '" href="javascript:;"  class="procurementcmp" id="procurementBtn">Add Procurement Details</a></li>' +
        '<li><a data-id="' + project.id + '" href="javascript:;" id="UpdateCampaign" class="updatecmp" >Update Campaign</a></li>' +
        '<li><a data-id="' + project.id + '" data-name="' + project.name + '" href="javascript:;"  id="addEstimatebtn' + project.id + '" class="addestimate">View Estimate</a></li>' + '<li><a data-id="' + project.id + '" data-name="' + project.name + '" href="javascript:;" id="deploymentbtn">Add Deployment Request</a></li>' +
        '<li><a href="javascript:;" id="SecurityScan">Start Security Scan</a></li>' +
        '<li><a data-id="' + project.id + '" data-name="' + project.name + '" href="./featureng/features.html#/?id=' + project.id + '&tab=' + tab +
        '" id="featurebtn">More Details</a></li>' +
        '</ul>' +
        '</div>' +
        '</div>' +
        '<div style="padding:5px;"><div id="graph-legend-' + project.id + '"></div><canvas id="canvas-' + project.id + '"   width="300px" height="200px"></canvas></div>' +
        '</div></div><div style="clear:both"></div>';
    $('#status-modal').html(statusTr);
    drawPieChart(project.name, project.id);
    drawBugChart(project.name, project.id);
    $('#deploymentbtn').click(function() {
        $('#deploymentform').modal({
            backdrop: 'static',
            keyboard: false
        });
        resetform("deploymentform");
        $('#deploymentform').find('#hdnDeploymentProjectId').val($(this).data('id'));
        $('#deploymentform').find('#hdnDeploymentProjectName').val($(this).data('name'));
    });
    $('#SecurityScan').click(function() {
        $('#SecurityohSnap').modal({
            backdrop: 'static',
            keyboard: false
        });


    });
});
$(document).on('click', 'a.analytic_status', function(e) {
    e.preventDefault();
    $('#status_popup').modal({
        backdrop: 'static',
        keyboard: false
    });
    var project = {};
    project.name = $(this).data('project');
    project.status = $(this).data('status');
    project.statusdata = $('#status-Design').html();
    project.id = $(this).data('id');
    var statusTr =
        '<div class="col-md-12 col-xs-12 proheading">' + project.name +
        ' <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div>' +
        '<div class="col-md-6 col-xs-12 sHeading">' +
        '<h4>Pipeline Overview</h4>' +
        '<div class="col-md-12">' + project.statusdata + '</div></div>' +
        '<div class="col-md-6 col-xs-12 sHeading">' +
        '<h4>Analytics Status</h4>' +
        '<div class="actioncenter">' +
        '<div class="btn-group">' +
        '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action</button>' +
        '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">' +
        '<span class="caret"></span>' +
        '<span class="sr-only">Toggle Dropdown</span>' +
        '</button>' +
        '<ul class="dropdown-menu dropdown-menu-position" style="right:0px;left:-113px;" role="menu">' +
        '<li><a data-id="' + project.id + '" data-name="' + project.name + '" href="javascript:;"  class="procurementcmp" id="procurementBtn">Add Procurement Details</a></li>' +
        '<li><a data-id="' + project.id + '" href="javascript:;" id="UpdateCampaign" class="updatecmp" >Update Campaign</a></li>' +
        '<li><a data-id="' + project.id + '" data-name="' + project.name + '" href="javascript:;"  id="addEstimatebtn' + project.id + '" class="addestimate">View Estimate</a></li>' + '<li><a data-id="' + project.id + '" data-name="' + project.name + '" href="javascript:;" id="deploymentbtn">Add Deployment Request</a></li>' +
        '<li><a href="javascript:;" id="SecurityScan">Start Security Scan</a></li>' +
        '<li><a data-id="' + project.id + '" data-name="' + project.name + '" href="./featureng/features.html#/?id=' + project.id + '&tab=' + tab +
        '" id="featurebtn">More Details</a></li>' +
        '</ul>' +
        '</div>' +
        '</div>' +
        '<div style="padding:5px;"><div id="graph-legend-' + project.id + '"></div><canvas id="canvas-' + project.id + '"   width="300px" height="200px"></canvas></div>' +
        '</div></div><div style="clear:both"></div>';
     $('#status-modal').html(statusTr);
    drawPieChart(project.name, project.id);
    drawAnalyticsChart(project.name, project.id);
    $('#deploymentbtn').click(function() {
        $('#deploymentform').modal({
            backdrop: 'static',
            keyboard: false
        });
        resetform("deploymentform");
        $('#deploymentform').find('#hdnDeploymentProjectId').val($(this).data('id'));
        $('#deploymentform').find('#hdnDeploymentProjectName').val($(this).data('name'));
    });
    $('#SecurityScan').click(function() {
        $('#SecurityohSnap').modal({
            backdrop: 'static',
            keyboard: false
        });
    });
});

/* Graphs*/
function drawBugChart(projectName, projectid) {
    var projectName = projectName;
    var projectid = projectid;
    $.ajax({
        url: apiURL + "/issue/" + projectid,
        type: "GET",
        dataType: 'json',
        headers: tokenaccess(),
        success: function(data) {
            if (projectName.length > 15) projectName = projectName.substring(0, 15) + '...';
            var barChartData = {
                labels: ["Feature", "Bug", "Feedback", "Test Case", "Test Scenarios"],
                datasets: [{
                    label: "Open",
                    fillColor: "rgba(237, 89, 78,0.5)",
                    strokeColor: "rgba(237, 89, 78,0.8)",
                    highlightFill: "rgba(237, 89, 78,0.75)",
                    highlightStroke: "rgba(237, 89, 78,1)",
                    data: [data.total_feature_New, data.total_bug_New, data.total_feedback_New, data.total_testScenarios_New, data.total_testcase_New]
                }, {
                    label: "Close",
                    fillColor: "rgba(78, 226, 236,0.5)",
                    strokeColor: "rgba(78, 226, 236,0.8)",
                    highlightFill: "rgba(78, 226, 236,0.75)",
                    highlightStroke: "rgba(78, 226, 236,1)",
                    data: [data.total_feature_Closed, data.total_bug_Closed, data.total_feedback_Closed, data.total_testScenarios_Closed, data.total_testcase_Closed]
                }]
            }

            var ctx = $("#canvas-" + projectid)[0].getContext("2d");
            console.log(ctx);
            var chart = new Chart(ctx).Bar(barChartData, {
                responsive: true,
                barStrokeWidth: 2,
                //Number - Spacing between each of the X value sets
                barValueSpacing: 5,
                //Number - Spacing between data sets within X values
                barDatasetSpacing: 1,
                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li style=\"list-style:none;display:inline\">&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"background-color:<%=datasets[i].fillColor%>;height:10px;width:20px;display:inline-block\">  </span>&nbsp;&nbsp;<%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
            });
            $("#graph-legend-" + projectid).html(chart.generateLegend());
        },
        error: function(data) {
            errordisplay(data);
        }
    });
}

function drawAnalyticsChart(projectName, projectid) {
    var projectName = projectName;
    var projectid = projectid;
    $.ajax({
        url: "http://10.97.85.76:8099/api/Analytics/SiteHit_Analytics?Projid=319&fromDate=2015-07-01&toDate=2015-07-30&limit=10",
        type: "GET",
        dataType: 'json',

        success: function(data) {
            data = JSON.parse(data);
            console.log(data.length);
            if (data.length != 0) {
                if (projectName.length > 15) projectName = projectName.substring(0, 15) + '...';
                var barChartData = {
                    labels: [], //data[0].Campaign_Date,data[1].Campaign_Date,data[2].Campaign_Date,data[3].Campaign_Date,data[4].Campaign_Date,data[5].Campaign_Date,data[6].Campaign_Date,data[7].Campaign_Date,data[8].Campaign_Date,data[9].Campaign_Date],
                    datasets: [{
                        label: "Visitors",
                        fillColor: "rgba(237, 89, 78,0.5)",
                        strokeColor: "rgba(237, 89, 78,0.8)",
                        highlightFill: "rgba(237, 89, 78,0.75)",
                        highlightStroke: "rgba(237, 89, 78,1)",
                        data: [] //data[0].Campaign_Hits,data[1].Campaign_Hits,data[2].Campaign_Hits,data[3].Campaign_Hits,data[4].Campaign_Hits,data[5].Campaign_Hits,data[6].Campaign_Hits,data[7].Campaign_Hits,data[8].Campaign_Hits,data[9].Campaign_Hits]
                    }]
                }

                var ctx = $("#canvas-" + projectid)[0].getContext("2d");
                var chart = new Chart(ctx).Line(barChartData, {
                    responsive: true,
                    barStrokeWidth: 2,
                    //Number - Spacing between each of the X value sets
                    barValueSpacing: 5,
                    //Number - Spacing between data sets within X values
                    barDatasetSpacing: 1,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li style=\"list-style:none;display:inline\">&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"background-color:<%=datasets[i].fillColor%>;height:10px;width:20px;display:inline-block\">  </span>&nbsp;&nbsp;<%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
                });
                for (var i = 0; i < data.length; i++) {

                    chart.addData([data[i].Campaign_Hits], data[i].Campaign_Date)
                }
                $("#graph-legend-" + projectid).html(chart.generateLegend());
            } else {
                ohSnap("data not available",'yellow');
            }
        },
        error: function(data) {
            errordisplay(data);
        }
    });
}

function drawPieChart(projectName, projectid) {
    var projectName = projectName;
    var projectid = projectid;




    if (projectName.length > 15) projectName = projectName.substring(0, 15) + '...';
    var pieData = [{
            value: 300,
            color: "#F7464A",
            highlight: "#FF5A5E",
            label: "Red"
        }, {
            value: 50,
            color: "#46BFBD",
            highlight: "#5AD3D1",
            label: "Green"
        }, {
            value: 100,
            color: "#FDB45C",
            highlight: "#FFC870",
            label: "Yellow"
        }, {
            value: 40,
            color: "#949FB1",
            highlight: "#A8B3C5",
            label: "Grey"
        }, {
            value: 120,
            color: "#4D5360",
            highlight: "#616774",
            label: "Dark Grey"
        }

    ];



    var ctx = document.getElementById("chart-area").getContext("2d");
    var ch = new Chart(ctx).Pie(pieData, {
        responsive: true,
        segmentShowStroke: true,
        segmentStrokeWidth: 2
    });
    console.log(ctx);
    console.log(ch);

}
/* Graphs end*/

var i = Math.floor((Math.random() * 100000) + 1);
/* Action button functionality*/
function post() {
    if ($('#camp_name').val() == "") {
        //ohSnap("Campaign name is mandatory",'yellow');
        $("#camp_name").css("border-color", "red");
        return false;
    }
    if ($('#market').val() == "") {
       // ohSnap("Market is mandatory",'yellow');
        $("#market").css("border-color", "red");
        return false;
    }
    if ($('#producer').val() == "") {
        //ohSnap("Producer is mandatory",'yellow');
        $("#producer").css("border-color", "red");
        return false;
    }
    $('#newrequest').modal('hide');
    $('.divOverlay').show();
    var currdate = new Date();
    var obj = {};
    var identifier = $('#camp_name').val().replace(/ /g, "") + '-' + i;
    obj.identifier = identifier.toLowerCase();
    i++;
    var str = '{"name":"' + $('#camp_name').val().trim() + '","description":"' + $('#description').val() + '","created_on":"' +
        currdate + '","response_put":' + response_put_f + ',"parent_id":' + parent_id + ',"id":' +
        $('#procurementform').find('#hdnProcurementProjectId').val() + ',"identifier":"' + obj.identifier +
        '","custom_fields":[{ "id":2,"name":"Launch Date","value":"' + $('#launch_date').val() + '"},' +
        '{"id":35,"name":"Exit Date","value":"' + $('#exit_date').val() + '"},' +
        '{"id":45,"name":"Acronis Link","value":"Campaign Factory"},' +
        '{"id":16,"name":"Project Type","value":"Campaign Factory" },' +
        '{"id":31,"name":"Live URL","value":"' + $('#url').val() + '" },' +
        '{"id":28,"name":"Producer","value":"' + $('#producer').val().trim() + '" },' +
        '{"id":17,"name":"Project Technology","value":"' + $('#host_plat').val() + '"},' + '{"id":29,"name":"Market","value":"' +
        $('#market').val().trim() + '"},{"id":3,"name":"Estimates Shared with Client","value":"' + $('#estimate').val() +
        '"},{"id":60,"name":"Due Date","value":"' + $('#due_date').val() + '"}]}';
    var options = {};
    options.url = apiURL + "/Project"; // "http://localhost:56403/api/project";//" "http://10.97.85.76:8090/api/project"; // 
    options.type = "POST";
    options.data = str;
    options.dataType = "json";
    options.headers = tokenaccess();
    options.contentType = "application/json";
    options.success = function(data) {
        $('#newrequest').modal("hide");
        $('.divOverlay').hide();
        ohSnap(data,'green');
        refreshdata();
    };
    options.error = function(data) {
        errordisplay(data);
        $('.divOverlay').hide();
    };
    $.ajax(options);
}

function refreshdata() {
    getDataNewEstimate();
    getDataNewArchived();
    getDataNewDevelopment();
    getDataNewLive();
};

function deploypost() {
    if ($('#deployment_date').val() == "") {
        //ohSnap("Deployment date is mandatory",'yellow');
        return false;
    }
    if ($('#deployment_slot').val() == "") {
        //ohSnap("Deployment Slot (IST) is mandatory",'yellow');
        return false;
    }
    if ($('#environment').val() == "") {
       // ohSnap("Environment is mandatory",'yellow');
        return false;
    }
    $('#deploymentform').modal("hide");
    $('.divOverlay').show();
    var priorityid = $("#priority").val();
    var str = '{"name":"' + $('#deploymentform').find('#hdnDeploymentProjectName').val() +
        '","response_put":' + response_put + ',"parent_id":' + parent_id + ',"priority_id":' + (priorityid == 'Low' ? '1' : '2') + ',"project_id":' + project_id + ',' +
        '"tracker_id":' + tracker_id + ',"status_id":' + status_id + ',"category_id":' + category_id + ',' +
        '"fixed_version_id":' + fixed_version_id + ',"parent_issue_id":' + parent_issue_id + ',"assigned_to_id":' + assigned_to_id + ',"id":' +
        $('#deploymentform').find('#hdnDeploymentProjectId').val() + ',"subject":"Name' + $('#deploymentform').find('#hdnDeploymentProjectName').val() + 'and id is' +
        $('#deploymentform').find('#hdnDeploymentProjectId').val() + '","is_private":' + is_private + ',"estimated_hours":' + estimated_hours + ',"description":"test description","due_date":"' +
        $('#deploymentform').find('#deployment_date').val() + '","custom_fields":[{ "id":' + deployment_environment_id + ',"name":"Environment","value":"' + $('#environment').val() + '"},' +
        '{"id":' + deployment_slot_id + ',"name":"Deployment Slot (IST)","value":"' + $('#deployment_slot').val() + '"}]}';

    var options = {};
    options.url = apiURL + "/copymaster";
    options.type = "POST";
    options.data = str;
    options.dataType = "json";
    options.headers = tokenaccess();
    options.contentType = "application/json";
    options.success = function() {
        $('#deploymentform').modal("hide");
        ohSnap("Data added successfully",'green');
        $('.divOverlay').hide();
    };
    options.error = function(data) {
        errordisplay(data);
        $('.divOverlay').hide();
    };
    $.ajax(options);
}

function addProcurement() {
    /* if ($('#Proc_AWS').val() == "") {
        ohSnap("AWS Account Link is mandatory");
        return false;
    }
    if ($('#Proc_Username').val() == "") {
        ohSnap("Username is mandatory");
        return false;
    }
    if ($('#Proc_Pass').val() == "") {
        ohSnap("Password is mandatory");
        return false;
    } */
    $('#procurementform').modal("hide");
    $('.divOverlay').show();
    var str = '{"name":"' + $('#procurementform').find('#hdnProcurementProjectName').val() + '","response_put":' + response_put + ',"parent_id":' + parent_id + ',"id":' + $('#procurementform').find('#hdnProcurementProjectId').val() + ',' +
        '"custom_fields":[{ "id":' + procurement_aws_id + ',"name":"Aws Account Link","value":"' + $('#Proc_AWS').val() + '"},' +
        '{"id":' + procurement_name_id + ',"name":"User Name","value":"' + $('#Proc_Username').val() + '"},' +
        '{"id":' + procurement_password_id + ',"name":"Password","value":"' + $('#Proc_Pass').val() + '"},' +
        '{"id":' + procurement_livecname_id + ',"name":"Live CName","value":"' + $('#Proc_Live_Cname').val() + '" },' +
        '{"id":' + procurement_liveelastic_id + ',"name":"Live Elastic IP","value":"' + $('#Proc_ELastic_IP').val() + '" },' +
        '{"id":' + procurement_QAcname_id + ',"name":"QA CName","value":"' + $('#Proc_QA_Cname').val() + '" },' +
        '{"id":' + procurement_QAelastic_id + ',"name":"QA Elastic IP","value":"' + $('#Proc_QA_Elastic_IP').val() + '"}]}';
    var options = {};
    options.url = apiURL + "/project"; // "http://localhost:56403/api/project"; // "http://10.97.85.76:8090/api/project"; // 
    options.type = "POST";
    options.data = str;
    options.dataType = "json";
    options.headers = tokenaccess();
    options.contentType = "application/json";
    options.success = function() {
        $('#procurementform').modal("hide");

        ohSnap("Procurement Details added successfully",'green');
        $('.divOverlay').hide();
    };
    options.error = function(data) {
        errordisplay(data);
        $('.divOverlay').hide();
    };
    $.ajax(options);
}

function resetform(formId) {
    $("#" + formId).find("input[type='text'] , input[type='email'], input.form-control, textarea").each(function() {
        $(this).val('').css("border-color", "");
    });
    $("#" + formId).find("select").each(function() {
        $(this).css("border-color", "");
        $(this).val('');
    });
}
function fillCampaignForm(id, name, exitdate, launchdate, startdate, producername, market, platform, burndown, estimate, projectcost) {
    $('#updateloader').show();
    $("#fldCampaignName").val(name);
    startdate = dateformat(startdate);
    $("#fldStartDate").val(startdate);
    launchdate = dateformat(launchdate);
    $("#fldLaunchDate").val(launchdate);
    exitdate = dateformat(exitdate);
    $("#fldExit").val(exitdate);
    $("#fldEstimate").val(estimate);
    $("#fldProducer").val(producername);
    $("#fldMarket").val(market);
    $('#fldPlatform').find("option").each(function() {
        if ($(this).text() == platform) {
            this.selected = true;
        }
    });
    $("#projectId").val(id);
}
function updateCampaign() {
    if ($('#fldCampaignName').val() == "") {
        //ohSnap("Campaign Name is mandatory",'yellow');
        return false;
    }
    if ($('#fldMarket').val() == "") {
       // ohSnap("Market is mandatory",'yellow');
        $("#fldMarket").css("border-color", "red");
        return false;
    }
    if ($('#fldProducer').val() == "") {
        //ohSnap("Producer is mandatory",'yellow');
        return false;
    }
    $('.divOverlay').show();
    var str = '{"name":"' + $('#fldCampaignName').val() + '","response_put":' + response_put + ',"parent_id":' + parent_id + ',"id":' + $('#projectId').val() + ',' +
        '"custom_fields":[{ "id":' + development_startdate_id + ',"name":"Development Start Date","value":"' + $('#fldStartDate').val() + '"},' +
        '{"id":' + launchdate_id + ',"name":"Launch Date","value":"' + $('#fldLaunchDate').val() + '"},' +
        '{"id":' + exitdate_id + ',"name":"Exit Date","value":"' + $('#fldExit').val() + '"},' +
        '{"id":' + estimates_id + ',"name":"Estimates Shared with Client","value":' + $('#fldEstimate').val() + ' },' +
        '{"id":' + producer_id + ',"name":"Producer","value":"' + $('#fldProducer').val() + '" },' +
        '{"id":' + market_id + ',"name":"Market","value":"' + $('#fldMarket').val() + '" },' +
        '{"id":' + project_tech_id + ',"name":"Project Technology","value":"' + $('#fldPlatform').val() + '" }]}';
    var options = {};
    options.url = apiURL + "/project"; //"http://10.97.85.76:8090/api/jsondata"; //"http://localhost:56403/api/project"; // 
    options.type = "POST";
    options.data = str;
    options.dataType = "json";
    options.headers = tokenaccess();
    options.contentType = "application/json";
    options.success = function() {
        $('#updatecampaignform').modal("hide");
        ohSnap("Campaign updated successfully",'green');
        refreshdata();
        $('.divOverlay').hide();
    };
    options.error = function(data) {
        errordisplay(data);
        $('.divOverlay').hide();
    };
    $.ajax(options);
}

/*Action functionality Ends*/
var specialKeys = new Array();
specialKeys.push(8); //Backspace
function IsNumeric(e) {
    var keyCode = e.which ? e.which : e.keyCode
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
    document.getElementById("error").style.display = ret ? "none" : "block";
    document.getElementById("estimate").style.border = ret ? "1px solid #ccc" : "1px solid red";

    return ret;
}
$(function() {
    $('#launch_date').datepicker({
        minDate: 0,
        dateFormat: 'yy-mm-dd',
        onSelect: function(selected) {
            var dt = new Date(selected);
            var dt1 = new Date(selected);
            $("#due_date ").datepicker("option", "maxDate", dt1);
            dt.setDate(dt.getDate() + 1);
            $("#exit_date").datepicker("option", "minDate", dt);
        }
    });

    $('#exit_date').datepicker({
        minDate: 0,
        dateFormat: 'yy-mm-dd',
        onSelect: function(selected) {
            var dta = new Date(selected);
            dta.setDate(dta.getDate() - 1);
            $("#launch_date ").datepicker("option", "maxDate", dta);
            $("#due_date").datepicker("option", "maxDate", dta);
        }
    });

    $('#due_date').datepicker({
        minDate: 0,
        dateFormat: 'yy-mm-dd',
        onSelect: function(selected) {
            var dta = new Date(selected);

            $("#launch_date ").datepicker("option", "minDate", dta);
        }
    });

    $('#fldLaunchDate').datepicker({
        minDate: 0,
        dateFormat: 'yy-mm-dd',
        onSelect: function(selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#fldExit").datepicker("option", "minDate", dt);
        }
    });
    $('#fldExit').datepicker({
        minDate: 0,
        dateFormat: 'yy-mm-dd',
        onSelect: function(selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#prod_date ").datepicker("option", "maxDate", dt);
        }
    });
    $('#fldStartDate').datepicker({
        minDate: 0,
        dateFormat: 'yy-mm-dd'
    });
    $('#deployment_date').datepicker({
        minDate: 0,
        dateFormat: 'yy-mm-dd'
    });
    $('#fldLaunchDate').datepicker({
        minDate: 0,
        dateFormat: 'yy-mm-dd'
    });
});
$(function() {
    // Autocomplete
    var availableMarket = [
        "Europe",
        "Asia",
    ];
    $(".availableMarket").autocomplete({
        source: availableMarket
    });
    var availablePlatforms = [
        "Static",
        "PimCore",
        "SiteCore",
    ];
    $(".availablePlatforms").autocomplete({
        source: availablePlatforms
    });
    var availableProducers = [
        "Mia Jappe",
        "Jeanett Buch",
        "Nisha",
    ];
    $(".availableProducers").autocomplete({
        source: availableProducers
    });
    var burndownVal = $("td").find(".burndown").html();
});

function getUrlVars() {
    var vars = [],
        hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function checkNum(e, id) {
    var keyCode = e.which ? e.which : e.keyCode;
    var ret = ((keyCode > 64 && keyCode < 91) || (keyCode > 96 && keyCode < 123) || keyCode == 8 || specialKeys.indexOf(keyCode) != -1 || keyCode == 32);
    document.getElementById("error2").style.display = ret ? "none" : "block";
    document.getElementById(id).style.border = ret ? "1px solid #ccc" : "1px solid red";
    return ret;
}

function checkSpace(e, id) {
    var keyCode = e.which ? e.which : e.keyCode;
    var ret = (specialKeys.indexOf(keyCode) != -1);
    document.getElementById("error3").style.display = ret ? "block" : "none";
    document.getElementById(id).style.border = ret ? "1px solid red" : "1px solid #ccc";
    return !ret;
}

function checkchar(e, id) {
    var keyCode = e.which ? e.which : e.keyCode;
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
    document.getElementById("error2").style.display = ret ? "none" : "block";
    document.getElementById(id).style.border = ret ? "1px solid #ccc" : "1px solid red";
    return ret;
}

function colorchange(x) {
    x.style.border = "1px solid #ccc";
}
/* Approve functionality*/
function initUpdate() {
    $("#tab-estdata").attr("aria-expanded", "true");
    $("#estimate-tab .est-d").addClass("active");
    $("#estimate_main").addClass("active");
    $("#tab-comment ").attr("aria-expanded", "false");
    $("#estimate-tab .est-c").removeClass("active");
    $("#comment_estimate").removeClass("active");

}

$(document).on('click', '.addestimate ,.estimatebudget', function() {
    $('#estimateform').modal({
        backdrop: 'static',
        keyboard: false
    });
    initUpdate();
    resetform("estimateform");
    $(".estHeading").html("<span style='color:#4CAE4C;font-weight:bold;font-size:16pt;'>" + $(this).data('name') + "</span> : Estimates");
    $('#estimateform').find('#estprojectName').val($(this).data('name'));
    $('#estimateform').find('#estprojectId').val($(this).data('id'));
    $('#estimateform').find('#estheading').val($(this).data('id'));
    populateEstimatesForm();
});
function refreshRate() {
    var rateCost = $("#estRateCost").val();
    var obj = $(".calc");
    var sum = 0;
    for (var i = 0; i < obj.length; i++) {
        var cost = obj[i].value * rateCost;
        $("#" + $(obj[i]).attr("id") + "Cost").val(cost);
        sum += obj[i].value == '' ? 0 : parseInt(obj[i].value);
    }
    var totalCost = sum * rateCost;
    $("#esttotal").val(sum);
    $("#esttotalCost").val(totalCost);
    var riskPercentage = $("#estrisk").val();
    riskPercentage = riskPercentage == '' ? 0 : riskPercentage;
    $("#estriskCost").val(totalCost * parseInt(riskPercentage) / 100);
}
function calculateEstimateCost(e) {
    var rateCost = $("#estRateCost").val();
    if ($(e).attr("id") != "estrisk") {
        $("#" + $(e).attr("id") + "Cost").val($(e).val() * rateCost);
    }
    var obj = $(".calc");
    var sum = 0;
    for (var i = 0; i < obj.length; i++) {
        sum += obj[i].value == '' ? 0 : parseInt(obj[i].value);
    }
    var totalCost = sum * rateCost;
    $("#esttotal").val(sum);
    $("#esttotalCost").val(totalCost);
    var riskPercentage = $("#estrisk").val();
    riskPercentage = riskPercentage == '' ? 0 : riskPercentage;
    $("#estriskCost").val(totalCost * parseInt(riskPercentage) / 100);
}
function populateEstimatesForm() {

    $('.divOverlay').show();

    $("#estRate").val("1");
    $("#estRateCost").val("315");
    $("#estId").val("0");
    var p_id = $('#estprojectId').val();
    var p_name = $('#estprojectName').val();

    $.ajax({
        url: apiURL + "/estimate?pid_Id=" + project_pid + "&pid_Value=" + p_id, //" + p_id,
        type: "GET",
        dataType: 'json',
        headers: tokenaccess(),
        success: function(data) {

            if (data.length != 0) {

                $("#estId").val(data[0].id);
                $("#estRate").val(data[0].custom_fields[0].value);
                $("#estRateCost").val(data[0].custom_fields[1].value);
                $("#estProjectMgr").val(data[0].custom_fields[2].value);
                $("#estUX").val(data[0].custom_fields[3].value);
                $("#estDev3").val(data[0].custom_fields[4].value);
                $("#estQA").val(data[0].custom_fields[5].value);
                $("#estSetup").val(data[0].custom_fields[6].value);
                $("#estoutofpocket").val(data[0].custom_fields[7].value);

                $("#estAccount").val(data[0].custom_fields[9].value);
                $("#estProducer").val(data[0].custom_fields[10].value);
                $("#estDesign").val(data[0].custom_fields[11].value);
                $("#estDevelopment").val(data[0].custom_fields[12].value);
                $("#estMaintenance").val(data[0].custom_fields[13].value);
                $("#estHost").val(data[0].custom_fields[14].value);
                $("#estother").val(data[0].custom_fields[15].value);
                $("#estrisk").val(data[0].custom_fields[16].value);

                refreshRate();
            }
        },
        error: function(data) {
            errordisplay(data);
            $('.divOverlay').hide();
        }
    }).done(function() {

        $(".divOverlay").hide();

        $('#Estimate_dataTable').show();
    });
}

function estimateAdd() {
    if ($('#estProjectMgr').val() == "") {
        //ohSnap("Project manager is mandatory",'yellow');
        return false;
    }
    if ($('#estDevelopment').val() == "") {
        //ohSnap("Development is mandatory",'yellow');
        return false;
    }
    if ($('#estQA').val() == "") {
        //ohSnap("QA is mandatory",'yellow');
        return false;
    }

    if ($('#estSetup').val() == "") {
       // ohSnap("Setup is mandatory",'yellow');
        return false;
    }
    if ($('#esttotal').val() == "") {
        //ohSnap("Total is mandatory",'yellow');
        return false;
    }
    var estid = $("#estId").val();
	var p_id=$('#estprojectId').val();
    var strEstimate = '{"subject":"' + $("#estprojectName").val() + '","project_id":' + estimate_project_id + ',"priority_id":' + priority_id_estimate + ',"tracker_id":' + tracker_id_estimate + ',' + (estid == 0 ? '' : ('"id":' + estid + ',')) +
        '"custom_fields":[{ id: ' + rate_hrs + ', name: "Rate Dkk Hrs", value: ' + ($("#estRate").val() == "" ? 0 : $("#estRate").val()) + ' },' +
        '{ id: ' + rate_cost + ', name: "Rate Dkk Cost", value: ' + ($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val()) + ' },' +
        '{ id:' + project_mgr + ', name: "Project Manager", value: ' + ($("#estProjectMgr").val() == "" ? 0 : $("#estProjectMgr").val()) + ' },' +
        '{ id: ' + UX + ', name: "UX (Wireframes)", value: ' + ($("#estUX").val() == "" ? 0 : $("#estUX").val()) + ' },' +
        '{ id: ' + dev_3D + ', name: "3D Development", value: ' + ($("#estDev3").val() == "" ? 0 : $("#estDev3").val()) + ' },' +
        '{ id: ' + QA_id + ', name: "QA", value: ' + ($("#estQA").val() == "" ? 0 : $("#estQA").val()) + ' },' +
        '{ id: ' + setup_id + ', name: "Setup and Deployment", value: ' + ($("#estSetup").val() == "" ? 0 : $("#estSetup").val()) + ' },' +
        '{ id:' + out_of_pocket_id + ', name: "Out of Pocket", value: ' + ($("#estoutofpocket").val() == "" ? 0 : $("#estoutofpocket").val()) + ' },' +
        '{ id: ' + total_id + ', name: "Total ", value: ' + ($("#esttotal").val() == "" ? 0 : $("#esttotal").val()) + ' },' +
        '{ id: ' + Account_mgt_id + ', name: "Account Management", value: ' + ($("#estAccount").val() == "" ? 0 : $("#estAccount").val()) + ' },' +
        '{ id: ' + producer_estimate_id + ', name: "Producer", value: ' + ($("#estProducer").val() == "" ? 0 : $("#estProducer").val()) + ' },' +
        '{ id:' + design_assistance_id + ', name: "Design Assistance", value: "' + ($("#estDesign").val() == "" ? 0 : $("#estDesign").val()) + '" },' +
        '{ id: ' + dev_id + ', name: "Development", value: ' + ($("#estDevelopment").val() == "" ? 0 : $("#estDevelopment").val()) + ' },' +
        '{ id:' + maintenance_id + ', name: "Maintenance", value: ' + ($("#estMaintenance").val() == "" ? 0 : $("#estMaintenance").val()) + ' },' +
        '{ id: ' + host_cost_id + ', name: "Amazon Hosting Cost", value: ' + ($("#estHost").val() == "" ? 0 : $("#estHost").val()) + ' },' +
        '{ id:' + other_id + ', name: "Other", value: ' + ($("#estother").val() == "" ? 0 : $("#estother").val()) + ' },' +
        '{ id:' + risk_id + ', name: "Risk(+/- %)", value: ' + ($("#estrisk").val() == "" ? 0 : $("#estrisk").val()) + ' },' +
        '{ id: ' + project_pid + ', name: "p_id" , value: ' + (p_id == "" ? 0 : p_id) + ' }]}';


    if (estid == 0) {
        var options = {};
        options.url = apiURL + "/estimate";
        options.type = "POST";
        options.data = strEstimate;
        options.dataType = "json";
        options.headers = tokenaccess();
        options.contentType = "application/json";
        options.success = function() {
            $('#estimateform').modal("hide");
            updateCampaignOnEstimate();
            estimatecomment($("#estcomment").val(),p_id);
        };
        options.error = function(data) {
            errordisplay(data);
        };
        $.ajax(options);

    } else {

        var options = {};

        options.url = apiURL + "/estimate";
        options.type = "PUT";
        options.data = strEstimate;
        options.dataType = "json";
        options.headers = tokenaccess();
        options.contentType = "application/json";
        options.success = function() {
            $('#estimateform').modal("hide");
            updateCampaignOnEstimate();
            estimatecomment($("#estcomment").val(),p_id);
        };
        options.error = function(data) {
            errordisplay(data);
        };
        $.ajax(options);

    }
}
$(document).on('click', '.cmt', function() {
    var eid = $("#estId").val();
    postcomment('#text-comment', eid,true);

});
function estimatecomment(escmt,p_id) {
console.log(p_id);
    var esid;
    $.ajax({
        url: apiURL + "/estimate?pid_Id=" + project_pid + "&pid_Value=" + p_id, //" + p_id,
        type: "GET",
        dataType: 'json',
        headers: tokenaccess(),
        success: function(data) {
            if (data.length != 0) {
                esid = data[0].id;
                console.log(esid);
                if ((escmt != '' | escmt != undefined | escmt.length > 0) && (esid != 0)) {
                    postcomment(escmt, esid,false);
                }
            }
        },
        error: function(data) {
            errordisplay(data);

        }
    });

}
function getComment() {
    var id = $("#estId").val();
    var comment = '';
    if (id != 0) {
        $.ajax({
            url: apiURL + '/notes?issueId=' + id,
            type: "GET",
            headers: tokenaccess(),
            success: function(data) {

                comment = '';
                for (var i = 0; i < data.length; i++) {

                    if (data[i].notes.length > 0) {
                        comment = comment + '<div class="col-md-12" style="box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.62);word-wrap:break-word; margin-bottom: 15px; padding-bottom: 15px;">' +
                            '<span class="col-md-9" style="font-weight:bold;color:#337ab7">' + data[i].user_name + '</span>' +
                            '<span class="col-md-3">' + data[i].created_on + '</span>' +
                            '<div class="col-md-12" style=" box-shadow: 0 0 1px 0 rgba(0,0,0,.62);background-color:#f2f2f2;"> ' + data[i].notes + ' </div></div><br>';
                    }
                }

                $("#est-comment").html(comment);
            },
            error: function(data) {
                errordisplay(data);
            }
        }).done(function(data) {
            $("#est-comment").html();
        });
    } else {
        comment = '';
        $("#est-comment").html(comment);
        ohSnap("add estimate first",'yellow');

        $('.divOverlay').hide();
    }

}
function postcomment(cmt, id,getCmntFlag) {
    $('.divOverlay').show();
    if (id != 0) {
        var options = {};
        options.url = apiURL + '/notes?issueId=' + id + '&message=' + cmt; // "http://localhost:56403/api/project";//" "http://10.97.85.76:8090/api/project"; // 
        options.type = "POST";
        options.headers = tokenaccess();
        options.contentType = "application/json";
        options.success = function(data) {
            $('.divOverlay').hide();
            ohSnap(data,'green');
			if(getCmntFlag){
				getComment();
			}
            $('.cmntBox').val('');
        };
        options.error = function(data) {
            errordisplay(data);
            $('.divOverlay').hide();
        };
        $.ajax(options);
    } else {
        ohSnap("Add estimate first",'yellow');
        $('.divOverlay').hide();
    }
}

function updateCampaignOnEstimate() {
    var optionsCmpUpdate = {};
    var stringCmp = '{"name":"' + $("#estprojectName").val() + '","response_put":' + response_put + ',"parent_id":' + parent_id +
        ',"id":' + $('#estprojectId').val() + ',"custom_fields":[{ id: ' + estimates_id + ', name: "Estimates Shared with Client", value: ' + $("#esttotal").val() + ' }]}';

    optionsCmpUpdate.url = apiURL + "/project";
    optionsCmpUpdate.type = "POST";
    optionsCmpUpdate.data = stringCmp;
    optionsCmpUpdate.dataType = "json";
    optionsCmpUpdate.headers = tokenaccess();
    optionsCmpUpdate.contentType = "application/json";
    optionsCmpUpdate.success = function() {
        $('#estimateform').modal("hide");
        ohSnap("Estimate updated successfully",'green');
        getDataNewEstimate();
   
    };
    optionsCmpUpdate.error = function(data) {
        errordisplay(data);
    };
    $.ajax(optionsCmpUpdate);
}

/* Approve functionality*/

function approveEstimate(id,name) {
console.log(id);
	$('#commentpopup').modal({
			backdrop: 'static',
			keyboard: false
		});
	$(".cmtHeading").html("<span style='color:#4CAE4C;font-weight:bold;font-size:16pt;'>" + name + "</span> : Approve");
	$(document).on('click','#approvePopUpCampaignSubmitButton',function(){
console.log(id);	
			estimatecomment($("#approve-comment").val(),id);
			updateCampaignOnApprove(name,id);
			
	});
}
function updateCampaignOnApprove(projectName, projectId) {
console.log(projectId);
    var optionsCmpUpdate = {};
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    var d = yyyy + '-' + (mm < 10 ? ('0' + mm) : mm) + '-' + (dd < 10 ? ('0' + dd) : dd);
    var stringCmp = '{"name":"' + projectName + '","response_put":' + response_put + ',"parent_id":' + parent_id +
        ',"id":' + projectId + ',"custom_fields":[{ "id": ' + development_startdate_id + ', "name": "Development Start Date", "value": "' + d + '" }]}';
    optionsCmpUpdate.url = apiURL + "/project";
    optionsCmpUpdate.type = "POST";
    optionsCmpUpdate.data = stringCmp;
    optionsCmpUpdate.dataType = "json";
    optionsCmpUpdate.headers = tokenaccess();
    optionsCmpUpdate.contentType = "application/json";
    optionsCmpUpdate.success = function() {
        $('#estimateform').modal("hide");
        ohSnap("Estimates Approved Successfully. Campaign moved to development tab.",'green');
        getDataNewEstimate();
    };
    optionsCmpUpdate.error = function(data) {
        errordisplay(data);
    };
    $.ajax(optionsCmpUpdate);
}
/* Approve functionality ends*/

$(function() {
    function tabletoJson(table) {
        var data = [];
        var headers = {};
        for (var i = 0; i < table.rows[0].cells.length; i++) {
            headers[i] = table.rows[0].cells[i].innerHTML.replace(/ /gi, '');
        }
        data.push(headers);
        for (var i = 1; i < table.rows.length; i++) {
            var tableRow = table.rows[i];
            var rowData = {};
            for (var j = 0; j < tableRow.cells.length; j++) {
                rowData[headers[j]] = tableRow.cells[j].innerHTML;
            }
            data.push(rowData);
        }
        return data;
    };

    function callme() {
        var rawtable = "<table id='pdfTable'>" +
            "<tr><th></th><th>Hrs</th><th>DKK(cost)</th></tr>" +
            "<tr><td>Rate</td><td>1</td><td>" + (($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val())) + "</td></tr>" +
            "<tr><td>Project Manager</td><td>" + ($("#estProjectMgr").val() == "" ? 0 : $("#estProjectMgr").val()) + "</td><td>" + (($("#estProjectMgr").val() == "" ? 0 : $("#estProjectMgr").val()) * ($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val())) + "</td></tr>" +
            "<tr><td>UX (Wireframes)</td><td>" + ($("#estUX").val() == "" ? 0 : $("#estUX").val()) + "</td><td>" + (($("#estUX").val() == "" ? 0 : $("#estUX").val()) * ($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val())) + "</td></tr>" +
            "<tr><td>3D Development</td><td>" + ($("#estDev3").val() == "" ? 0 : $("#estDev3").val()) + "</td><td>" + (($("#estDev3").val() == "" ? 0 : $("#estDev3").val()) * ($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val())) + "</td></tr>" +
            "<tr><td>QA</td><td>" + ($("#estQA").val() == "" ? 0 : $("#estQA").val()) + "</td><td>" + (($("#estQA").val() == "" ? 0 : $("#estQA").val()) * ($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val())) + "</td></tr>" +
            "<tr><td>Setup and Deployment</td><td>" + ($("#estSetup").val() == "" ? 0 : $("#estSetup").val()) + "</td><td>" + (($("#estSetup").val() == "" ? 0 : $("#estSetup").val()) * ($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val())) + "</td></tr>" +
            "<tr><td>Account Management</td><td>" + ($("#estAccount").val() == "" ? 0 : $("#estAccount").val()) + "</td><td>" + (($("#estAccount").val() == "" ? 0 : $("#estAccount").val()) * ($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val())) + "</td></tr>" +
            "<tr><td>Producer</td><td>" + ($("#estProducer").val() == "" ? 0 : $("#estProducer").val()) + "</td><td>" + (($("#estProducer").val() == "" ? 0 : $("#estProducer").val()) * ($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val())) + "</td></tr>" +
            "<tr><td>Design Assistance</td><td>" + ($("#estDesign").val() == "" ? 0 : $("#estDesign").val()) + "</td><td>" + (($("#estDesign").val() == "" ? 0 : $("#estDesign").val()) * ($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val())) + "</td></tr>" +
            "<tr><td>Development</td><td>" + ($("#estDevelopment").val() == "" ? 0 : $("#estDevelopment").val()) + "</td><td>" + (($("#estDevelopment").val() == "" ? 0 : $("#estDevelopment").val()) * ($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val())) + "</td></tr>" +
            "<tr><td>Maintenance</td><td>" + ($("#estMaintenance").val() == "" ? 0 : $("#estMaintenance").val()) + "</td><td>" + (($("#estMaintenance").val() == "" ? 0 : $("#estMaintenance").val()) * ($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val())) + "</td></tr>" +
            "<tr><td>Amazon Hosting Cost</td><td>" + ($("#estHost").val() == "" ? 0 : $("#estHost").val()) + "</td><td>" + (($("#estHost").val() == "" ? 0 : $("#estHost").val()) * ($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val())) + "</td></tr>" +
            "<tr><td>Other</td><td>" + ($("#estother").val() == "" ? 0 : $("#estother").val()) + "</td><td>" + (($("#estother").val() == "" ? 0 : $("#estother").val()) * ($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val())) + "</td></tr>" +
            "<tr><td>Total</td><td>" + ($("#esttotal").val() == "" ? 0 : $("#esttotal").val()) + "</td><td>" + (($("#esttotal").val() == "" ? 0 : $("#esttotal").val()) * ($("#estRateCost").val() == "" ? 0 : $("#estRateCost").val())) + "</td></tr>" +
            "<tr><td>Risk(+/- %)</td><td>" + ($("#estrisk").val() == "" ? 0 : $("#estrisk").val()) + " %</td><td>DKK " + ($("#estriskCost").val() == "" ? 0 : $("#estriskCost").val()) + "</td></tr>" +
            "</table>";

        $("#test").html(rawtable);
        var table = tabletoJson($('#pdfTable').get(0));

        var doc = new jsPDF('p', 'pt', 'letter', true);
        doc.cellInitialize();
        $.each(table, function(i, row) {
            $.each(row, function(j, cell) {
                doc.cell(1, 10, 190, 20, cell, i);
            });
        });
        doc.save($("#estprojectName").val());
    };
    $('.gettabledata').click(function(event) {
        event.preventDefault();
        callme();
    });
});