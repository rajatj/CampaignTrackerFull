

$(function(){
	$('#gotoforgot').click(function(){
		$('#signin').slideUp(function(){$('#forgotpass').slideDown();});
	});
	$('#backtologin').click(function(){
		$('#forgotpass').slideUp(function(){$('#signin').slideDown();});
	});
	/* Slide Toogle */
	$(".navigation >ul li > div.header").click(function(){
		var arrow = $(this).find("span.arrow");
		if(arrow.hasClass("up")){
			arrow.removeClass("up");
			arrow.addClass("down");
		}else if(arrow.hasClass("down")){
			arrow.removeClass("down");
			arrow.addClass("up");
		}
		$(this).parent().find("ul.menu").slideToggle();
	});
	$('#hidenav').click(function(){
		$('.navigation').fadeOut();
		setTimeout(function(){
			$('.maincontainer').animate({ width:'100%'},800);
		},700)
	});
	$('#openNav').click(function(){
		$('.navigation').show();
			$('.maincontainer').css('width','83%');
	});

	
	function resetcampaignform(formId)
{

$("#"+formId).find("input[type='text'] , input[type='email'], input.form-control, textarea").each(function(){
$(this).val('').css("border-color","");

});
$("#"+formId).find("select").each(function(){
$(this).css("border-color","");
$(this).val('');
});
 $("#due_date").datepicker("option", "minDate", 0);
 $("#due_date").datepicker("option", "maxDate", null);
 $("#launch_date").datepicker("option", "maxDate", null);
 $("#exit_date").datepicker("option", "minDate", 1);
 
}
	
	$('#request-campaign').click(function(){
		$('#newrequest').modal();
		
		resetcampaignform('newrequest');
	});
	$('#request-newamazon').click(function(){
		$('#newamazon').modal();
	})
	$('#request-newform').click(function(){
		$('#newform').modal();
	});
})