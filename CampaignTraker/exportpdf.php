<?php
//============================================================+
// File name   : example_061.php
// Begin       : 2010-05-24
// Last Update : 2014-01-25
//
// Description : Example 061 for TCPDF class
//               XHTML + CSS
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: XHTML + CSS
 * @author Nicola Asuni
 * @since 2010-05-25
 */

// Include the main TCPDF library (search for installation path).
require_once('downloadpdf/tcpdf_import.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 061');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('L', 'A4');
// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$json_string = 'http://10.97.85.82:8090/api/jsondata';

$jsondata = file_get_contents($json_string);
$obj = json_decode($jsondata, true);
$arrlength=count($obj);
$table = '<table class="tablepdf">'.
			'<tr class="head">'.
			
			'<th >Campaign Name</th>'.
			'<th>Budget</th>'.
			'<th>Start Date</th>'.
			'<th>Launch Date</th>'.
			'<th>Exit Date</th>'.
			'</tr>';
for($i=0; $i<$arrlength; $i++){

		$table .='<tr>'.
			
			'<td>'.$obj[$i]['name'].'</td>'.
			'<td>'.$obj[$i]['project_cost'].'</td>'.
			'<td>'.$obj[$i]['development_start_date'].'</td>'.
			'<td>'.$obj[$i]['launch_date'].'</td>'.
			'<td>'.$obj[$i]['exit_date'].'</td>'.
			'</tr>';
}

$table .='</table>';
$html = <<<EOF
<!-- EXAMPLE OF CSS STYLE -->
<style>
table{
	border-collapse: collapse;
	font-family: "Arial";
	font-size: 12px;
	color: #444;
}
table tr.head th{color: #000; height:20px;}
table tr.head th{background: #bbb; color: #000; font-weight: bold;}
table td,table th{padding: 8px 10px; border: #ddd 1px solid;}
table tr:nth-child(even){background: #f8f8f8;}
    h1 {
        color: #069;
        font-size: 22px;
        text-align:center
    }

</style>

<h1 class="title">Campaign Records</h1>
$table
EOF;


// Print text using writeHTMLCell()
$pdf->writeHTML($html, true, false, true, false, '');

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('Campaign_List.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
