angular.module('myApp', ['ui.bootstrap', 'ngSanitize', 'angularUtils.directives.dirPagination', 'dateFilter']).controller('detailPageCtrl',
    function($scope, $location, $http, $window, $filter, $modal, $rootScope) {

        $scope.currentPage = 1;
        $scope.pageSize = 10;
        var tokenKey = "accessToken";
        $scope.token = $window.localStorage.getItem(tokenKey);
        $rootScope.headers = {};
        $rootScope.UserName = $window.localStorage.getItem("CTuser");
        $(".userName").html("<span >Welcome " + $rootScope.UserName + "</span><span class='caret'></span> ");

        if ($scope.token) {
            $rootScope.headers.Authorization = $scope.token;
        } else {
            $window.location.href = '../index.html';
            return false;
        }
        $rootScope.apiUrl = apiURL + '/';
        $rootScope.projectId = $location.search().id;
        $rootScope.projectTab = $location.search().tab;

        $scope.issueUrl = $rootScope.apiUrl + 'IssueDetailPerProject/?project_id=' + $scope.projectId + '&tracker_id=2&offset=0&limit=1000';
        $rootScope.serviceUrl = $rootScope.apiUrl + 'IssueDetailPerProject/?project_id=65&tracker_id=14&offset=0&limit=1000';
        $rootScope.logoutUrl = $rootScope.apiUrl + 'Accounts/logout';

        $rootScope.ajaxcalls = function() {
            var reqs = {
                method: 'GET',
                url: $scope.issueUrl,

                headers: $rootScope.headers,
                contentType: 'application/json'

            }
            $http(reqs).success(

                function(response2) {

                    $scope.featuredata = response2;
                    $scope.filterData = response2;

                });

            var reqs2 = {
                method: 'GET',
                url: $rootScope.serviceUrl,

                headers: $rootScope.headers,
                contentType: 'application/json'

            }
            $http(reqs2).success(

                function(response3) {

                    $rootScope.serviced = $scope.servicedata = response3;


                });

        }


        $rootScope.ajaxcalls();
        $rootScope.refreshdetail = function() {
            $rootScope.ajaxcalls();
        }


        $rootScope.logout = function() {
            var reqlogout = {
                method: 'POST',
                url: $scope.logoutUrl,
                headers: $rootScope.headers,
                contentType: 'application/json'
            }
            $http(reqlogout).success(

                function(response) {

                    $window.localStorage.clear();
                    $window.location = '../index.html';

                });
        }


        $scope.formPopup = function(url) {


            var modalInstance = $modal.open({
                animation: $scope.animationEnabled,
                templateUrl: url,
                backdrop: 'static',
                keyboard: false,
                controller: 'ModalInstanceCtrl',
                resolve: {
                    data: function() {
                        return $scope.data;
                    }
                }
            });
        };

        $scope.tabShowerChk = 'about';

        $scope.tabShowerChk2 = 'overview';



        $scope.imageSrc = './images/toy.jpg';
        $scope.jsondataUrl = $rootScope.apiUrl + 'jsondata/' + $rootScope.projectTab;
        var reqs3 = {
                method: 'GET',
                url: $scope.jsondataUrl,


                headers: $rootScope.headers,
                contentType: 'application/json'

            }
            //console.log($scope.jsondataUrl);
        $http(reqs3).success(
            function(response) {
                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];



                $scope.data = $scope.getProject(response, $rootScope.projectId);
                //$scope.data.img_url ='./'+$scope.data.img_url;
                try {
                    var launchDateparse = /(\d+)-(\d+)-(\d+)/.exec($scope.data.launch_date);

                    //console.log(launchDateparse);			
                    $scope.CurrentProjectLaunchDate = $scope.data.launch_date;
                    $scope.CurrentLaunchDate = new Date(+launchDateparse[3], months.indexOf(launchDateparse[2]), +launchDateparse[1]);
                    console.log($scope.CurrentLaunchDate);
                } catch (err) {}
                try {
                    var exitDateparse = /(\d+)-(\d+)-(\d+)/.exec($scope.data.exit_date);
                    $scope.CurrentProjectExitDate = $scope.data.exit_date;
                    $scope.CurrentExitDate = new Date(+exitDateparse[3], months.indexOf(exitDateparse[2]), +exitDateparse[1]);

                    //	alert($scope.CurrentProjectExitDate);
                } catch (err) {
                    //alert($scope.CurrentProjectExitDate);
                }

                $scope.visibleAnalyticTab = ($scope.CurrentLaunchDate < new Date()) && ($scope.CurrentLaunchDate != undefined);
            });
        $scope.getProject = function(allProjects, projectId) {

            for (var i = 0; i < allProjects.length; i++) {
                if (allProjects[i].id == projectId) {
                    return allProjects[i];
                }
            }
        }
        $scope.tabShower = function(tab) {

            $scope.tabShowerChk = tab;
            if ($scope.tabShowerChk == "analytics") {

                $scope.tabShower2('overview');

            }

        }




        $scope.tabShower2 = function(tab) {

            $scope.tabShowerChk2 = tab;


            if ($scope.tabShowerChk2 == 'overview') {
                var barChartData = {
                    labels: ["Feature", "Bug", "Feedback", "Test Case", "Test Scenarios"],
                    datasets: [{
                        label: "Close",
                        fillColor: "transparent",
                        strokeColor: "rgba(237, 89, 78,0.8)",
                        highlightFill: "rgba(237, 89, 78,0.75)",
                        highlightStroke: "rgba(237, 89, 78,1)",
                        data: [100, 200, 100, 500, 300, 800]
                    }, {
                        label: "Open",
                        fillColor: "transparent",
                        strokeColor: "rgba(78, 226, 236,0.8)",
                        highlightFill: "rgba(78, 226, 236,0.75)",
                        highlightStroke: "rgba(78, 226, 236,1)",
                        data: [300, 300, 300, 100, 200, 700]
                    }]

                }

                var barChartData1 = {
                    labels: ["", "", "", "", "", ""],
                    datasets: [{
                        label: "Close",
                        fillColor: "transparent",
                        strokeColor: "rgba(237, 89, 78,0.8)",
                        highlightFill: "rgba(237, 89, 78,0.75)",
                        highlightStroke: "rgba(237, 89, 78,1)",
                        data: [100, 200, 100, 500, 300, 800]
                    }, {
                        label: "Open",
                        fillColor: "transparent",
                        strokeColor: "rgba(78, 226, 236,0.8)",
                        highlightFill: "rgba(78, 226, 236,0.75)",
                        highlightStroke: "rgba(78, 226, 236,1)",
                        data: [300, 300, 300, 100, 200, 700]
                    }]

                }

                var barChartData2 = {
                    labels: ["", "", "", "", "", ""],
                    datasets: [{
                        label: "Close",
                        fillColor: "transparent",
                        strokeColor: "rgba(237, 89, 78,0.8)",
                        highlightFill: "rgba(237, 89, 78,0.75)",
                        highlightStroke: "rgba(237, 89, 78,1)",
                        data: [100, 200, 100, 500, 300, 800]
                    }, {
                        label: "Open",
                        fillColor: "transparent",
                        strokeColor: "rgba(78, 226, 236,0.8)",
                        highlightFill: "rgba(78, 226, 236,0.75)",
                        highlightStroke: "rgba(78, 226, 236,1)",
                        data: [300, 300, 300, 100, 200, 700]
                    }]

                }

                var barChartData3 = {
                    labels: ["", "", "", "", "", ""],
                    datasets: [{
                        label: "Close",
                        fillColor: "transparent",
                        strokeColor: "rgba(237, 89, 78,0.8)",
                        highlightFill: "rgba(237, 89, 78,0.75)",
                        highlightStroke: "rgba(237, 89, 78,1)",
                        data: [100, 200, 100, 500, 300, 800]
                    }, {
                        label: "Open",
                        fillColor: "transparent",
                        strokeColor: "rgba(78, 226, 236,0.8)",
                        highlightFill: "rgba(78, 226, 236,0.75)",
                        highlightStroke: "rgba(78, 226, 236,1)",
                        data: [300, 300, 300, 100, 200, 700]
                    }]

                }

                var barChartData4 = {
                    labels: ["", "", "", "", "", ""],
                    datasets: [{
                        label: "Close",
                        fillColor: "transparent",
                        strokeColor: "rgba(237, 89, 78,0.8)",
                        highlightFill: "rgba(237, 89, 78,0.75)",
                        highlightStroke: "rgba(237, 89, 78,1)",
                        data: [100, 200, 100, 500, 300, 800]
                    }, {
                        label: "Open",
                        fillColor: "transparent",
                        strokeColor: "rgba(78, 226, 236,0.8)",
                        highlightFill: "rgba(78, 226, 236,0.75)",
                        highlightStroke: "rgba(78, 226, 236,1)",
                        data: [300, 300, 300, 100, 200, 700]
                    }]

                }
                var barChartData5 = {
                    labels: ["", "", "", "", "", ""],
                    datasets: [{
                        label: "Close",
                        fillColor: "transparent",
                        strokeColor: "rgba(237, 89, 78,0.8)",
                        highlightFill: "rgba(237, 89, 78,0.75)",
                        highlightStroke: "rgba(237, 89, 78,1)",
                        data: [100, 200, 100, 500, 300, 800]
                    }, {
                        label: "Open",
                        fillColor: "transparent",
                        strokeColor: "rgba(78, 226, 236,0.8)",
                        highlightFill: "rgba(78, 226, 236,0.75)",
                        highlightStroke: "rgba(78, 226, 236,1)",
                        data: [300, 300, 300, 100, 200, 700]
                    }]

                }
                var barChartData6 = {
                    labels: ["", "", "", "", "", ""],
                    datasets: [{
                        label: "Close",
                        fillColor: "transparent",
                        strokeColor: "rgba(237, 89, 78,0.8)",
                        highlightFill: "rgba(237, 89, 78,0.75)",
                        highlightStroke: "rgba(237, 89, 78,1)",
                        data: [100, 200, 100, 500, 300, 800]
                    }, {
                        label: "Open",
                        fillColor: "transparent",
                        strokeColor: "rgba(78, 226, 236,0.8)",
                        highlightFill: "rgba(78, 226, 236,0.75)",
                        highlightStroke: "rgba(78, 226, 236,1)",
                        data: [300, 300, 300, 100, 200, 700]
                    }]

                }


                setTimeout(function() {
                    var ctx = document.getElementById("line").getContext("2d");
                    window.myBar = new Chart(ctx).Line(barChartData, {
                        responsive: true
                    });
                    var ctx = document.getElementById("line1").getContext("2d");
                    window.myBar = new Chart(ctx).Line(barChartData1, {
                        responsive: true
                    });
                    var ctx = document.getElementById("line2").getContext("2d");
                    window.myBar = new Chart(ctx).Line(barChartData2, {
                        responsive: true
                    });
                    var ctx = document.getElementById("line3").getContext("2d");
                    window.myBar = new Chart(ctx).Line(barChartData3, {
                        responsive: true
                    });


                    var ctx = document.getElementById("line4").getContext("2d");
                    window.myBar = new Chart(ctx).Line(barChartData4, {
                        responsive: true
                    });


                    var ctx = document.getElementById("line5").getContext("2d");
                    window.myBar = new Chart(ctx).Line(barChartData5, {
                        responsive: true
                    });


                    var ctx = document.getElementById("line6").getContext("2d");
                    window.myBar = new Chart(ctx).Line(barChartData6, {
                        responsive: true
                    });

                }, 500);




            }


            if ($scope.tabShowerChk2 == 'browser') {
                var barChartData = {
                    labels: ["Feature", "Bug", "Feedback", "Test Case", "Test Scenarios"],
                    datasets: [{
                        label: "Close",
                        fillColor: "rgba(237, 89, 78,0.5)",
                        strokeColor: "rgba(237, 89, 78,0.8)",
                        highlightFill: "rgba(237, 89, 78,0.75)",
                        highlightStroke: "rgba(237, 89, 78,1)",
                        data: [300, 200, 100]
                    }, {
                        label: "Open",
                        fillColor: "rgba(78, 226, 236,0.5)",
                        strokeColor: "rgba(78, 226, 236,0.8)",
                        highlightFill: "rgba(78, 226, 236,0.75)",
                        highlightStroke: "rgba(78, 226, 236,1)",
                        data: [300, 300, 300]
                    }]

                }

                setTimeout(function() {
                    var ctx = document.getElementById("canvas").getContext("2d");
                    window.myBar = new Chart(ctx).Pie(barChartData, {
                        responsive: true
                    });
                }, 500);

            }




        }



        $scope.animationsEnabled = true;



        $scope.issuedetail = function(data) {
            $scope.popup(data, 'issue-detail-pop-up.html');
        }

        $scope.popup = function(data, templ) { //console.log(data);
            $scope.jsonObj = data;
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: templ,
                backdrop: 'static',
                keyboard: false,
                controller: 'DetailModalInstanceCtrl',
                windowClass: 'center-modal',
                resolve: {
                    data: function() {
                        return data;
                    }
                }

            });
        };



        $scope.filterdata = function(fl) {
            $scope.featuredata = {};
            if (fl == "Feature") {
                $scope.featuredata = $filter('filter')($scope.filterData, {
                    tracker: {
                        name: "Feature"
                    }
                });
                //console.log("feature");
            } else if (fl == "Feedback") {
                $scope.featuredata = $filter('filter')($scope.filterData, {
                    tracker: {
                        name: "Feedback"
                    }
                });

            } else {
                $scope.featuredata = $scope.filterData;

            }


        }

        $scope.animationsEnabled = true;



        $scope.openPopup = function() {
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'add-new-campaign.html',
                backdrop: 'static',
                keyboard: false,
                controller: 'RequestModalInstanceCtrl'
            });
        };
        $scope.detailSection = function(data) {


            $scope.jsonObj = data;
            //$rootScope.displayOverlay=true;
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'add-feature-popup.html',
                backdrop: 'static',
                keyboard: false,
                controller: 'FeatureModalInstanceCtrl',

                resolve: {
                    data: function() {
                        return data;
                    }
                }


            });
        };


    });


angular.module('myApp').controller('DetailModalInstanceCtrl', function($scope, $modalInstance, data, $modal, $rootScope) {

    $scope.data = data;


    $scope.ok = function() {
        $modalInstance.close();
    };

    $scope.cancel = function() {
        $modalInstance.dismiss();
    };




    $scope.animationEnabled = true;




});



angular.module('myApp').controller('OtherController', function($scope) {
    $scope.pageChangeHandler = function(num) {
        // console.log('1 going to page ' + num);
    };
});




angular.module('myApp').controller('RequestModalInstanceCtrl', function($scope, $modalInstance, $modal, $http, $filter, $timeout, $rootScope) {
    //$scope.value1="";
    $scope.obj = {};


    $scope.genCampaign = function() {
        $modalInstance.dismiss();
        $scope.obj.name = $scope.camp_name;
        $scope.obj.response_put = false;
        var i = Math.floor((Math.random() * 100000) + 1);
        var identifier = $scope.camp_name.replace(/ /g, "") + '-' + i;
        $scope.obj.identifier = identifier.toLowerCase();
        i++;
        $scope.obj.description = $scope.proj_description;
        //$scope.obj.name = $scope.value1;//done
        $scope.obj.Prod_Date = $scope.Prod_Date;
        $scope.FProd_Date = $filter('date')($scope.Prod_Date, "yyyy-MM-dd"); //Working
        $scope.obj.Exit_Date = $filter('date')($scope.Exit_Date, "yyyy-MM-dd");
        var objCustom = {};
        objCustom.id = 2;
        objCustom.name = "Launch Date";
        objCustom.value = $scope.FProd_Date; //done


        var customlist = [{
                "id": 2,
                "name": "Launch Date",
                "value": $scope.FProd_Date
            },

            {
                "id": 3,
                "name": "Estimates Shared with Client",
                "value": $scope.New_Estimate
            },

            {
                "id": 23,
                "name": "Market",
                "value": $scope.marketList
            },

            {
                "id": 17,
                "name": "Project Technology",
                "value": $scope.New_Platform
            },

            {
                "id": 28,
                "name": "Producer",
                "value": $scope.New_Producer
            },

            {
                "id": 31,
                "name": "Live URL",
                "value": $scope.New_url
            },

            {
                "id": 16,
                "name": "Project Type",
                "value": "Campaign Factory"
            },

            {
                "id": 45,
                "name": "Acronis Link",
                "value": "Campaign Factory"
            }
        ];

        $scope.obj.custom_fields = customlist;
        $scope.obj.parent_id = 23;
        $scope.strFinal = JSON.stringify($scope.obj);

        $scope.addUrl = $rootScope.apiUrl + 'project';
        //console.log($scope.strFinal);
        var req = {
            method: 'POST',
            url: $scope.addUrl,
            data: $scope.strFinal,
            dataType: 'json',
            contentType: 'application/json'

        }

        $http(req).success(function() {
            //$modalInstance.dismiss();
            $timeout(function() {
                alert("New Campaign Created Successfully");
            }, 500);


        }).error(function() {
            //$modalInstance.dismiss();
            $timeout(function() {
                alert("sorry, please try again later");
            }, 500);
        });
    };


    $scope.ok = function() {
        $modalInstance.close();
    };

    $scope.cancel = function() {
        $modalInstance.dismiss();
    };

    $scope.Onfocusout = function() {
        if ($scope.value1 == "") {
            alert('This Field Cant Blank');
            //$('#fldCampaignName'+id).focus();
        }
    };

});




angular.module('myApp').controller('ModalInstanceCtrl', function($scope, $modalInstance, data, $modal, $filter, $http, $timeout, $rootScope) {
    $scope.DeployCampaignUrl = $rootScope.apiUrl + 'copymaster';
    $rootScope.items = data;

    $scope.deployRequest = function() {
        $modalInstance.dismiss();

        /* var objCustom = {};
          objCustom.id = 39;
          objCustom.name = "Environment";
          objCustom.value=$scope.Environment; */

        /* var objCust = {};
		  objCust.id = 49;
		  objCust.name = "Deployment Slot (IST)";
		  objCust.value=$scope.DeploymentSlot;
   */
        var customlist = [{
                "id": 39,
                "name": "Environment",
                "value": $scope.Environment
            },

            {
                "id": 49,
                "name": "Deployment Slot (IST)",
                "value": $scope.DeploymentSlot
            }
        ];
        var pro_parent = {};
        pro_parent.id = 23;
        pro_parent.name = "Campaign Factory";
        $scope.deployobj = {};
        $scope.deployobj.subject = $rootScope.items.name; //done
        $scope.deployobj.project_id = 65; //fixed for deployment 
        $scope.deployobj.tracker_id = 14; //fixed
        $scope.deployobj.status_id = 1;
        $scope.deployobj.category_id = 1;
        $scope.deployobj.fixed_version_id = 1;
        $scope.deployobj.assigned_to_id = 1;
        $scope.deployobj.parent_issue_id = 1;
        $scope.deployobj.is_private = false;
        $scope.deployobj.estimated_hours = 123;
        $scope.deployobj.due_date = "2015-12-12"; //greater than current  date 
        $scope.deployobj.description = "test description"; //done
        $scope.deployobj.priority_id = 2; //done
        $scope.deployobj.custom_fields = customlist;

        $scope.strDeployFinal = JSON.stringify($scope.deployobj);
        //console.log($scope.strDeployFinal);

        var req = {
            method: 'POST',
            url: $scope.DeployCampaignUrl,
            data: $scope.strDeployFinal,
            dataType: 'json',
            header: $rootScope.headers,
            contentType: 'application/json'

        }

        $http(req).success(function() {

            $timeout(function() {
                alert("Data Added Successfully");
            }, 500);


        }).error(function() {
            $modalInstance.dismiss();
            $timeout(function() {
                alert("Sorry, Please Try Again Later");
            }, 500);
        });

        $scope.ok = function() {
            $modalInstance.close();
        };

        $scope.cancel = function() {
            $modalInstance.dismiss();
        };


    };
    /* deoployRequest Function Ends */




    $scope.cancelModal = function(size) {
        $modalInstance.dismiss();
        //$rootScope.displayOverlay=true;
        /* var modalInstance = $modal.open(
				{
				animation: true,
				templateUrl: 'DetailModalContent',
				controller: 'DetailModalInstanceCtrl',
				size:size,
				//windowClass: 'center-modal',
						resolve: 
							{
									items: function ()
									{
										return data;
									}					
							}	
			
				});	 */

    }



});




angular.module('myApp').controller('FeatureModalInstanceCtrl', function($scope, $modalInstance, $modal, $http, $filter, $location, $timeout, $rootScope) {
    //$scope.value1="";

    $scope.statusnew = 'New';

    $scope.add = function() {
        $modalInstance.dismiss();

        $scope.obj = {};


        $scope.obj.subject = $scope.subject; //done

        $scope.obj.project_id = parseInt($rootScope.projectId);
        $scope.obj.tracker_id = 2;

        if ($scope.statusnew == 'New') {
            $scope.obj.status_id = 1;
        } else {

            alert('status is not new');
            //return ;
        }
        $scope.obj.priority_id = 2;

        $scope.obj.description = $scope.description; //done

        $rootScope.displayOverlay = true;
        $scope.strFinal = JSON.stringify($scope.obj);
        //console.log($scope.strFinal);
        var req4 = {
            method: 'POST',
            url: $rootScope.apiUrl + 'copymaster',
            data: $scope.strFinal,
            dataType: 'json',
            headers: $rootScope.headers,
            contentType: 'application/json'

        }

        $http(req4).success(function() {
            $rootScope.displayOverlay = false;
            //$modalInstance.dismiss();
            $timeout(function() {
                alert("New Feature Added Successfully");
                $rootScope.refreshdetail();
            }, 50);


        }).error(function() {
            $rootScope.displayOverlay = false;
            //$modalInstance.dismiss();
            $timeout(function() {
                alert("sorry, please try again later");
            }, 50);
        });



    };


    $scope.ok = function() {
        $modalInstance.close();
    };

    $scope.cancel = function() {
        $modalInstance.dismiss();
    };

    $scope.Onfocusout = function() {
        if ($scope.subject == "") {
            alert('This Field Cant Blank');

        }
    };

});


angular.module('dateFilter', []).filter('dateformat', function() {
    return function(input) {
        if (input != undefined && input.length > 0) {
            var dateparse = /(\d\d\d\d-\d+-\d+).*/.exec(input);
            if (dateparse.length > 0) {
                return dateparse[1];
            }
        }
        return "";
    };
});