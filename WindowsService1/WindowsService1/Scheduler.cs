﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Configuration;

namespace WinServiceCT
{
    public partial class Scheduler : ServiceBase
    {
        private Timer timerJsonSqlSync = null;
        private Timer timerPingService = null;
        private Timer timerUserSync = null;
        private Timer timerDailyNotification = null;
        private Timer timerMailQueueService = null;

        int timeJsonSql = 0;
        int timePing = 0;
        int timeUserSync = 0;
        int timeDailyNotification = 0;
        int timeMailQueue = 0;

        public Scheduler()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                timerJsonSqlSync = new Timer();
                timerPingService = new Timer();
                timerUserSync = new Timer();
                timerDailyNotification = new Timer();
                timerMailQueueService = new Timer();

                int.TryParse(ConfigurationManager.AppSettings["SyncUsersTime"], out timeUserSync);
                timerUserSync.Elapsed += new ElapsedEventHandler(timerUserSync_Elapsed);
                timerUserSync.Enabled = true;

                int.TryParse(ConfigurationManager.AppSettings["ApiTime"], out timeJsonSql);
                timerJsonSqlSync.Elapsed += new ElapsedEventHandler(timerJsonSqlSync_Elapsed);
                timerJsonSqlSync.Enabled = true;

                int.TryParse(ConfigurationManager.AppSettings["PingTime"], out timePing);
                timerPingService.Elapsed += new ElapsedEventHandler(timerPingService_Elapsed);
                timerPingService.Enabled = true;

                int.TryParse(ConfigurationManager.AppSettings["DailyNotificationTime"], out timeDailyNotification);
                timerDailyNotification.Elapsed += new ElapsedEventHandler(timerDailyNotification_Elapsed);
                timerDailyNotification.Enabled = true;

                int.TryParse(ConfigurationManager.AppSettings["MailQueueScheduleTime"], out timeMailQueue);
                timerMailQueueService.Elapsed += new ElapsedEventHandler(timerMailQueueService_Elapsed);
                timerMailQueueService.Enabled = true;

                Library.WriteErrorLog("WinServiceCT OnStart called");
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        void timerMailQueueService_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                int.TryParse(ConfigurationManager.AppSettings["MailQueueScheduleTime"], out timeMailQueue);
                timerMailQueueService.Interval = timeMailQueue * 1000;
                string MailQueueCheck = ConfigurationManager.AppSettings["MailQueueServiceEnabled"];
                //Library.WriteErrorLog("Sync Daily Notifications Logging Started ");
                if (MailQueueCheck.ToUpper() == "true".ToUpper() || MailQueueCheck.ToUpper() == "t".ToUpper())
                {
                    // call api and save the data from Json to SQL on web api
                    string con = ConfigurationManager.AppSettings["apiUrl"] + "notifications/ExecuteMailQueue";
                    var request = (HttpWebRequest)WebRequest.Create(con);
                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    string s = responseString.ToString();
                }
                //Library.WriteDataLog(s);
                //Library.WriteErrorLog("Sync Daily Notifications Logged Successfully ");
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog("timerMailQueueService_Elapsed: " + ex.ToString());


            }
        }

        void timerDailyNotification_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                int.TryParse(ConfigurationManager.AppSettings["DailyNotificationTime"], out timeDailyNotification);
                timerDailyNotification.Interval = timeDailyNotification * 1000;
                string MailQueueCheck = ConfigurationManager.AppSettings["DailyMailNotificationServiceEnabled"];
                if (MailQueueCheck.ToUpper() == "true".ToUpper() || MailQueueCheck.ToUpper() == "t".ToUpper())
                {
                    //Library.WriteErrorLog("Sync Daily Notifications Logging Started ");

                    // call api and save the data from Json to SQL on web api
                    string con = ConfigurationManager.AppSettings["apiUrl"] + "notifications/dailynotifications";
                    var request = (HttpWebRequest)WebRequest.Create(con);
                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    string s = responseString.ToString();
                }
                //Library.WriteDataLog(s);
                //Library.WriteErrorLog("Sync Daily Notifications Logged Successfully ");
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog("timerDailyNotification_Elapsed: " + ex.ToString());


            }
        }

        void timerUserSync_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                int.TryParse(ConfigurationManager.AppSettings["SyncUsersTime"], out timeUserSync);
                timerUserSync.Interval = timeUserSync * 1000;

                //Library.WriteErrorLog("Sync Users Logging Started ");

                // call api and save the data from Json to SQL on web api
                string con = ConfigurationManager.AppSettings["apiUrl"] + "JsonToSqlSync/1";
                var request = (HttpWebRequest)WebRequest.Create(con);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                string s = responseString.ToString();

                //Library.WriteDataLog(s);
                //Library.WriteErrorLog("Sync Users Logged Successfully ");
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog("timerUserSync_Elapsed: " + ex.ToString());
                //Library.WriteDataLog(ex);

            }
        }

        void timerJsonSqlSync_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                int.TryParse(ConfigurationManager.AppSettings["ApiTime"], out timeJsonSql);
                timerJsonSqlSync.Interval = timeJsonSql * 1000;

                //Library.WriteErrorLog("Sync Data Logging Started ");

                // call api and save the data from Json to SQL on web api
                string con = ConfigurationManager.AppSettings["apiUrl"] + "JsonToSqlSync";
                var request = (HttpWebRequest)WebRequest.Create(con);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                string s = responseString.ToString();

                //Library.WriteDataLog(s);
                //Library.WriteErrorLog("Sync Data Logged Successfully ");
            }
            catch (WebException we)
            {
                Library.WriteDataLog("timerJsonSqlSync_Elapsed WebException: " + we.Message);
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog("timerJsonSqlSync_Elapsed: " + ex.ToString());
                //Library.WriteDataLog(ex);

            }
        }

        void timerPingService_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                int.TryParse(ConfigurationManager.AppSettings["PingTime"], out timePing);
                timerPingService.Interval = timePing * 1000;
                string pingCheck = ConfigurationManager.AppSettings["startPingCheck"];
                string s = string.Empty;



                //Call Ping Status Check Controller 
                if (pingCheck.ToUpper() == "true".ToUpper() || pingCheck.ToUpper() == "t".ToUpper())
                {
                    //Library.WriteErrorLog("Ping Data Logging Started " + timePing.ToString());
                    string conStatus = ConfigurationManager.AppSettings["apiUrl"] + "sitestatus";
                    HttpWebRequest requestStatus = (HttpWebRequest)WebRequest.Create(conStatus);
                    requestStatus.Timeout = 600 * 1000;
                    HttpWebResponse responseStatus = (HttpWebResponse)requestStatus.GetResponse();

                    var responseString = new StreamReader(responseStatus.GetResponseStream()).ReadToEnd();
                    s = responseString.ToString();
                    //Library.WriteDataLog(s);
                    //Library.WriteErrorLog("Ping Data Logged Successfully ");
                }



            }
            catch (Exception ex)
            {
                Library.WriteErrorLog("timerPingService_Elapsed: " + ex.ToString());
                //Library.WriteDataLog(ex);

            }
        }


        protected override void OnStop()
        {
            try
            {

                timerJsonSqlSync.Stop();
                timerJsonSqlSync.Dispose();

                timerPingService.Stop();
                timerPingService.Dispose();

                timerUserSync.Stop();
                timerUserSync.Dispose();
                Library.WriteErrorLog("WinServiceCT OnStop called");
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }
    }
}
