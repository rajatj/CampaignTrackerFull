﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
namespace WinServiceCT
{
    class Library
    {
        //static string jsonPath = ConfigurationSettings.AppSettings["jsonFilePath"];
        //static string dataFolderName = ConfigurationSettings.AppSettings["dataFolderName"];
        //static string jsonFileName = ConfigurationSettings.AppSettings["jsonFileName"];

        public static void WriteErrorLog(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile"+DateTime.Now.ToString("yyyy-MM-dd")+".txt", true);
                sw.WriteLine("Exception: " + ex.ToString() + ":" + DateTime.Now.ToString());
                sw.Flush();
                sw.Close();

            }
            catch
            {
            }

        }

        /// <summary>
        /// Log for writting log file after success data read/write operations
        /// </summary>
        /// <param name="Message">Message to be passed</param>
        public static void WriteErrorLog(string Message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt", true);
                sw.WriteLine(Message + " : " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss"));
                sw.Flush();
                sw.Close();

            }
            catch
            {
            }

        }
        public static void WriteDataLog(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\DataExceptionFile.txt", true);
                //string jsonFilePath = System.Configuration.ConfigurationSettings.AppSettings["jsonFilePath"];
                //sw = new StreamWriter(jsonFilePath, true);
                sw.WriteLine("WriteException: " + ex.Message + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss"));
                sw.Flush();
                sw.Close();

            }
            catch
            {
            }

        }
        public static void WriteDataLog(string Message)
        {

            StreamWriter sw = null;

            try
            {

                //string jsonFilePath = jsonPath;

                //WriteToLogFile("Json Path : " + jsonFilePath);
                //using (FileStream fs = new FileStream(jsonFilePath, FileMode.Create, FileAccess.Write))
                //{
                //    using (StreamWriter swr = new StreamWriter(fs))
                //    {
                //        swr.WriteLine(Message);
                //        swr.Flush();
                //        swr.Close();
                //    }
                //    fs.Close();
                //}
                using (FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "\\DataFile.json", FileMode.Create, FileAccess.Write))
                {
                    using (StreamWriter swr = new StreamWriter(fs))
                    {
                        swr.WriteLine(Message);
                        swr.Flush();
                        swr.Close();
                    }
                    fs.Close();
                }

            }
            catch (Exception ex)
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ":" + ex.Message + "\t -WriteDataLogException");
                sw.Flush();
                sw.Close();
            }

        }

        public static void WriteToLogFile(string Message)
        {
            StreamWriter sw = null;
            sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
            sw.WriteLine(DateTime.Now.ToString() + ":" + Message );
            sw.Flush();
            sw.Close();
        }
    }
}
