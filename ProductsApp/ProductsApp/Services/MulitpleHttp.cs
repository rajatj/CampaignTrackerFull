﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace ProductsApp.Services
{
    class MulitpleHttp
    {
        public static Task<string> GetAsync(string url)
        {
            var tcs = new TaskCompletionSource<string>();
            try
            {

                var request = (HttpWebRequest)WebRequest.Create(url);
                string proxyAddress = ConfigurationManager.AppSettings["proxyAddress"];
                string username = ConfigurationManager.AppSettings["username"];
                string password = ConfigurationManager.AppSettings["password"];
                int timeOut = 0;
                int.TryParse(ConfigurationManager.AppSettings["proxyTimeOut"], out timeOut);
                if ((!string.IsNullOrWhiteSpace(proxyAddress)) && (!string.IsNullOrWhiteSpace(username)) && (!string.IsNullOrWhiteSpace(password)))
                {
                    WebProxy proxy = new WebProxy();
                    proxy.Address = new Uri(proxyAddress);
                    proxy.Credentials = new NetworkCredential(username, password);
                    request.Proxy = proxy;
                }
                if (timeOut > 0)
                    request.Timeout = timeOut;
                else
                    request.Timeout = 10000;

                try
                {
                    request.BeginGetResponse(iar =>
                    {
                        HttpWebResponse response = null;
                        try
                        {
                            response = (HttpWebResponse)request.EndGetResponse(iar);
                            tcs.SetResult((Convert.ToInt32( response.StatusCode).ToString()+"," + request.RequestUri.ToString()));
                        }
                        catch (Exception exc)
                        {
                            tcs.SetException(exc);
                        }
                        finally
                        {
                            if (response != null) response.Close();
                        }
                    }, null);
                }
                catch (Exception exc)
                {
                    tcs.SetException(exc);
                }
            }
            catch (Exception e)
            {
                tcs.SetException(e);
            }
            return tcs.Task;
        }
    }

}
