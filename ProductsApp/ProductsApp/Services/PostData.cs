﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;

namespace ProductsApp.Services
{
    public class PostData
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public static string PostMethod(Issue data, string priorityId, string trackerId)
        {
            try
            {
                String finalString = "";

                XmlDocument xmlDoc = new XmlDocument();
                XmlNode rootNode = xmlDoc.CreateElement("issue");
                xmlDoc.AppendChild(rootNode);

                XmlNode idNode = xmlDoc.CreateElement("project_id");
                idNode.InnerText = data.project_id.ToString();//pass project id 
                rootNode.AppendChild(idNode);

                XmlNode nameNode = xmlDoc.CreateElement("subject");
                nameNode.InnerText = data.subject;
                rootNode.AppendChild(nameNode);

                XmlNode identifierNode = xmlDoc.CreateElement("priority_id");
                identifierNode.InnerText = priorityId;
                rootNode.AppendChild(identifierNode);

                XmlNode statusNode = xmlDoc.CreateElement("status_id");
                statusNode.InnerText = data.status_id.ToString();
                rootNode.AppendChild(statusNode);

                XmlNode trackerNode = xmlDoc.CreateElement("tracker_id");
                trackerNode.InnerText = trackerId;
                rootNode.AppendChild(trackerNode);

                string XmlizedString = "";
                using (StringWriter sw = new StringWriter())
                {
                    using (XmlTextWriter tx = new XmlTextWriter(sw))
                    {
                        xmlDoc.WriteTo(tx);
                        XmlizedString = sw.ToString();
                    }
                }
                finalString = XmlizedString.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
                string postirl = System.Configuration.ConfigurationManager.AppSettings["hostUrl"] + "issues.xml?key=" + System.Configuration.ConfigurationManager.AppSettings["apiKey"];
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(postirl);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(finalString);
                request.ContentType = "application/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();
                Uri res = (Uri)response.ResponseUri;
                string resLocation = response.Headers["Location"].ToString();
                string issueIdCreated = resLocation.Split(new string[] { "issues/" }, StringSplitOptions.None)[1];
                return issueIdCreated;
            }
            catch (Exception ex)
            {
                Logger.Error("PostData.PostMethod" + ex.ToString());
                return string.Empty;
            }

        }

        public static bool AddWatchers(string issueId, string[] userIDs)
        {
            try
            {

                foreach (string user in userIDs)
                {
                    // add watcher

                    String finalString = "";

                    XmlDocument xmlDoc = new XmlDocument();
                    XmlNode rootNode = xmlDoc.CreateElement("user_id");
                    rootNode.InnerText = user;
                    xmlDoc.AppendChild(rootNode);

                    string XmlizedString = "";
                    using (StringWriter sw = new StringWriter())
                    {
                        using (XmlTextWriter tx = new XmlTextWriter(sw))
                        {
                            xmlDoc.WriteTo(tx);
                            XmlizedString = sw.ToString();
                        }
                    }

                    finalString = XmlizedString.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
                    string postirl = System.Configuration.ConfigurationManager.AppSettings["hostUrl"] + "issues/" + issueId + "/watchers.xml?key=" + System.Configuration.ConfigurationManager.AppSettings["apiKey"];
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(postirl);
                    byte[] bytes;
                    bytes = System.Text.Encoding.ASCII.GetBytes(finalString);
                    request.ContentType = "application/xml; encoding='utf-8'";
                    request.ContentLength = bytes.Length;
                    request.Method = "POST";
                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                    HttpWebResponse response;
                    response = (HttpWebResponse)request.GetResponse();

                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("PostData.AddWatchers" + ex.ToString());
                return false;
            }

        }

        public static bool UpdateIssue(string issueId, string[] data, string[] element)
        {
            try
            {
                // add notes

                String finalString = "";

                XmlDocument xmlDoc = new XmlDocument();
                XmlNode rootNode = xmlDoc.CreateElement("issue");
                xmlDoc.AppendChild(rootNode);

                for (int i = 0; i < data.Length; i++)
                {
                    XmlNode statusNode = xmlDoc.CreateElement(element[i]);
                    statusNode.InnerText = data[i];
                    rootNode.AppendChild(statusNode);

                }
                string XmlizedString = "";
                using (StringWriter sw = new StringWriter())
                {
                    using (XmlTextWriter tx = new XmlTextWriter(sw))
                    {
                        xmlDoc.WriteTo(tx);
                        XmlizedString = sw.ToString();
                    }
                }

                finalString = XmlizedString.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
                string postirl = System.Configuration.ConfigurationManager.AppSettings["hostUrl"] + "issues/" + issueId + ".xml?key=" + System.Configuration.ConfigurationManager.AppSettings["apiKey"];
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(postirl);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(finalString);
                request.ContentType = "application/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "PUT";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("PostData.UpdateIssue" + ex.ToString());
                return false;
            }

        }

        public static bool UpdateIssue(string issueId, string[] data, string[] element ,string apiKey)
        {
            try
            {
                // add notes

                String finalString = "";

                XmlDocument xmlDoc = new XmlDocument();
                XmlNode rootNode = xmlDoc.CreateElement("issue");
                xmlDoc.AppendChild(rootNode);

                for (int i = 0; i < data.Length; i++)
                {
                    XmlNode statusNode = xmlDoc.CreateElement(element[i]);
                    statusNode.InnerText = data[i];
                    rootNode.AppendChild(statusNode);

                }
                string XmlizedString = "";
                using (StringWriter sw = new StringWriter())
                {
                    using (XmlTextWriter tx = new XmlTextWriter(sw))
                    {
                        xmlDoc.WriteTo(tx);
                        XmlizedString = sw.ToString();
                    }
                }

                finalString = XmlizedString.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
                string postirl = System.Configuration.ConfigurationManager.AppSettings["hostUrl"] + "issues/" + issueId + ".xml?key=" + apiKey;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(postirl);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(finalString);
                request.ContentType = "application/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "PUT";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("PostData.UpdateIssue" + ex.ToString());
                return false;
            }

        }

        public static bool UpdatePassword( string Password , string redmineUserId)
        {

            try
            {
                String finalString = "";

                XmlDocument xmlDoc = new XmlDocument();
                XmlNode rootNode = xmlDoc.CreateElement("user");
                xmlDoc.AppendChild(rootNode);

                XmlNode idNode = xmlDoc.CreateElement("password");
                idNode.InnerText = Password;//pass project id 
                rootNode.AppendChild(idNode);


                string XmlizedString = "";
                using (StringWriter sw = new StringWriter())
                {
                    using (XmlTextWriter tx = new XmlTextWriter(sw))
                    {
                        xmlDoc.WriteTo(tx);
                        XmlizedString = sw.ToString();
                    }
                }
                finalString = XmlizedString.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
                string postirl = System.Configuration.ConfigurationManager.AppSettings["hostUrl"] + "users/"+redmineUserId+".xml?key=" + System.Configuration.ConfigurationManager.AppSettings["apiKey"];
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(postirl);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(finalString);
                request.ContentType = "application/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "PUT";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }
                else
                {
                    Logger.Debug("PostData.UpdatePassword Put failed and Xml post string= " + postirl);
                    return false;
                    
                }
            }
            catch (Exception ex)
            {
                Logger.Error("PostData.UpdatePassword" + ex.ToString());
                return false;
            }


        }

    }
}