﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
namespace ProductsApp.Services
{
    public class Total_TimeService
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public double GetTimePerProject(int pid)
        {
            try
            {
                double time_spent = 0.0;
                string url = ConfigurationManager.AppSettings["hostUrl"] + "/time_entries.json?key=" + ConfigurationManager.AppSettings["apiKey"] + "&project_id=" + pid;

                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();

                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);

                    string str = reader.ReadToEnd();
                    RootObject time =
                                    (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);
                    List<TimeEntry> time_entry = time.time_entries;


                    for (int i = 0; i < time_entry.Count; i++)
                    {

                        time_spent += time_entry[i].hours;
                    }

                }

                return time_spent;
            }

            catch(Exception ex)
            {
                Logger.Error("Total_TimeService.GetTimePerProject" + ex.ToString());
                return 0.0;
            }
        }


    }
}