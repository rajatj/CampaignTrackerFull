﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace ProductsApp.Services
{
    public class TokenAuthorization
    {
        static DatabaseConnection dc = new DatabaseConnection();

        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// <para>Check for Token in Database and Return </para>
        /// <para>empty string id token doesn't exists in DB or token is not present</para>
        /// <para>Returns Exception on exception</para>
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        public static string CheckToken(HttpRequestMessage Request)
        {
            try
            {
                var headers = Request.Headers;

                if (headers.Contains("Authorization"))
                {
                    string token = headers.GetValues("Authorization").First();

                    string sqlCheckTokenUser = "Select [RedmineUserId] from [Users] where [Token]=@Token";
                    SqlParameter[] sqlParam = new SqlParameter[1];
                    sqlParam[0] = new SqlParameter("@Token", System.Data.SqlDbType.VarChar);
                    sqlParam[0].Value = Encryption.Decrypt(token);
                    string redmineUserId = dc.GetSingleCell(sqlCheckTokenUser, sqlParam);
                    if (!string.IsNullOrEmpty(redmineUserId))
                        return redmineUserId;
                    else
                        return string.Empty;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("TokenAuthorization.CheckToken Exeption"+ex.ToString());
                return "Exception";
            }
        }
    }
}