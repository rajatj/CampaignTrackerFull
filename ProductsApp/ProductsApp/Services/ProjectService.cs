﻿using ProductsApp.Models;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
namespace ProductsApp.Services
{
    public class ProjectService
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public List<ProjectVM> GetAllProjects()
        {
            try
            {
                int limit = 100;
                int offset = 0;
                int totalCount = 1;


                List<ProjectVM> projectVM = new List<ProjectVM>();
                List<Project> project_list = new List<Project>();

                while (totalCount > offset)
                {
                    string url = ConfigurationManager.AppSettings["hostUrl"] + "projects.json?limit=" + limit + "&offset=" + offset + "&key=" + ConfigurationManager.AppSettings["apiKey"];
                    WebRequest request = WebRequest.Create(url);
                    WebResponse response = request.GetResponse();

                    var time = new Total_TimeService();
                    var issue = new IssueService();
                    string[] result;

                    using (Stream responseStream = response.GetResponseStream())
                    {
                        #region // Get single project
                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                        string str = reader.ReadToEnd();

                        RootObject root = (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);
                        project_list = root.projects;

                        totalCount = root.total_count;
                        offset = root.offset;
                        limit = root.limit;

                        ProjectVM proVM;
                        for (int i = 0; i < project_list.Count; i++, offset++)
                        {

                            if (project_list[i].custom_fields[2].value == "Campaign Factory" && project_list[i].status.ToString() == "1")
                            {
                                proVM = new ProjectVM();

                                proVM.id = project_list[i].id;

                                proVM.parent = project_list[i].parent;

                                proVM.TotalTimeSpent = time.GetTimePerProject(proVM.id);

                                try
                                {
                                    proVM.EstimatedTime = Convert.ToDouble(project_list[i].custom_fields[1].value);

                                }
                                catch
                                {
                                    proVM.EstimatedTime = 0.0;
                                }
                                if (project_list[i].custom_fields[9].value == "")
                                {
                                    proVM.exit_date = "NA";
                                }
                                else
                                {
                                    DateTime d = Convert.ToDateTime(project_list[i].custom_fields[9].value);
                                    proVM.exit_date = d.ToString("dd-MMM-yyyy");
                                }

                                if (project_list[i].custom_fields[10].value == "")
                                {
                                    proVM.development_start_date = "NA";
                                }
                                else
                                {
                                    DateTime d1 = Convert.ToDateTime(project_list[i].custom_fields[10].value);
                                    proVM.development_start_date = d1.ToString("dd-MMM-yyyy");
                                }

                                proVM.project_cost = (proVM.EstimatedTime) * (Convert.ToDouble((ConfigurationManager.AppSettings["cost"])));

                                if (proVM.EstimatedTime < proVM.TotalTimeSpent)
                                    proVM.burndown = false;
                                else
                                    proVM.burndown = true;

                                try
                                {
                                    proVM.launch_date = Convert.ToDateTime(project_list[i].custom_fields[0].value).ToString("dd-MMM-yyyy");
                                }
                                catch
                                {
                                    proVM.launch_date = "";
                                }

                                proVM.producer_name = project_list[i].custom_fields[4].value;

                                proVM.market = project_list[i].custom_fields[5].value;

                                proVM.hosting_platform = project_list[i].custom_fields[3].value;
                                proVM.live_url = project_list[i].custom_fields[7].value;

                                result = issue.GetProgressPerProject(proVM.id);

                                proVM.status = result[0];
                                proVM.progress = Convert.ToDouble(result[1]);
                                proVM.name = project_list[i].name;
                                
                                //proVM.members = GetMembersList.GetAllMembersByProject(project_list[i].id);
                                //proVM.pending_with = project_list[i].custom_fields

                                foreach (CustomField cs in project_list[i].custom_fields)
                                {
                                    if (cs.name == "Pending With")
                                        proVM.pending_with = cs.value;
                                    if (cs.name == "Due Date")
                                    {
                                        DateTime dueDate;
                                        if (DateTime.TryParse(cs.value, out dueDate))
                                            proVM.due_date = cs.value;
                                        else
                                            proVM.due_date = string.Empty;
                                    }
                                    if (cs.name == "Image")
                                        proVM.image_url = cs.value;
                                    if (cs.name == "QA CName")
                                        proVM.qa_url = cs.value;
                                }

                                projectVM.Add(proVM);
                            }
                        }
                        #endregion
                    }

                }
                return projectVM;
            }
            catch (WebException we)
            {
                Logger.Error("ProjectService.GetAllProjects" + we.ToString());
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error("ProjectService.GetAllProjects" + ex.ToString());
                return null;
            }

        }


    }
}