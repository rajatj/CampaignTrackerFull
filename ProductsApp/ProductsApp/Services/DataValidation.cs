﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ProductsApp.Services
{
    public class DataValidation
    {
        private DateTime _exitDate;
        private double _project_cost;
        private string _producer_name;
        private string _market;
        private string _hosting_platform;
        private string _phase_status;
        private double _progress;
        private DateTime _development_start_date;
        private DateTime _launch_date;
        private double _TotalTimeSpent;
        private double _EstimatedTime;
        
        private string _pending_with;
        private DateTime _due_date;
        private string _image_url;
        private string inputDateFormat = "dd-MMM-yyyy";

        //validations for inserting data into project_main Table

        public int id { get; set; }
        public string name { get; set; }
        public string exit_date
        {
            get
            {
                if (_exitDate != DateTime.MinValue)
                    return _exitDate.ToString("yyyy-MM-dd");
                else
                    return null;
            }
            set
            {
                if (!DateTime.TryParseExact(value, inputDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out _exitDate))

                    _exitDate = DateTime.MinValue;

            }
        }
        public double project_cost
        {
            get { return _project_cost; }
            set
            {
                try
                {
                    _project_cost = Math.Round(value, 2);
                }
                catch
                {

                }
            }
        }
        public bool burndown { get; set; }
        public string producer_name
        {
            get
            {
                return _producer_name;
            }
            set
            {
                if (value.Length <= 200)
                    _producer_name = value;
                else
                    _producer_name = "Invalid Length";
            }
        }
        public string market
        {
            get
            {
                return _market;
            }
            set
            {
                if (value.Length <= 200)
                    _market = value;
                else
                    _market = "Invalid Length";
            }
        }
        public string hosting_platform
        {
            get
            {
                return _hosting_platform;
            }
            set
            {
                if (value.Length <= 100)
                    _hosting_platform = value;
                else
                    _hosting_platform = "Invalid Length";
            }
        }
        public string phase_status
        {
            get
            {
                return _phase_status;
            }
            set
            {
                if (value.Length <= 100)
                    _phase_status = value;
                else
                    _phase_status = "Invalid Length";
            }
        }
        public double progress
        {
            get { return _progress; }
            set
            {
                try
                {
                    _progress = Math.Round(value, 2);
                }
                catch
                {

                }
            }
        }
        public string live_url { get; set; }
        public string development_start_date
        {
            get
            {
                if (_development_start_date != DateTime.MinValue)
                    return _development_start_date.ToString("yyyy-MM-dd");
                else
                    return null;
            }
            set
            {
                if (!DateTime.TryParseExact(value, inputDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out _development_start_date))
                    _development_start_date = DateTime.MinValue;
            }
        }
        public string launch_date
        {
            get
            {
                if (_launch_date != DateTime.MinValue)
                    return _launch_date.ToString("yyyy-MM-dd");
                else
                    return null;
            }
            set
            {
                if (!DateTime.TryParseExact(value, inputDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out _launch_date))
                    _launch_date = DateTime.MinValue;

            }
        }
        public double TotalTimeSpent
        {
            get { return _TotalTimeSpent; }
            set
            {
                try
                {
                    _TotalTimeSpent = Math.Round(value, 2);
                }
                catch
                {

                }
            }
        }
        public double EstimatedTime
        {
            get { return _EstimatedTime; }
            set
            {
                try
                {
                    _EstimatedTime = Math.Round(value, 2);
                }
                catch
                {

                }
            }
        }
        public bool PingStatus { get; set; }

        public string due_date
        {
            get
            {
                if (_due_date != DateTime.MinValue)
                    return _due_date.ToString("yyyy-MM-dd");
                else
                    return null;
            }
            set
            {
                if (!DateTime.TryParseExact(value, inputDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out _due_date))
                    _due_date = DateTime.MinValue;

            }
        }
        public string pending_with { get; set; }
        public string image_url { get; set; }
        public string qa_url { get; set; }

    }
}