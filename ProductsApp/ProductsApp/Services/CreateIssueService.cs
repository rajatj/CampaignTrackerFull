﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;

namespace ProductsApp.Services
{
    public class CreateIssueService
    {
        DatabaseConnection dc = new DatabaseConnection();

        public string CreateIssue(Issue data, string redmineUserId)
        {
            String finalString = "";

            XmlDocument xmlDoc = new XmlDocument();
            XmlNode rootNode = xmlDoc.CreateElement("issue");
            xmlDoc.AppendChild(rootNode);

            XmlNode idNode = xmlDoc.CreateElement("project_id");
            idNode.InnerText = data.project_id.ToString();//pass project id 
            rootNode.AppendChild(idNode);

            XmlNode nameNode = xmlDoc.CreateElement("subject");
            nameNode.InnerText = data.subject;
            rootNode.AppendChild(nameNode);

            try
            {
                XmlNode identifierNode = xmlDoc.CreateElement("priority_id");
                identifierNode.InnerText = data.priority_id.ToString();
                rootNode.AppendChild(identifierNode);
            }
            catch { }
            XmlNode statusNode = xmlDoc.CreateElement("status_id");
            statusNode.InnerText = data.status_id.ToString();
            rootNode.AppendChild(statusNode);

            XmlNode trackerNode = xmlDoc.CreateElement("tracker_id");
            trackerNode.InnerText = data.tracker_id.ToString();
            rootNode.AppendChild(trackerNode);
            try
            {
                XmlNode deployNode = xmlDoc.CreateElement("due_date");
                deployNode.InnerText = data.due_date.ToString();
                rootNode.AppendChild(deployNode);
            }
            catch { }

            XmlNode custom_fieldsNode = xmlDoc.CreateElement("custom_fields");
            XmlAttribute attribute = xmlDoc.CreateAttribute("type");
            attribute.Value = "array";
            custom_fieldsNode.Attributes.Append(attribute);
            try
            {
                foreach (var v in data.custom_fields)
                {

                    XmlNode custom_fieldNode = xmlDoc.CreateElement("custom_field");

                    XmlAttribute attribute1 = xmlDoc.CreateAttribute("id");
                    attribute1.Value = v.id.ToString();
                    custom_fieldNode.Attributes.Append(attribute1);

                    XmlAttribute attribute2 = xmlDoc.CreateAttribute("name");
                    attribute2.Value = v.name;
                    custom_fieldNode.Attributes.Append(attribute2);

                    XmlNode valueNode = xmlDoc.CreateElement("value");
                    valueNode.InnerText = v.value;
                    custom_fieldNode.AppendChild(valueNode);

                    custom_fieldsNode.AppendChild(custom_fieldNode);

                }
                rootNode.AppendChild(custom_fieldsNode);
            }
            catch { }
            string XmlizedString = "";
            using (StringWriter sw = new StringWriter())
            {
                using (XmlTextWriter tx = new XmlTextWriter(sw))
                {
                    xmlDoc.WriteTo(tx);
                    XmlizedString = sw.ToString();
                }
            }

            finalString = XmlizedString.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

            string getUserApiKey = "Select [RedmineApiKey] from [Users] where [RedmineUserId] = '" + redmineUserId + "'";
            string UserApiKey = dc.GetSingleCell(getUserApiKey);



            string postirl = System.Configuration.ConfigurationManager.AppSettings["hostUrl"] + "issues.xml?key=" + UserApiKey;// System.Configuration.ConfigurationManager.AppSettings["apiKey"];
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(postirl);
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(finalString);
            request.ContentType = "application/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                return responseStr;
            }
            return null;
        }
    }
}