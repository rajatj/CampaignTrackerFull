﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ProductsApp.Services
{
    public class DatabaseConnection
    {
        static string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionStringSql"];



        public bool checkIfExists(string query)
        {
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand(query, con);
                    con.Open();

                    SqlDataReader sr = cmd.ExecuteReader();
                    if (sr.HasRows)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteErrorLog("DatabaseConnection.checkIfExists Exception: " + ex.ToString());
                return false;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
        }
        public string GetSingleCell(string query)
        {
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand(query, con);
                    con.Open();

                    object sr = cmd.ExecuteScalar();
                    if (sr != null)
                    {
                        if (!string.IsNullOrWhiteSpace(sr.ToString()))
                            return sr.ToString();
                        else
                            return string.Empty;
                    }
                    else
                        return string.Empty;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteErrorLog("DatabaseConnection.GetSingleCell Exception: " + ex.ToString());
                return string.Empty;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
        }
        public string GetSingleCell(string query , SqlParameter[] param)
        {
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddRange(param);
                    con.Open();

                    object sr = cmd.ExecuteScalar();
                    if (sr != null)
                    {
                        if (!string.IsNullOrWhiteSpace(sr.ToString()))
                            return sr.ToString();
                        else
                            return string.Empty;
                    }
                    else
                        return string.Empty;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteErrorLog("DatabaseConnection.GetSingleCell Exception: " + ex.ToString());
                return string.Empty;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
        }
        public string[] GetIssueIdAndEnabled(string query)
        {
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand(query, con);
                    con.Open();

                    SqlDataReader sr = cmd.ExecuteReader();
                    if (sr.HasRows)
                    {
                        string[] str = new string[2];
                        str[0] = sr[0].ToString();
                        str[1] = sr[1].ToString();
                        return str;
                    }
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteErrorLog("DatabaseConnection.GetIssueIdAndEnabled Exception: " + ex.ToString());
                return null;
            }
            finally
            {
                con.Close();
            }
        }

        public DataTable GetDataTable(string query)
        {
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    SqlDataAdapter sda = new SqlDataAdapter(query, con);
                    //con.Open();

                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    return dt;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteErrorLog("GetDataTable Exception: " + ex.ToString());
                return null;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
        }
        public DataTable GetDataTable(string query , SqlParameter[] param)
        {
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    SqlDataAdapter sda = new SqlDataAdapter(query, con);
                    //con.Open();
                    sda.SelectCommand.Parameters.AddRange(param);

                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    return dt;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteErrorLog("GetDataTable with sqlparam Exception: " + ex.ToString());
                return null;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
        }
        public bool InsertUpdateDelete(string query)
        {
            SqlConnection con = null;
            //SqlTransaction trans = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    con.Open();
                    using (SqlTransaction trans = con.BeginTransaction())
                    {
                        using (SqlCommand cmd = new SqlCommand(query, con))
                        {
                            try
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.Transaction = trans as SqlTransaction;
                                

                                int result = cmd.ExecuteNonQuery();
                                if (result >= 0)
                                {
                                    trans.Commit();
                                    return true;
                                }
                                else
                                    return false;
                            }
                            catch (Exception exe)
                            {
                                ErrorLogging.WriteErrorLog("DatabaseConnection.InsertUpdateDelete Inner" + exe.ToString());
                                trans.Rollback();
                                return false;

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteErrorLog("DatabaseConnection.InsertUpdateDelete" + ex.ToString());
                return false;

            }
            finally
            {
                if (con != null)
                    con.Close();
            }
        }

        public bool InsertUpdateDelete(string query, SqlParameter[] param)
        {
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    con.Open();
                    using (SqlTransaction trans = con.BeginTransaction())
                    {
                        using (SqlCommand cmd = new SqlCommand(query, con))
                        {
                            try
                            {
                                cmd.Transaction = trans as SqlTransaction;
                                foreach (SqlParameter p in param)
                                {
                                    if (p.Value == null)
                                        p.Value = DBNull.Value;
                                    if (p.Value.ToString() == "null")
                                        p.Value = DBNull.Value;
                                }
                                cmd.Parameters.AddRange(param);
                                cmd.CommandType = CommandType.Text;


                                

                                int result = cmd.ExecuteNonQuery();
                                if (result >= 0)
                                {
                                    trans.Commit();
                                    return true;
                                }
                                else
                                    return false;
                            }
                            catch (Exception exe)
                            {
                                ErrorLogging.WriteErrorLog("DatabaseConnection.InsertUpdateDelete inner" + exe.ToString());
                                trans.Rollback();
                                return false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteErrorLog("DatabaseConnection.InsertUpdateDelete" + ex.ToString());

                return false;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
        }

        public bool InsertUpdateDeleteDynamic(string query, SqlParameter[] param)
        {
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(connectionString))
                {
                    con.Open();
                    using (SqlTransaction trans = con.BeginTransaction())
                    {
                        using (SqlCommand cmd = new SqlCommand(query, con))
                        {
                            try
                            {
                                cmd.Transaction = trans as SqlTransaction;
                                cmd.Parameters.AddRange(param);
                                cmd.CommandType = CommandType.Text;

                                int result = cmd.ExecuteNonQuery();
                                if (result > 0)
                                {
                                    trans.Commit();
                                    return true;
                                }
                                else
                                    return false;
                            }
                            catch (Exception exe)
                            {
                                ErrorLogging.WriteErrorLog("DatabaseConnection.InsertUpdateDeleteDynamic inner" + exe.ToString());
                                trans.Rollback();
                                return false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteErrorLog("DatabaseConnection.InsertUpdateDeleteDynamic" + ex.ToString());

                return false;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
        }

        /// <summary>
        /// Insert MySqlParamCreate
        /// </summary>
        /// <param name="noOfParams">Number of params</param>
        /// <param name="paramName">use sql param name used in query string</param>
        /// <param name="dbtype">db type for params</param>
        /// <param name="value">value of params</param>
        public SqlParameter[] MySqlParamCreate(int noOfParams, string[] paramName, SqlDbType[] dbtype, string[] value)
        {
            SqlParameter[] mysqlParam = new SqlParameter[noOfParams];
            for (int i = 0; i < mysqlParam.Length; i++)
            {
                mysqlParam[i] = new SqlParameter(paramName[i], dbtype) { Value = value[i] };
            }
            return mysqlParam;

        }
    }
}