﻿using Newtonsoft.Json;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ProductsApp.Services
{
    public class JsonDeserializer
    {
        public static List<ProjectVM> JsonFileDeserializer()
        {
            try
            {

                string jsonFilePath = System.Configuration.ConfigurationManager.AppSettings["jsonDataFilePath"];

                StreamReader myFile = new StreamReader(jsonFilePath);
                string myString = myFile.ReadToEnd();

                myFile.Close();

                var items = JsonConvert.DeserializeObject<List<ProductsApp.Models.Json.RootObject>>(myString);
                ProjectVM proVM;
                List<ProjectVM> list = new List<ProjectVM>();
                foreach (ProductsApp.Models.Json.RootObject r in items)
                {
                    proVM = new ProjectVM();
                    proVM.id = r.id;
                    proVM.name = r.name;
                    proVM.exit_date = r.exit_date;
                    proVM.project_cost = r.project_cost;
                    proVM.burndown = r.burndown;
                    proVM.producer_name = r.producer_name;
                    proVM.hosting_platform = r.hosting_platform;
                    proVM.status = r.status;
                    proVM.progress = r.progress;
                    //ProductsApp.Models.Json.Parent p = new Models.Json.Parent();
                    //proVM.parent = new Models.Parent();
                    //proVM.parent.id = p.id;
                    //proVM.parent.name = p.name;
                    //proVM.parent

                    proVM.live_url = r.live_url;
                    proVM.development_start_date = r.development_start_date;
                    proVM.launch_date = r.launch_date;
                    proVM.TotalTimeSpent = r.TotalTimeSpent;
                    proVM.EstimatedTime = r.EstimatedTime;
                    proVM.market = r.market;

                    list.Add(proVM);
                }


                return list;

            }
            catch
            {


                return null;
            }
        }
    }
}