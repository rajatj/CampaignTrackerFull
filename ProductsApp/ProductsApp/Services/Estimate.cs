﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.Services
{
    public class Estimate
    {
        public static string CreateEstimate(Estimate data)
        {
            try
            {
                String finalString = "";

                XmlDocument xmlDoc = new XmlDocument();
                XmlNode rootNode = xmlDoc.CreateElement("issue");
                xmlDoc.AppendChild(rootNode);

                XmlNode idNode = xmlDoc.CreateElement("project_id");
                idNode.InnerText = data.project_id.ToString();//pass project id 
                rootNode.AppendChild(idNode);

                XmlNode nameNode = xmlDoc.CreateElement("subject");
                nameNode.InnerText = data.subject;
                rootNode.AppendChild(nameNode);

                XmlNode identifierNode = xmlDoc.CreateElement("priority_id");
                identifierNode.InnerText = priorityId;
                rootNode.AppendChild(identifierNode);

                XmlNode statusNode = xmlDoc.CreateElement("status_id");
                statusNode.InnerText = data.status_id.ToString();
                rootNode.AppendChild(statusNode);

                XmlNode trackerNode = xmlDoc.CreateElement("tracker_id");
                trackerNode.InnerText = trackerId;
                rootNode.AppendChild(trackerNode);

                string XmlizedString = "";
                using (StringWriter sw = new StringWriter())
                {
                    using (XmlTextWriter tx = new XmlTextWriter(sw))
                    {
                        xmlDoc.WriteTo(tx);
                        XmlizedString = sw.ToString();
                    }
                }
                finalString = XmlizedString.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
                string postirl = System.Configuration.ConfigurationManager.AppSettings["hostUrl"] + "issues.xml?key=" + System.Configuration.ConfigurationManager.AppSettings["apiKey"];
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(postirl);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(finalString);
                request.ContentType = "application/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();
                Uri res = (Uri)response.ResponseUri;
                string resLocation = response.Headers["Location"].ToString();
                string issueIdCreated = resLocation.Split(new string[] { "issues/" }, StringSplitOptions.None)[1];
                return issueIdCreated;
            }
            catch (Exception ex)
            {
                Logger.Error("PostData.PostMethod" + ex.ToString());
                return string.Empty;
            }

        }
    }
}