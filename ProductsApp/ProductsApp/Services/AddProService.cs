﻿using ProductsApp.Models;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Http;

namespace ProductsApp.Services
{
    public class AddProService
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        DatabaseConnection dc = new DatabaseConnection();
        public string postXMLData(string destinationUrl, string requestXml, ProjectVM provm)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
                request.ContentType = "application/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();

                int statusCode = Convert.ToInt32(response.StatusCode);

                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created)
                {
                    int id = 0;

                    string[] strArr = response.Headers["Location"].ToString().Split('/');
                    int.TryParse(strArr[strArr.Length - 1], out id);//getting id of created project
                    provm.id = id;
                    List<string> userRole = null;


                    SqlQueries.Project_Main_Tbl insertUpdateProjectMainTable = new SqlQueries.Project_Main_Tbl();
                    string DBResult = insertUpdateProjectMainTable.InsertOrUpdate(provm);
                    if (DBResult == "Success")
                    {
                        AddUserToProjectMembership(id.ToString(), GetUsersFromGroup(out userRole), userRole);
                        return "Success";
                    }
                    else
                    {
                        Logger.Debug("AddProService.postXMLData: Sql DB Insert Failed: " + DBResult);
                        return "Api Server failure";
                    }
                }
                else if (statusCode == 404)
                {
                    Logger.Debug("AddProService.postXMLData: 404 status code redmine: Redmine Unreachable/Not Responding");
                    return "Redmine Unreachable/Not Responding";
                }
                else if (statusCode == 422)
                {
                    Logger.Debug("AddProService.postXMLData: 422 status code redmine : Data Invalid");
                    return "Data Invalid";
                }
                else
                {
                    Logger.Debug("AddProService.postXMLData: Unknown Status: " + statusCode.ToString());
                    return "Failed";
                }

            }
            catch (WebException we)
            {
                Logger.Error("AddProService.postXMLData " + we.ToString());
                return "Redmine Server Not Responding";
            }
            catch (Exception ex)
            {
                Logger.Error("AddProService.postXMLData " + ex.ToString());
                return "Api Server failure";

            }

        }

        public string putXMLData(string destinationUrl, string requestXml, ProjectVM provm)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
                request.ContentType = "application/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "PUT";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();

                int statusCode = Convert.ToInt32(response.StatusCode);

                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created)
                {

                    SqlQueries.Project_Main_Tbl insertUpdateProjectMainTable = new SqlQueries.Project_Main_Tbl();
                    //string DBResult = insertUpdateProjectMainTable.InsertOrUpdate(provm);
                    string DBResult = insertUpdateProjectMainTable.UpdateCampaignDynamicParam(provm);
                    if (DBResult == "Success")
                    {
                        return "Success";
                    }
                    else
                    {
                        Logger.Debug("AddProService.putXMLData: Sql DB Insert Failed: " + DBResult);
                        return "Api Server failure";
                    }
                }
                else if (statusCode == 404)
                {
                    Logger.Debug("AddProService.putXMLData: 404 status code redmine: Redmine Unreachable/Not Responding");
                    return "Redmine Unreachable/Not Responding";
                }
                else if (statusCode == 422)
                {
                    Logger.Debug("AddProService.putXMLData: 422 status code redmine : Data Invalid");
                    return "Data Invalid";
                }
                else
                {
                    Logger.Debug("AddProService.putXMLData: Unknown Status: " + statusCode.ToString());
                    return "Failed";
                }

            }
            catch (WebException we)
            {
                Logger.Error("AddProService.putXMLData " + we.ToString());
                return "Redmine Server Not Responding";
            }
            catch (Exception ex)
            {
                Logger.Error("AddProService.putXMLData " + ex.ToString());
                return "Api Server failure";

            }


        }

        private List<string> GetUsersFromGroup(out List<string> roles)
        {
            roles = new List<string>();
            try
            {
                string[] groupIdList = ConfigurationManager.AppSettings["Groups"].ToString().Split(',');
                List<string> returnList = new List<string>();
                foreach (string groups in groupIdList)
                {
                    string groupId = groups.Trim();

                    string url = ConfigurationManager.AppSettings["hostUrl"] + "groups/" + groupId + ".json?include=users,memberships&key=" + ConfigurationManager.AppSettings["apiKey"];
                    Logger.Debug("AddProService.GetUsersFromGroup Url: " + url);
                    WebRequest request = WebRequest.Create(url);
                    WebResponse response = request.GetResponse();

                    using (Stream responseStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                        string str = reader.ReadToEnd();

                        RootGroup root = (RootGroup)Newtonsoft.Json.JsonConvert.DeserializeObject<RootGroup>(str);
                        Group group = root.group;
                        foreach (User u in group.users)
                        {
                            returnList.Add(u.id.ToString());
                            roles.Add(group.memberships[0].roles[0].id.ToString());
                        }
                        //for (int i = 0; i < group.users.Count(); i++)
                        //{
                        //    returnList[i] = group.users[i].id.ToString();
                        //    roles[i]= group.memberships[0].roles[0].name.ToString();
                        //}


                    }
                }
                return returnList;
            }
            catch (Exception ex)
            {
                Logger.Error("AddProService.GetUsersFromGroup Exception: " + ex.ToString());
                return null;
            }
        }

        private void AddUserToProjectMembership(string projectId, List<string> userIdList, List<string> roles)
        {
            try
            {

                if (roles == null)
                    return;
                for (int i = 0; i < userIdList.Count; i++)
                {
                    string roleId = roles[i];
                    string url = ConfigurationManager.AppSettings["hostUrl"] + "projects/" + projectId + "/memberships.json?key=" + ConfigurationManager.AppSettings["apiKey"];
                    Logger.Debug("AddProService.AddUserToProjectMembership Url: " + url);
                    string jsonStringPost = "{\"membership\":{\"user_id\":" + userIdList[i] + ",\"role_ids\":[ " + roleId + " ]}}";
                    Logger.Debug("AddProService.AddUserToProjectMembership json: " + jsonStringPost);

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse response = null;
                    int MembershipId = 0;
                    try
                    {
                        byte[] bytes;
                        bytes = System.Text.Encoding.ASCII.GetBytes(jsonStringPost);
                        request.ContentType = "application/json; encoding='utf-8'";
                        request.ContentLength = bytes.Length;
                        request.Method = "POST";
                        Stream requestStream = request.GetRequestStream();
                        requestStream.Write(bytes, 0, bytes.Length);
                        requestStream.Close();

                        response = (HttpWebResponse)request.GetResponse();

                        string[] strArr = response.Headers["Location"].ToString().Split('/');
                        int.TryParse(strArr[strArr.Length - 1], out MembershipId);
                        if (response.StatusCode == HttpStatusCode.Created)
                        {

                            string associateMembersToCampaignQuery = @"Insert into [Memberships] ([Id] , [RedmineUserId] , [Role] , [Project_Id]) VALUES (@Id , @RedmineUserId  , @Role , @Project_Id)";


                            SqlParameter[] param = new SqlParameter[4];
                            param[0] = new SqlParameter("@Id", SqlDbType.BigInt);
                            param[0].Value = MembershipId;
                            param[1] = new SqlParameter("@RedmineUserId", SqlDbType.BigInt);
                            param[1].Value = userIdList[i];
                            param[2] = new SqlParameter("@Role", SqlDbType.VarChar);
                            param[2].Value = roles[i];
                            param[3] = new SqlParameter("@Project_Id", SqlDbType.BigInt);
                            param[3].Value = projectId;

                            if (!dc.InsertUpdateDelete(associateMembersToCampaignQuery, param))
                                Logger.Debug("AddProService.AddUserToProjectMembership insertMembersToSqlDB Failed");

                        }
                    }
                    catch (WebException we)
                    {
                        string associateMembersToCampaignQuery = @"Insert into [Memberships] ([Id] , [RedmineUserId] , [Role] , [Project_Id]) VALUES (@Id , @RedmineUserId  , @Role , @Project_Id)";


                        SqlParameter[] param = new SqlParameter[4];
                        param[0] = new SqlParameter("@Id", SqlDbType.BigInt);
                        param[0].Value = MembershipId;
                        param[1] = new SqlParameter("@RedmineUserId", SqlDbType.BigInt);
                        param[1].Value = userIdList[i];
                        param[2] = new SqlParameter("@Role", SqlDbType.VarChar);
                        param[2].Value = roles[i];
                        param[3] = new SqlParameter("@Project_Id", SqlDbType.BigInt);
                        param[3].Value = projectId;

                        if (!dc.InsertUpdateDelete(associateMembersToCampaignQuery, param))
                            Logger.Debug("AddProService.AddUserToProjectMembership insertMembersToSqlDB Failed");

                    }
                }
            }

            catch (Exception ex)
            {
                Logger.Error("AddProService.AddUserToProjectMembership Exception: " + ex.ToString());
            }
        }

    }
}