﻿using NLog;
using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace ProductsApp.Services
{
    public class GetMembersList
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static List<Membership> GetAllMembersByProject(int ProjectId)
        {
            try
            {
                int limit = 100;
                int offset = 0;
                int totalCount = 1;
                List<Membership> memberships = new List<Membership>();
                while (totalCount > offset)
                {
                    string url = ConfigurationManager.AppSettings["hostUrl"] + "projects/" + ProjectId.ToString() + "/memberships.json?limit=" + limit + "&offset=" + offset + "&key=" + ConfigurationManager.AppSettings["apiKey"];
                    WebRequest request = WebRequest.Create(url);
                    WebResponse response = request.GetResponse();

                    using (Stream responseStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                        string str = reader.ReadToEnd();
                        RootMembers root = (RootMembers)Newtonsoft.Json.JsonConvert.DeserializeObject<RootMembers>(str);
                        var tempList = memberships.Concat(root.memberships);
                        memberships = tempList.ToList<Membership>();
                        totalCount = root.total_count;
                        //offset = root.offset;
                        limit = root.limit;
                        offset = memberships.Count;
                    }
                }
                return memberships;
            }
            catch (Exception ex)
            {
                Logger.Error("GetMembersList.GetAllMembersByProject" + ex.ToString());
                return null;
            }

        }
    }
}