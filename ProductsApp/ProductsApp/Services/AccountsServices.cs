﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace ProductsApp.Services
{
    public class AccountsServices
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        DatabaseConnection dc = new DatabaseConnection();

        public string Login(ProductsApp.Models.Accounts.Login param, out string Token)
        {
            try
            {
                Token = string.Empty;

                if (string.IsNullOrWhiteSpace(param.Username) || string.IsNullOrWhiteSpace(param.Password))
                {
                    return "Username or password can't be blank";
                }
                string getUsernameSqlQuery = string.Empty;
                string result = string.Empty;
                string username = param.Username.Trim();
                //string email = param.Email.Trim();
                string password = param.Password.Trim();


                #region//unauthorized access check
                string url = ConfigurationManager.AppSettings["hostUrl"] + "projects.json";
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    request.Credentials = new NetworkCredential(username, password);
                    HttpWebResponse response;
                    response = (HttpWebResponse)request.GetResponse();

                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        return "Un-Authorized Access!!Check Id or Password";
                    }

                }
                catch (WebException we)
                {
                    try
                    {
                        if (we.Status == WebExceptionStatus.ProtocolError)
                        {
                            var response = we.Response as HttpWebResponse;
                            if (response != null)
                            {
                                if (response.StatusCode == HttpStatusCode.Unauthorized)
                                {
                                    return "Un-Authorized Access!!Check Id or Password";
                                }
                            }
                            else
                            {
                                Logger.Debug("AccountsServices.Login " + we.ToString());
                                // no http status code available
                            }
                        }
                        else
                        {
                            Logger.Debug("AccountsServices.Login " + we.ToString());
                            // no http status code available
                        }
                    }
                    catch (Exception exe)
                    {
                        Logger.Error("AccountsServices.Login " + exe.ToString());
                    }
                }

                #endregion

                SqlParameter[] sqlParam = new SqlParameter[1];
                sqlParam[0] = new SqlParameter("@Username", System.Data.SqlDbType.VarChar);
                sqlParam[0].Value = username;

                getUsernameSqlQuery = "Select [RedmineUserId]  from [dbo].[Users] where [Username] = @Username and [Active] = 1";

                DataTable dt = new DataTable();
                string redmineUserIdDB = dc.GetSingleCell(getUsernameSqlQuery, sqlParam);

                if (!string.IsNullOrWhiteSpace(redmineUserIdDB))
                {
                    string randomToken = Encryption.RandomStringGenerator();
                    string setTokenQuery = "Update [dbo].[Users] set [Token]=@Token  where [RedmineUserId] = @RedmineUserId";

                    sqlParam = new SqlParameter[2];
                    sqlParam[0] = new SqlParameter("@Token", System.Data.SqlDbType.VarChar);
                    sqlParam[0].Value = randomToken;
                    sqlParam[1] = new SqlParameter("@RedmineUserId", System.Data.SqlDbType.VarChar);
                    sqlParam[1].Value = redmineUserIdDB;

                    if (dc.InsertUpdateDelete(setTokenQuery, sqlParam))
                    {
                        Token = Encryption.Encrypt(randomToken);
                        result = "Login Successfull";
                    }
                    else
                        result = "Server Error";
                }
                else
                    return "Sorry!! User Not Authorized To View Campaigns";

                return result;

            }
            catch (Exception ex)
            {
                Logger.Error("AccountsServices.Login Exception: " + ex.ToString());
                Token = string.Empty;
                return "Sorry!! Api exception occured. Please check logs";
            }
        }

        /// <summary>
        /// <para>Check for Token in Database and Return </para>
        /// <para>empty string id token doesn't exists in DB or token is not present</para>
        /// <para>Returns Exception on exception</para>
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        public string CheckToken(HttpRequestMessage Request)
        {
            try
            {
                var headers = Request.Headers;

                if (headers.Contains("Authorization"))
                {
                    string token = headers.GetValues("Authorization").First();

                    string sqlCheckTokenUser = "Select [RedmineUserId] from [Users] where [Token]=@Token";
                    SqlParameter[] sqlParam = new SqlParameter[1];
                    sqlParam[0] = new SqlParameter("@Token", System.Data.SqlDbType.VarChar);
                    sqlParam[0].Value = Encryption.Decrypt(token);
                    string redmineUserId = dc.GetSingleCell(sqlCheckTokenUser, sqlParam);
                    if (!string.IsNullOrEmpty(redmineUserId))
                        return redmineUserId;
                    else
                        return string.Empty;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("AccountsServices.CheckToken Exeption" + ex.ToString());
                return "Exception";
            }
        }

        public string DeleteToken(string redmineUserId)
        {
            try
            {
                string sqlDeleteUserToken = "Update [Users] set [Token]='' where  [RedmineUserId]=@RedmineUserId";
                SqlParameter[] sqlParam = new SqlParameter[1];
                sqlParam[0] = new SqlParameter("@RedmineUserId", System.Data.SqlDbType.VarChar);
                sqlParam[0].Value = redmineUserId;

                if (dc.InsertUpdateDelete(sqlDeleteUserToken, sqlParam))
                    return "Successfully Logged Out";
                else
                    return "Logout Unsuccessfull";

            }
            catch (Exception ex)
            {
                Logger.Error("AccountsServices.DeleteToken Exeption" + ex.ToString());
                return "Api Exception Logout Failed";
            }
        }

    }
}