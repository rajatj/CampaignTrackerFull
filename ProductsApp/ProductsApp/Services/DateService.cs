﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
namespace ProductsApp.Services
{
    public class DateService
    {
        public string GetCreatedDatePerProject(int pid)
        {
            string date = null;
            string url = "http://10.97.85.87/redmine//projects.json?project_id=" + pid;
            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();

            using (Stream responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                string str = reader.ReadToEnd();
                RootObject root_date =
                                (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);

                List<Project> project_list = root_date.projects;
                foreach (var pro in project_list)
                {
                    if (pro.id == pid)
                        date = pro.created_on;
                }
                return date;
            }
        }
    }
}