﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace ProductsApp.Services
{
    public class EmailService
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public static bool EstimateToDevlopmentMovedMail(string projectId , string projectName)
        {
            try
            {
                string subject = "Campaign Moved Successfully";
                string body = "Capaign Name: "+projectName+" Id: "+projectId+" has moved from estimate to development stage. Thanks !!";
                List<string> mailToList = new List<string>();
                SqlQueries.EmailNotification emailNotification = new SqlQueries.EmailNotification();
                DataTable dt = emailNotification.GetUserEmailIds(projectId);
                EmailService emailService = new EmailService();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    mailToList.Add(dt.Rows[i]["RedmineEmailId"].ToString());
                }
                if (emailService.SetMailQueue(body, subject, mailToList))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.Error("EmailService.EstimateToDevlopmentMovedMail " + ex.ToString());
                return false;
            }
        }

        public bool DailyMailNotifications()
        {
            try
            {
                SqlQueries.EmailNotification notification = new SqlQueries.EmailNotification();

                DataTable dtdue_date_Reminder = new DataTable();
                DataTable dtExit_Date_Reminder = new DataTable();
                DataTable dtLaunch_Date_Reminder = new DataTable();

                DataTable dtdue_date_Moved = new DataTable();
                DataTable dtExit_Date_Moved = new DataTable();
                DataTable dtLaunch_Date_Moved = new DataTable();

                dtdue_date_Reminder = notification.Reminder("due_date");
                dtExit_Date_Reminder = notification.Reminder("Exit_Date");
                dtLaunch_Date_Reminder = notification.Reminder("Launch_Date");

                dtdue_date_Moved = notification.MoveTabs("due_date");
                dtExit_Date_Moved = notification.MoveTabs("Exit_Date");
                dtLaunch_Date_Moved = notification.MoveTabs("Launch_Date");

                Email emailData = new Email();
                emailData.UserName = ConfigurationManager.AppSettings["Username_Email"].Trim();
                emailData.Password = ConfigurationManager.AppSettings["Password_Email"].Trim();
                emailData.EmailFrom = ConfigurationManager.AppSettings["Username_Email"].Trim();

                string daysBeforeReminderMailTrigger = ConfigurationManager.AppSettings["daysBeforeReminderMailTrigger"].Trim();
                #region// reminder mails
                Notifier(dtdue_date_Reminder, emailData, "due_date");
                Notifier(dtExit_Date_Reminder, emailData, "Exit_Date");
                Notifier(dtLaunch_Date_Reminder, emailData, "Launch_Date");

                #endregion
                #region // Moved mails
                Notifier(dtdue_date_Moved, emailData, "due_date_Moved");

                Notifier(dtExit_Date_Moved, emailData, "Exit_Date_Moved");

                Notifier(dtLaunch_Date_Moved, emailData, "Launch_Date_Moved");
                #endregion
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("EmailService.DailyMailNotifications " + ex.ToString());
                return false;
            }
        }

        public bool ExecuteMailQueue()
        {
            try
            {
                Email emailData = new Email();
                emailData.UserName = ConfigurationManager.AppSettings["Username_Email"].Trim();
                emailData.Password = ConfigurationManager.AppSettings["Password_Email"].Trim();
                emailData.EmailFrom = ConfigurationManager.AppSettings["Username_Email"].Trim();

                string subject = string.Empty;
                string body = string.Empty;
                string emailTo = string.Empty;
                string id = string.Empty;

                SqlQueries.EmailNotification emailNotification = new SqlQueries.EmailNotification();
                if (!emailNotification.CreateMailQueTableIfNotExists())
                {
                    Logger.Debug("EmailService.ExecuteMailQueue Failed To Create Table MailQueue");
                    return false;
                }
                DataTable mailTable = emailNotification.GetMailQueue();

                for (int i = 0; i < mailTable.Rows.Count; i++)
                {
                    id = mailTable.Rows[i]["Id"].ToString();
                    subject = mailTable.Rows[i]["MailSubject"].ToString();
                    body = mailTable.Rows[i]["MailBody"].ToString();
                    emailTo = mailTable.Rows[i]["MailTo"].ToString();

                    emailData.Subject = subject;
                    emailData.Body = body;
                    emailData.EmailTo = emailTo;

                    if (SendEmail(emailData))
                    {
                        emailNotification.DisableMailQueue(id);
                    }
                    else
                    {
                        emailNotification.IncreaseRetryCount(id);
                    }
                }


                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("EmailService.ExecuteMailQueue " + ex.ToString());
                return false;
            }
        }
        public bool SetMailQueue(string body, string subject, List<string> mailTo)
        {
            try
            {
                bool result = true;
                SqlQueries.EmailNotification emailNotification = new SqlQueries.EmailNotification();
                mailTo.ForEach(mail =>
                {
                    if (!emailNotification.InsertMailQueue(body, subject, mail))
                        result = false;
                });

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error("EmailService.SetMailQueue " + ex.ToString());
                return false;
            }
        }

        private void Notifier(DataTable dt, Email emailData, string DateType)
        {
            try
            {
                string daysBeforeReminderMailTrigger = ConfigurationManager.AppSettings["daysBeforeReminderMailTrigger"].Trim();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string projectId = dt.Rows[i]["Project_Id"].ToString();
                    string name = dt.Rows[i]["Name"].ToString();
                    string EmailTo = GetListOfStakeholdersEmail(projectId);
                    if (string.IsNullOrEmpty(EmailTo))
                        continue;
                    emailData.EmailTo = EmailTo;
                    switch (DateType)
                    {
                        case "due_date":
                            emailData.Subject = "Due Date Reminder Mail for Project: " + name + "and Project Id: " + projectId;
                            emailData.Body = "The Due Date for Project: " + name + " has " + daysBeforeReminderMailTrigger + " days left. This mail is indended for all the stakeholders. Kindly fill out the estimates. Ignore if estimates have been filled. Thanks!! ";
                            break;
                        case "Exit_Date":
                            emailData.Subject = "Exit Date Reminder Mail for Project: " + name + "and Project Id: " + projectId;
                            emailData.Body = "The Exit Date for Project: " + name + " has " + daysBeforeReminderMailTrigger + " days left. This mail is indended for all the stakeholders. Campaign is about to move to Archived state as the exit date is about to come. Thanks!! ";
                            break;
                        case "Launch_Date":
                            emailData.Subject = "Launch Date Reminder Mail for Project: " + name + "and Project Id: " + projectId;
                            emailData.Body = "The Launch Date for Project: " + name + " has " + daysBeforeReminderMailTrigger + " days left. This mail is indended for all the stakeholders. Campaign is about to move to Live state as the launch date is about to come. Thanks!! ";
                            break;
                        case "due_date_Moved":
                            emailData.Subject = "Due Date Expiration Mail for Project: " + name + "and Project Id: " + projectId;
                            emailData.Body = "The Due Date for Project: " + name + " has been expired. Please put in new due date or fill in estimates and approved the campaign. Ignore the mail if already done. Thanks !!";
                            break;
                        case "Exit_Date_Moved":
                            emailData.Subject = "Archived!! Project: " + name + "and Project Id: " + projectId;
                            emailData.Body = "The Project: " + name + " has been moved from Live to Archived state as the Exit Date has been met. Thanks !!";
                            break;
                        case "Launch_Date_Moved":
                            emailData.Subject = "Live!! Project: " + name + "and Project Id: " + projectId;
                            emailData.Body = "The Project: " + name + " has been moved from Development to Live state as the Launch Date has been met. Thanks !!";
                            break;
                        default:
                            emailData.Subject = "";
                            emailData.Body = "";
                            break;

                    }
                    if (!SendEmail(emailData))
                        Logger.Error("Email Sending Failed . Check Logs");
                }
            }
            catch (Exception ex)
            {
                Logger.Error("EmailService.DailyMailNotifications " + ex.ToString());
            }

        }

        private bool SendEmail(Email emailData)
        {
            try
            {
                using (MailMessage mm = new MailMessage(emailData.EmailFrom, emailData.EmailTo))
                {
                    mm.Subject = emailData.Subject;
                    mm.Body = emailData.Body;
                    SmtpClient smtp = new SmtpClient();
                    string smtpAddress = ConfigurationManager.AppSettings["SmtpAddress"].Trim();
                    int smtpPort = 0;
                    int.TryParse(ConfigurationManager.AppSettings["SmtpPort"].Trim(), out smtpPort);
                    smtp.Host = smtpAddress;//"smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(emailData.UserName, emailData.Password);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = smtpPort;
                    smtp.Send(mm);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("EmailService.SendEmail " + ex.ToString());
                return false;
            }

        }

        private string GetListOfStakeholdersEmail(string projectId)
        {
            SqlQueries.EmailNotification notif = new SqlQueries.EmailNotification();
            DataTable dt = new DataTable();
            dt = notif.GetUserEmailIds(projectId);
            string emailTo = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i > 0)
                {
                    emailTo += ",";
                }
                emailTo += dt.Rows[i]["RedmineEmailId"].ToString();
            }

            return emailTo;
        }
    }
}