﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
namespace ProductsApp.Services
{
    public class IssueService
    {

        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public string[] GetProgressPerProject(int pid)
        {
            try
            {
                string[] returningValue = new string[2];
                double prog = 0.0;
                int total = 0;
                int closed_count = 0;
                string issue_status = "";
                string url = ConfigurationManager.AppSettings["hostUrl"] + "issues.json?status_id=closed&limit=100&key=" + ConfigurationManager.AppSettings["apiKey"] + "&project_id=" + pid;
                string url1 = ConfigurationManager.AppSettings["hostUrl"] + "issues.json?status_id=*&limit=100&key=" + ConfigurationManager.AppSettings["apiKey"] + "&project_id=" + pid;


                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();



                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    string str = reader.ReadToEnd();
                    RootObject time =
                                    (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);
                    closed_count = time.total_count;

                }
                WebRequest request1 = WebRequest.Create(url1);
                WebResponse response1 = request1.GetResponse();

                int feature_count = 0, closed_feature_count = 0;
                int feedback_count = 0, closed_feedback_count = 0;

                using (Stream responseStream1 = response1.GetResponseStream())
                {
                    StreamReader reader1 = new StreamReader(responseStream1, Encoding.UTF8);
                    string str1 = reader1.ReadToEnd();
                    RootObject time1 =
                                    (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str1);

                    total = time1.total_count;


                    List<Issue> issue_list = time1.issues;


                    foreach (var issue in issue_list)
                    {
                        if (issue.tracker.name == "Feature")
                        {
                            feature_count++;
                            if (issue.status.name == "Closed")
                            {
                                closed_feature_count++;
                            }
                        }
                        if (issue.tracker.name == "Feedback/Enhancement")
                        {
                            feedback_count++;
                            if (issue.status.name == "Closed")
                            {
                                closed_feedback_count++;
                            }

                        }

                    }

                    if (feature_count == 0)
                        issue_status = "Design";
                    else if (closed_feature_count < feature_count)
                        issue_status = "Development";
                    else if (closed_feature_count == feature_count && feedback_count == 0)
                        issue_status = "Testing";
                    else if (feedback_count == closed_feedback_count)
                        issue_status = "Live";

                }
                if (feature_count != 0)
                {
                    prog = (closed_feature_count * 100.0) / feature_count;
                    //   prog = (closed_count * 100.0) / total;
                }
                returningValue[0] = issue_status;
                returningValue[1] = prog.ToString();
                return returningValue;
            }
            catch (Exception ex)
            {
                Logger.Error("IssueService.GetProgressPerProject" + ex.ToString());
                string[] result = { "No Status", "0.0" };
                return result;
            }
        }

    }
}