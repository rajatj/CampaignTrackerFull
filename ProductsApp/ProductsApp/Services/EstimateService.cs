﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;

namespace ProductsApp.Services
{
    public class EstimateService
    {
        public string CreateAtProjectCreation(Estimate data)
        {

           var json = new JavaScriptSerializer().Serialize(data);
            //   string result = "";
            String finalString = "";
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode rootNode = xmlDoc.CreateElement("issue");
            xmlDoc.AppendChild(rootNode);

            XmlNode idNode = xmlDoc.CreateElement("project_id");
            idNode.InnerText = "386";// project id of estmate management project 
            rootNode.AppendChild(idNode);

            XmlNode nameNode = xmlDoc.CreateElement("subject");
            nameNode.InnerText = "Name of project";
            rootNode.AppendChild(nameNode);
            try
            {
                XmlNode identifierNode = xmlDoc.CreateElement("priority_id");
                // Priority p = new Priority();
                identifierNode.InnerText = "2";
                rootNode.AppendChild(identifierNode);
            }
            catch { }
            XmlNode statusNode = xmlDoc.CreateElement("status_id");
            statusNode.InnerText = "1";
            rootNode.AppendChild(statusNode);

            XmlNode trackerNode = xmlDoc.CreateElement("tracker_id");
            trackerNode.InnerText = "19";
            rootNode.AppendChild(trackerNode);

            XmlNode discriptionNode = xmlDoc.CreateElement("description");
            discriptionNode.InnerText = json;
            rootNode.AppendChild(discriptionNode);


                //XmlNode custom_fieldsNode = xmlDoc.CreateElement("custom_fields");
                //XmlAttribute attribute = xmlDoc.CreateAttribute("type");
                //attribute.Value = "array";
                //custom_fieldsNode.Attributes.Append(attribute);

                //List<CustomField> cf = new List<CustomField>();
                //cf = data.custom_fields;
                //foreach (var v in cf)
                //{

                //    XmlNode custom_fieldNode = xmlDoc.CreateElement("custom_field");

                //    XmlAttribute attribute1 = xmlDoc.CreateAttribute("id");
                //    attribute1.Value = v.id.ToString();
                //    custom_fieldNode.Attributes.Append(attribute1);

                //    XmlAttribute attribute2 = xmlDoc.CreateAttribute("name");
                //    attribute2.Value = v.name;
                //    custom_fieldNode.Attributes.Append(attribute2);

                //    XmlNode valueNode = xmlDoc.CreateElement("value");
                //    valueNode.InnerText = v.value;
                //    custom_fieldNode.AppendChild(valueNode);

                //    custom_fieldsNode.AppendChild(custom_fieldNode);

                //}
                //rootNode.AppendChild(custom_fieldsNode);
        
            string XmlizedString = "";
            using (StringWriter sw = new StringWriter())
            {
                using (XmlTextWriter tx = new XmlTextWriter(sw))
                {
                    xmlDoc.WriteTo(tx);
                    XmlizedString = sw.ToString();
                }
            }

            finalString = XmlizedString.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
            string posturl = System.Configuration.ConfigurationManager.AppSettings["hostUrl"] + "issues.xml?key=" + System.Configuration.ConfigurationManager.AppSettings["apiKey"];
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(posturl);
            byte[] bytes;
            //       finalString = "<issue><project_id>65</project_id><subject>Testing47</subject><priority_id>2</priority_id><status_id>1</status_id><tracker_id>14</tracker_id></issue>";
            bytes = System.Text.Encoding.ASCII.GetBytes(finalString);
            request.ContentType = "application/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            Uri res = (Uri)response.ResponseUri;
            return response.StatusCode.ToString();
          //  return json;
        }
     
    }
}