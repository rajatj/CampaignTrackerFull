﻿using ProductsApp.Models;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace ProductsApp.Services
{
    public class ProjectDetailService
    {
        DatabaseConnection dc = new DatabaseConnection();
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public List<Issue> Details(int pro_id, int tracker_id, int offset, int limit, string redmineUserId)
        {
            try
            {
                RootObject root = new RootObject();
                List<Issue> IssueVM = new List<Issue>();
                List<Issue> issues_list = new List<Issue>();
                string str = "";
                //    bool next_tab = false;

                string getUserApiKey = "Select [RedmineApiKey] from [Users] where [RedmineUserId] = '" + redmineUserId + "'";
                string UserApiKey = dc.GetSingleCell(getUserApiKey);
                string url = ConfigurationSettings.AppSettings["hostUrl"] + "issues.json?project_id=" + pro_id + "&tracker_id=" + tracker_id + "&status_id=*" + "&offset=" + offset + "&limit=" + limit + "&key=" + UserApiKey;// ConfigurationSettings.AppSettings["apiKey"];
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    str = reader.ReadToEnd();
                    root = (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);
                    issues_list = root.issues;
                }

                return issues_list;
            }
            catch (WebException we)
            {
                Logger.Error("ProjectDetailService.Details: " + we.ToString());
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error("ProjectDetailService.Details: " + ex.ToString());
                return null;
            }
        }

        public Return_list IssueDetails(int pro_id, string redmineUserId)
        {
            try
            {
                int total_bug_New = 0;
                int total_bug_Closed = 0;
                int total_feature_Closed = 0;
                int total_feature_New = 0;
                int total_feedback_Closed = 0;
                int total_feedback_New = 0;
                int total_testcase_Closed = 0;
                int total_testcase_New = 0;
                int total_testScenarios_Closed = 0;
                int total_testScenarios_New = 0;
                RootObject root = new RootObject();
                List<Issue> IssueVM = new List<Issue>();
                List<Issue> issues_list = new List<Issue>();
                //List<int> Return_list = new List<int>();
                Return_list result = new Return_list();
                string str = "";
                //    bool next_tab = false;
                string getUserApiKey = "Select [RedmineApiKey] from [Users] where [RedmineUserId] = '" + redmineUserId + "'";
                string UserApiKey = dc.GetSingleCell(getUserApiKey);
                string url_bug = ConfigurationSettings.AppSettings["hostUrl"] + "issues.json?project_id=" + pro_id + "&tracker_id=1&status_id=*&limit=100&key=" + UserApiKey;// ConfigurationSettings.AppSettings["apiKey"];

                WebRequest request_bug_closed = WebRequest.Create(url_bug);
                WebResponse response_bug_close = request_bug_closed.GetResponse();
                using (Stream responseStream = response_bug_close.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    str = reader.ReadToEnd();
                    root = (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);
                    issues_list = root.issues;

                    foreach (Issue issue in issues_list)
                    {

                        //if (issue.status_id == 1)
                        if (issue.status.id != 5)
                        {
                            total_bug_New++;
                        }
                        if (issue.status.id == 5)
                        {
                            total_bug_Closed++;
                        }
                    }
                    result.total_bug_New = total_bug_New;
                    result.total_bug_Closed = total_bug_Closed;

                }

                // feature 

                string url_feature = ConfigurationSettings.AppSettings["hostUrl"] + "issues.json?project_id=" + pro_id + "&tracker_id=2&status_id=*&limit=100 &key=" + ConfigurationSettings.AppSettings["apiKey"];
                WebRequest request_feature = WebRequest.Create(url_feature);
                WebResponse response_feature = request_feature.GetResponse();
                using (Stream responseStream = response_feature.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    str = reader.ReadToEnd();
                    root = (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);
                    issues_list = root.issues;
                    foreach (var issue in issues_list)
                    {
                        if (issue.status.id == 5)
                        {
                            total_feature_Closed++;
                        }
                        if (issue.status.id != 5)
                        {
                            total_feature_New++;
                        }
                    }
                    result.total_feature_Closed = total_feature_Closed;
                    result.total_feature_New = total_feature_New;
                }

                //feedback
                string url_feedback = ConfigurationSettings.AppSettings["hostUrl"] + "issues.json?project_id=" + pro_id + "&tracker_id=12&status_id=*&limit=100&key=" + ConfigurationSettings.AppSettings["apiKey"];
                WebRequest request_feedback = WebRequest.Create(url_feedback);
                WebResponse response_feedback = request_feedback.GetResponse();
                using (Stream responseStream = response_feedback.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    str = reader.ReadToEnd();
                    root = (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);
                    issues_list = root.issues;
                    foreach (var issue in issues_list)
                    {
                        if (issue.status.id == 5)
                        {
                            total_feedback_Closed++;
                        }
                        if (issue.status.id != 5)
                        {
                            total_feedback_New++;
                        }
                    }
                    result.total_feedback_Closed = total_feedback_Closed;
                    result.total_feedback_New = total_feedback_New;
                }
                //test case

                string url_testcase = ConfigurationSettings.AppSettings["hostUrl"] + "issues.json?project_id=" + pro_id + "&tracker_id=9&status_id=*&limit=100&key=" + ConfigurationSettings.AppSettings["apiKey"];
                WebRequest request_testcase = WebRequest.Create(url_testcase);
                WebResponse response_testcase = request_testcase.GetResponse();
                using (Stream responseStream = response_testcase.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    str = reader.ReadToEnd();
                    root = (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);
                    issues_list = root.issues;
                    foreach (var issue in issues_list)
                    {
                        if (issue.status.id == 5)
                        {
                            total_testcase_Closed++;
                        }
                        if (issue.status.id != 5)
                        {
                            total_testcase_New++;
                        }
                    }
                    result.total_testcase_Closed = total_testcase_Closed;
                    result.total_testcase_New = total_testcase_New;
                }
                // test Scenarios
                string url_Scenarios = ConfigurationSettings.AppSettings["hostUrl"] + "issues.json?project_id=" + pro_id + "&tracker_id=10&status_id=*&limit=100&key=" + ConfigurationSettings.AppSettings["apiKey"];
                WebRequest request_testScenarios = WebRequest.Create(url_Scenarios);
                WebResponse response_testScenarios = request_testScenarios.GetResponse();
                using (Stream responseStream = response_testScenarios.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    str = reader.ReadToEnd();
                    root = (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);
                    issues_list = root.issues;
                    foreach (var issue in issues_list)
                    {
                        if (issue.status.id == 5)
                        {
                            total_testScenarios_Closed++;
                        }
                        if (issue.status.id != 5)
                        {
                            total_testScenarios_New++;
                        }
                    }
                    result.total_testScenarios_Closed = total_testScenarios_Closed;
                    result.total_testScenarios_New = total_testScenarios_New;
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error("ProjectDetailService.IssueDetails" + ex.ToString());
                return null;
            }
        }



    }
}