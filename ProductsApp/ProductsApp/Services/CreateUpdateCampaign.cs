﻿using ProductsApp.Models;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Xml;

namespace ProductsApp.Services
{
    public class CreateUpdateCampaign
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        static DatabaseConnection dc = new DatabaseConnection();
        public static string CreateUpdate(Project data, string redmineUserId)
        {
            string returnResult = string.Empty;
            try
            {

                if (data == null)
                {
                    Logger.Debug("CreateUpdateCampaign.CreateUpdate : Null Project Data ");
                    returnResult = "Invalid Data Recieved";
                    return returnResult;
                }
                if (data.response_put && data.id == 0)
                {
                    Logger.Debug("CreateUpdateCampaign.CreateUpdate : Can't Update Project With No Id");
                    returnResult = "Can't Update Project With No Id";
                    return returnResult;
                }


                string dateFromatDB = "dd-MMM-yyyy";
                string dateFromatData = "yyyy-MM-dd";
                string exit_date = string.Empty;
                double project_cost = 0;
                string producer_name = string.Empty;
                string market = string.Empty;
                string hosting_platform = string.Empty;
                string status = string.Empty;
                double progress = 0;
                string live_url = string.Empty;
                string development_start_date = string.Empty;
                string launch_date = string.Empty;
                string post_result = string.Empty;
                string due_date = string.Empty;
                string image_url = string.Empty;
                string pending_with = string.Empty;
                double estimateTime = 0;
                string qa_url = string.Empty;
                if (data.custom_fields != null)
                {
                    for (int i = 0; i < data.custom_fields.Count; i++)
                    {
                        if (data.custom_fields[i].name == "Image")
                            image_url = data.custom_fields[i].value;
                        if (data.custom_fields[i].name == "Exit Date")
                            exit_date = data.custom_fields[i].value;
                        if (data.custom_fields[i].name == "Estimates Shared with Client")
                        {
                            Double.TryParse(data.custom_fields[i].value, out estimateTime);
                            //estimateTime=project_cost;
                            project_cost = estimateTime * (Convert.ToDouble((ConfigurationManager.AppSettings["cost"])));
                        }
                        if (data.custom_fields[i].name == "Due Date")
                            due_date = data.custom_fields[i].value;
                        if (data.custom_fields[i].name == "QA CName")
                            qa_url = data.custom_fields[i].value;
                        if (data.custom_fields[i].name == "Producer")
                            producer_name = data.custom_fields[i].value;
                        if (data.custom_fields[i].name == "Market")
                            market = data.custom_fields[i].value;
                        if (data.custom_fields[i].name == "Project Technology")
                            hosting_platform = data.custom_fields[i].value;
                        if (data.custom_fields[i].name == "Live URL")
                            live_url = data.custom_fields[i].value;
                        if (data.custom_fields[i].name == "Development Start Date")
                            development_start_date = data.custom_fields[i].value;
                        if (data.custom_fields[i].name == "Launch Date")
                            launch_date = data.custom_fields[i].value;
                        if (data.custom_fields[i].name == "Pending With")
                            pending_with = data.custom_fields[i].value;

                    }
                }
                //Parse datetime for DataValidation class
                #region Date validation
                DateTime exitDate;
                DateTime launchDate;
                DateTime startDate;
                DateTime dueDate;

                bool isExitDateValid = DateTime.TryParseExact(exit_date, dateFromatData, CultureInfo.InvariantCulture, DateTimeStyles.None, out exitDate);
                bool isDevStartDateValid = DateTime.TryParseExact(development_start_date, dateFromatData, CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate);
                bool isLaunchDateValid = DateTime.TryParseExact(launch_date, dateFromatData, CultureInfo.InvariantCulture, DateTimeStyles.None, out launchDate);
                bool isDueDateValid = DateTime.TryParseExact(due_date, dateFromatData, CultureInfo.InvariantCulture, DateTimeStyles.None, out dueDate);


                if (!string.IsNullOrWhiteSpace(exit_date) && !isExitDateValid)
                {
                    Logger.Debug("CreateUpdateCampaign.CreateUpdate : Invalid Exit Date Format");
                    returnResult = "Invalid Exit Date Format";
                    return returnResult;

                }
                if (!string.IsNullOrWhiteSpace(development_start_date) && !isDevStartDateValid)
                {
                    Logger.Debug("CreateUpdateCampaign.CreateUpdate : Invalid Start Date Format");
                    returnResult = "Invalid Start Date Format";
                    return returnResult;
                }

                if (!string.IsNullOrWhiteSpace(launch_date) && !isLaunchDateValid)
                {
                    Logger.Debug("CreateUpdateCampaign.CreateUpdate : Invalid Launch Date Format");
                    returnResult = "Invalid Launch Date Format";
                    return returnResult;
                }

                if (((startDate > launchDate) || (launchDate > exitDate) || (startDate > exitDate)) && ((launch_date != "") && (exit_date != "")))
                {
                    Logger.Debug("CreateUpdateCampaign.CreateUpdate : Start Date < Launch Date < Exit Date");
                    returnResult = "Start Date < Launch Date < Exit Date";
                    return returnResult;
                }
                #endregion

                exit_date = exitDate.ToString(dateFromatDB);
                launch_date = launchDate.ToString(dateFromatDB);
                development_start_date = startDate.ToString(dateFromatDB);
                due_date = dueDate.ToString(dateFromatDB);

                ProjectVM provm = new ProjectVM();
                provm.id = data.id;
                provm.name = data.name;
                provm.exit_date = exit_date;
                provm.project_cost = project_cost;
                provm.producer_name = producer_name;
                provm.market = market;
                provm.hosting_platform = hosting_platform;
                provm.status = status;
                provm.progress = progress;
                provm.live_url = live_url;
                provm.development_start_date = development_start_date;
                provm.launch_date = launch_date;
                provm.EstimatedTime = estimateTime;
                provm.due_date = due_date;
                provm.image_url = image_url;
                provm.pending_with = pending_with;
                provm.qa_url = qa_url;


                string getUserApiKey = "Select [RedmineApiKey] from [Users] where [RedmineUserId] = '" + redmineUserId + "'";
                string UserApiKey = dc.GetSingleCell(getUserApiKey);

                string getPendingWith = "Select [pending_with] from [Project_Main] where [Project_Id] = '" + data.id + "'";
                string pendingWith = dc.GetSingleCell(getPendingWith);

                #region // Send mails on change of pending_with status
                if (data.response_put)
                {
                    if (!EmailService.EstimateToDevlopmentMovedMail(data.id.ToString(), data.name))
                        Logger.Debug("CreateUpdateCampaign.CreateUpdate: Unable to send EstimateToDevlopmentMovedMails ");

                    if ((pending_with.Trim().ToUpper() != pendingWith.Trim().ToUpper()) && !string.IsNullOrEmpty(pending_with))
                    {
                        if (!EmailService.EstimateToDevlopmentMovedMail(data.id.ToString() , data.name))
                            Logger.Debug("CreateUpdateCampaign.CreateUpdate: Unable to send EstimateToDevlopmentMovedMails ");
                    }
                }
                #endregion

                string requestUri = System.Configuration.ConfigurationManager.AppSettings["hostUrl"] + "projects.xml?key=" + UserApiKey;//System.Configuration.ConfigurationManager.AppSettings["apiKey"];

                AddProService aps = new AddProService();

                string XmlizedString = XmlFormater(data);

                String finalString = XmlizedString.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

                if (data.response_put == false)
                {
                    post_result = aps.postXMLData(requestUri, finalString, provm);
                }
                else
                {
                    int idchange = data.id;
                    string requesturi_put = ConfigurationManager.AppSettings["hostUrl"] + "projects/" + idchange + ".xml?key=" + UserApiKey;// ConfigurationManager.AppSettings["apiKey"];
                    post_result = aps.putXMLData(requesturi_put, finalString, provm);
                    if (post_result != "Success")
                    {
                        Logger.Debug("CreateUpdateCampaign.CreateUpdate : requestUri " + requesturi_put);
                        Logger.Debug("CreateUpdateCampaign.CreateUpdate : xml " + finalString);

                    }
                }

                if (post_result == "Success")
                {
                    return "Success";
                }
                else
                {
                    Logger.Debug("CreateUpdateCampaign.CreateUpdate : Post Failed" + post_result);
                    Logger.Debug("CreateUpdateCampaign.CreateUpdate : requestUri " + requestUri);
                    Logger.Debug("CreateUpdateCampaign.CreateUpdate : xml " + finalString);
                    return post_result;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("CreateUpdateCampaign.CreateUpdate " + ex.ToString());
                returnResult = "Api Server Exception";
                return returnResult;
            }
        }

        private static string XmlFormater(Project data)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode rootNode = xmlDoc.CreateElement("project");
            xmlDoc.AppendChild(rootNode);

            XmlNode idNode = xmlDoc.CreateElement("id");
            idNode.InnerText = (data.id).ToString();
            rootNode.AppendChild(idNode);

            XmlNode nameNode = xmlDoc.CreateElement("name");
            nameNode.InnerText = data.name;
            rootNode.AppendChild(nameNode);

            XmlNode identifierNode = xmlDoc.CreateElement("identifier");
            identifierNode.InnerText = data.identifier;
            rootNode.AppendChild(identifierNode);

            XmlNode ParentNode = xmlDoc.CreateElement("parent_id");
            ParentNode.InnerText = data.parent_id;
            rootNode.AppendChild(ParentNode);

            rootNode.AppendChild(ParentNode);

            string[] module = ConfigurationManager.AppSettings["module"].Split(';');
            string[] tracking = ConfigurationManager.AppSettings["tracking"].Split(';');

            //  int i = module.Length;
            for (int i = 0; i < module.Length; i++)
            {
                XmlNode moduleNode = xmlDoc.CreateElement("enabled_module_names");
                moduleNode.InnerText = module[i];
                rootNode.AppendChild(moduleNode);
            }

            for (int j = 0; j < tracking.Length; j++)
            {
                XmlNode trackerNode = xmlDoc.CreateElement("tracker_ids");
                trackerNode.InnerText = tracking[j];
                rootNode.AppendChild(trackerNode);
            }

            XmlNode custom_fieldsNode = xmlDoc.CreateElement("custom_fields");
            XmlAttribute attribute = xmlDoc.CreateAttribute("type");
            attribute.Value = "array";
            custom_fieldsNode.Attributes.Append(attribute);

            foreach (var v in data.custom_fields)
            {

                XmlNode custom_fieldNode = xmlDoc.CreateElement("custom_field");


                XmlAttribute attribute1 = xmlDoc.CreateAttribute("id");
                attribute1.Value = v.id.ToString();
                custom_fieldNode.Attributes.Append(attribute1);

                XmlAttribute attribute2 = xmlDoc.CreateAttribute("name");
                attribute2.Value = v.name;
                custom_fieldNode.Attributes.Append(attribute2);

                XmlNode valueNode = xmlDoc.CreateElement("value");

                valueNode.InnerText = v.value;
                custom_fieldNode.AppendChild(valueNode);

                custom_fieldsNode.AppendChild(custom_fieldNode);
            }

            rootNode.AppendChild(custom_fieldsNode);

            string XmlizedString = "";
            using (StringWriter sw = new StringWriter())
            {
                using (XmlTextWriter tx = new XmlTextWriter(sw))
                {
                    xmlDoc.WriteTo(tx);
                    XmlizedString = sw.ToString();
                }
            }
            return XmlizedString;
        }
    }
}