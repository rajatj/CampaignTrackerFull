﻿using Newtonsoft.Json;
using ProductsApp.Models;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace ProductsApp.Services
{
    public class JsonToSQL
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        static DatabaseConnection dc = new DatabaseConnection();

        public List<string> SyncProjects()
        {
            List<string> listReturnValues = new List<string>();
            try
            {
                List<ProjectVM> projectList = new List<ProjectVM>();
                ProjectService getProjects = new ProjectService();
                projectList = getProjects.GetAllProjects();

                foreach (ProjectVM proVM in projectList)
                {
                    SqlQueries.Project_Main_Tbl mainTableIsertionObject = new SqlQueries.Project_Main_Tbl();
                    string mainTableResult = mainTableIsertionObject.InsertOrUpdate(proVM);
                    //if (mainTableResult == "Success")
                    //{
                    //    SqlQueries.Membership_Tbl membershipTblObj;

                    //    foreach (Membership mem in proVM.members)
                    //    {
                    //        foreach (Role role in mem.roles)
                    //        {
                    //            membershipTblObj = new SqlQueries.Membership_Tbl();

                    //            string membershipResult = membershipTblObj.InsertOrUpdateMembership(mem.id.ToString(), mem.user.id.ToString(), role.name, proVM.id.ToString());

                    //        }
                    //    }

                    //}
                    listReturnValues.Add("ProjectId = " + proVM.id.ToString() + " MainTable : " + mainTableResult);



                }
                #region // delete project not in redmine
                DataTable dt = dc.GetDataTable("Select [Project_Id] from [dbo].[Project_Main] ");
                List<Int64> projectIdRedmine = new List<Int64>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < projectList.Count; i++)
                        {
                            projectIdRedmine.Add(projectList[i].id);
                        }
                        foreach (DataRow dr in dt.Rows)
                        {
                            //delete project which are not present
                            if (!projectIdRedmine.Contains(Convert.ToInt64(dr["Project_Id"])))
                            {
                                string deleteProjectRowQuery = "Delete from [Memberships] where Project_Id= '" + dr["Project_Id"].ToString() + "' Delete from Project_Main where Project_Id = '" + dr["Project_Id"].ToString() + "' ";
                                listReturnValues.Add("Rows Delted for ProjectId " + dr["Project_Id"].ToString() + ": " + dc.InsertUpdateDelete(deleteProjectRowQuery).ToString());
                            }
                        }

                    }
                }
                #endregion

                listReturnValues.Add("Sync Sucess");
                return listReturnValues;
            }
            catch (Exception ex)
            {
                Logger.Error("JsonToSQL.SyncData" + ex.ToString());
                listReturnValues.Add("Sync Failed:" + ex.ToString());
                return listReturnValues;

            }
        }

        public static List<string> SyncUsers()
        {
            try
            {
                string[] groupIdList = ConfigurationManager.AppSettings["Groups"].ToString().Split(',');
                List<string> listReturnValues = new List<string>();
                List<User> userListUser = new List<User>();
                foreach (string groups in groupIdList)
                {
                    string groupId = groups.Trim();

                    int limit = 100;
                    int offset = 0;
                    int totalCount = 1;

                    List<User> userList = new List<User>();

                    while (totalCount > offset)
                    {


                        string url = ConfigurationManager.AppSettings["hostUrl"] + "users.json?group_id=" + groupId + "&limit=" + limit + "&offset=" + offset + "&key=" + ConfigurationManager.AppSettings["apiKey"];
                        WebRequest request = WebRequest.Create(url);
                        WebResponse response = request.GetResponse();


                        using (Stream responseStream = response.GetResponseStream())
                        {

                            StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                            string str = reader.ReadToEnd();

                            RootUsers root = (RootUsers)Newtonsoft.Json.JsonConvert.DeserializeObject<RootUsers>(str);
                            userList = root.users;

                            totalCount = root.total_count;
                            offset = root.offset;
                            limit = root.limit;

                            for (int i = 0; i < userList.Count; i++, offset++)
                            {
                                string urlSingleUser = ConfigurationManager.AppSettings["hostUrl"] + "users/" + userList[i].id + ".json?include=memberships" + "&key=" + ConfigurationManager.AppSettings["apiKey"];
                                WebRequest requestUser = WebRequest.Create(urlSingleUser);
                                WebResponse responseUser = requestUser.GetResponse();


                                using (Stream responseStreamUser = responseUser.GetResponseStream())
                                {

                                    StreamReader readerUser = new StreamReader(responseStreamUser, Encoding.UTF8);
                                    string strUser = readerUser.ReadToEnd();

                                    RootSingleUser rootUser = (RootSingleUser)Newtonsoft.Json.JsonConvert.DeserializeObject<RootSingleUser>(strUser);
                                    
                                    if (!userList.Contains(rootUser.user))
                                        userListUser.Add(rootUser.user);

                                    SqlQueries.UsersAndMemberships_Tbl userMemTblInsertObj = new SqlQueries.UsersAndMemberships_Tbl();
                                    string membershipsIds = string.Empty;
                                    foreach (Membership mem in rootUser.user.memberships)
                                    {
                                        membershipsIds += mem.id.ToString() + ",";
                                    }
                                    listReturnValues.Add("Row insert/update for userId: " + rootUser.user.id + "memberships: " + membershipsIds + " Result: " + userMemTblInsertObj.InsertOrUpdateUserAndMembership(rootUser.user, rootUser.user.memberships));

                                }
                            }


                        }

                    }
                }
                #region // delete Users not in redmine
                DataTable dt = dc.GetDataTable("Select [RedmineUserId] from [dbo].[Users] ");
                List<Int64> userIdRedmine = new List<Int64>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < userListUser.Count; i++)
                        {
                            userIdRedmine.Add(userListUser[i].id);

                            DataTable dtMem = dc.GetDataTable("Select [Id] from [dbo].[Memberships] where RedmineUserId = '" + userListUser[i].id.ToString() + "'");

                            List<Int64> memIdUser = new List<Int64>();
                            if (dtMem != null)
                            {
                                if (dtMem.Rows.Count > 0)
                                {
                                    for (int j = 0; j < userListUser[i].memberships.Count; j++)
                                    {
                                        memIdUser.Add(userListUser[i].memberships[j].id);
                                    }

                                    foreach (DataRow drMem in dtMem.Rows)
                                    {
                                        if (!memIdUser.Contains(Convert.ToInt64(drMem["Id"])))
                                        {
                                            string deleteProjectRowQuery = " Delete from [Memberships] where ID= '" + drMem["Id"].ToString() + "'";
                                            listReturnValues.Add("Rows Deleted for Membership Table Id " + drMem["Id"].ToString() + ": " + dc.InsertUpdateDelete(deleteProjectRowQuery).ToString());
                                        }
                                    }
                                }
                            }
                        }
                        foreach (DataRow dr in dt.Rows)
                        {
                            long RedmineUserId = Convert.ToInt64(dr["RedmineUserId"]);
                            bool checkIfExists = userIdRedmine.Contains(RedmineUserId);
                            //delete project which are not present
                            if (!checkIfExists)
                            {
                                string deleteProjectRowQuery = "Delete from [Memberships] where RedmineUserId= '" + dr["RedmineUserId"].ToString() + "' Delete from Users where RedmineUserId = '" + dr["RedmineUserId"].ToString() + "' ";
                                listReturnValues.Add("Rows from Users and Membership table Delted for UserId  " + dr["RedmineUserId"].ToString() + ": " + dc.InsertUpdateDelete(deleteProjectRowQuery).ToString());
                            }
                        }

                    }
                }
                #endregion
                return listReturnValues;
            }
            catch (WebException we)
            {
                Logger.Error("JsonToSQL.SyncUsers" + we.ToString());
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error("JsonToSQL.SyncUsers" + ex.ToString());
                return null;
            }

        }

    }

}