﻿using ProductsApp.Models;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ProductsApp.Services
{
    public class SqlQueries
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public class Project_Main_Tbl
        {
            DatabaseConnection dc = new DatabaseConnection();

            /// <summary>
            /// <para>Insert or update Project_Main table </para>
            /// <para>returns "Project VM Null" string if parameter is null </para>
            /// <para>returns "Project ID Invalid" string if id is null </para>
            ///  <para>returns "Success" or "Fail" string on insert/update  </para>
            ///  <para>returns string which contains Exception: for uncaught exception</para>
            /// </summary>
            /// <param name="proVM">PojectVM class</param>
            /// <returns>string value regarding status of insert/update</returns>
            public string InsertOrUpdate(ProjectVM proVM)
            {
                try
                {
                    ProjectVM p = new ProjectVM();
                    if (proVM == null)
                        return "Project VM Null";

                    int arrayLength = 19;
                    #region  //paramNameSql
                    string[] paramNameSql = new string[arrayLength];
                    paramNameSql[0] = "@Project_Id";
                    paramNameSql[1] = "@EstimatedTime";
                    paramNameSql[2] = "@TotalTimeSpent";
                    paramNameSql[3] = "@Burndown";
                    paramNameSql[4] = "@Development_Start_Date";
                    paramNameSql[5] = "@Exit_Date";
                    paramNameSql[6] = "@Hosting_Platform";
                    paramNameSql[7] = "@Launch_Date";
                    paramNameSql[8] = "@Live_Url";
                    paramNameSql[9] = "@Market";
                    paramNameSql[10] = "@Name";
                    paramNameSql[11] = "@Producer_Name";
                    paramNameSql[12] = "@Progress";
                    paramNameSql[13] = "@Project_Cost";
                    paramNameSql[14] = "@Phase_Status";
                    paramNameSql[15] = "@pending_with";
                    paramNameSql[16] = "@due_date";
                    paramNameSql[17] = "@image_url";
                    paramNameSql[18] = "@qa_url";
                    #endregion

                    #region // sql dbtype array
                    SqlDbType[] dbType = new SqlDbType[arrayLength];
                    dbType[0] = SqlDbType.BigInt;//"@Project_Id";
                    dbType[1] = SqlDbType.Decimal;//"@EstimatedTime";
                    dbType[2] = SqlDbType.Decimal;//"@TotalTimeSpent";
                    dbType[3] = SqlDbType.Bit;// "@Burndown";
                    dbType[4] = SqlDbType.Date;// "@Development_Start_Date";
                    dbType[5] = SqlDbType.Date;// "@Exit_Date";
                    dbType[6] = SqlDbType.VarChar;//"@Hosting_Platform";
                    dbType[7] = SqlDbType.Date; // "@Launch_Date";
                    dbType[8] = SqlDbType.VarChar;// "@Live_Url";
                    dbType[9] = SqlDbType.VarChar;// "@Market";
                    dbType[10] = SqlDbType.VarChar; //"@Name";
                    dbType[11] = SqlDbType.VarChar;// "@Producer_Name";
                    dbType[12] = SqlDbType.Decimal;// "@Progress";
                    dbType[13] = SqlDbType.Decimal;// "@Project_Cost";
                    dbType[14] = SqlDbType.VarChar;// "@Phase_Status";
                    dbType[15] = SqlDbType.VarChar;// "@pending_with";
                    dbType[16] = SqlDbType.Date;// "@due_date";
                    dbType[17] = SqlDbType.VarChar;// "@image_url";
                    dbType[18] = SqlDbType.VarChar;//@qa_url;

                    #endregion

                    #region// data values & paramValue array

                    DataValidation dv = new DataValidation();
                    dv.id = proVM.id;
                    dv.name = proVM.name;
                    dv.exit_date = proVM.exit_date;
                    dv.project_cost = proVM.project_cost;
                    dv.burndown = proVM.burndown;
                    dv.producer_name = proVM.producer_name;
                    dv.market = proVM.market;
                    dv.hosting_platform = proVM.hosting_platform;
                    dv.phase_status = proVM.status;
                    dv.progress = proVM.progress;
                    dv.live_url = proVM.live_url;
                    dv.development_start_date = proVM.development_start_date;
                    dv.launch_date = proVM.launch_date;
                    dv.TotalTimeSpent = proVM.TotalTimeSpent;
                    dv.EstimatedTime = proVM.EstimatedTime;

                    dv.pending_with = proVM.pending_with;
                    dv.due_date = proVM.due_date;
                    dv.image_url = proVM.image_url;
                    dv.qa_url = proVM.qa_url;

                    string[] paramValue = new string[arrayLength];
                    paramValue[0] = dv.id.ToString();//"@Project_Id";
                    paramValue[1] = dv.EstimatedTime.ToString();// "@EstimatedTime";
                    paramValue[2] = dv.TotalTimeSpent.ToString();// "@TotalTimeSpent";
                    paramValue[3] = dv.burndown.ToString();// "@Burndown";
                    paramValue[4] = dv.development_start_date;// "@Development_Start_Date";
                    paramValue[5] = dv.exit_date;// "@Exit_Date";
                    paramValue[6] = dv.hosting_platform;// "@Hosting_Platform";
                    paramValue[7] = dv.launch_date;// "@Launch_Date";
                    paramValue[8] = dv.live_url;// "@Live_Url";
                    paramValue[9] = dv.market;// "@Market";
                    paramValue[10] = dv.name;// "@Name";
                    paramValue[11] = dv.producer_name;// "@Producer_Name";
                    paramValue[12] = dv.progress.ToString();// "@Progress";
                    paramValue[13] = dv.project_cost.ToString();// "@Project_Cost";
                    paramValue[14] = dv.phase_status;// "@Phase_Status";
                    paramValue[15] = dv.pending_with;// "@pending_with";
                    paramValue[16] = dv.due_date; // "@due_date";
                    paramValue[17] = dv.image_url; // "@image_url";
                    paramValue[18] = dv.qa_url;//

                    if (dv.id == 74)
                    {
                        string sr = string.Empty;
                    }
                    #endregion

                    string projectTableId = dc.GetSingleCell("Select Id from Project_Main where Project_Id = '" + dv.id + "'");
                    SqlParameter[] param = dc.MySqlParamCreate(arrayLength, paramNameSql, dbType, paramValue);

                    #region //sql query insert/update
                    if (string.IsNullOrWhiteSpace(projectTableId))
                    {
                        //insert into db
                        string insertQuery = @"INSERT INTO [dbo].[Project_Main] ([Project_Id], [EstimatedTime], [TotalTimeSpent]
           ,[Burndown], [Development_Start_Date] ,[Exit_Date] ,[Hosting_Platform] ,[Launch_Date] ,[Live_Url] ,[Market]
           ,[Name] ,[Producer_Name] ,[Progress] ,[Project_Cost] ,[Phase_Status] ,[pending_with] ,[due_date] ,[image_url],[qa_url] )
     VALUES
           (@Project_Id ,@EstimatedTime ,@TotalTimeSpent ,@Burndown ,@Development_Start_Date ,@Exit_Date ,@Hosting_Platform
           ,@Launch_Date ,@Live_Url ,@Market ,@Name ,@Producer_Name ,@Progress ,@Project_Cost ,@Phase_Status ,@pending_with ,@due_date ,@image_url , @qa_url)";


                        if (dc.InsertUpdateDelete(insertQuery, param))
                            return "Success";
                        else
                        {
                            Logger.Debug("SqlQueries.Project_Main_Tbl.InsertOrUpdate: Failed To Insert Query");
                            return "Fail";
                        }
                    }
                    else
                    {
                        //update into row db

                        string updateQuery = @"UPDATE [dbo].[Project_Main]
                                               SET [EstimatedTime] = @EstimatedTime
                                                  ,[TotalTimeSpent] = @TotalTimeSpent
                                                  ,[Burndown] = @Burndown
                                                  ,[Development_Start_Date] = @Development_Start_Date
                                                  ,[Exit_Date] = @Exit_Date
                                                  ,[Hosting_Platform] = @Hosting_Platform
                                                  ,[Launch_Date] = @Launch_Date
                                                  ,[Live_Url] = @Live_Url
                                                  ,[Market] = @Market
                                                  ,[Name] = @Name
                                                  ,[Producer_Name] = @Producer_Name
                                                  ,[Progress] = @Progress
                                                  ,[Project_Cost] = @Project_Cost
                                                  ,[Phase_Status] = @Phase_Status
                                                  ,[pending_with] = @pending_with
                                                  ,[due_date] = @due_date
                                                  ,[image_url] = @image_url
                                                  ,[qa_url] = @qa_url
                                               WHERE [Id] = '" + projectTableId + "'";

                        if (dc.InsertUpdateDelete(updateQuery, param))
                            return "Success";
                        else
                        {
                            Logger.Debug("SqlQueries.Project_Main_Tbl.InsertOrUpdate: Failed To Update Query");
                            return "Fail";
                        }
                    }
                    #endregion

                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.Project_Main_Tbl.InsertOrUpdate : " + ex.ToString());
                    return "Exception: " + ex.ToString();
                }
            }

            /// <summary>
            /// <para>Update Campaign table </para>
            /// <para>returns "Project VM Null" string if parameter is null </para>
            /// <para>returns "Project ID Invalid" string if id is null </para>
            ///  <para>returns "Success" or "Fail" string on update  </para>
            ///  <para>returns string which contains Exception: for uncaught exception</para>
            /// </summary>
            /// <param name="proVM">PojectVM class</param>
            /// <returns>string value regarding status of update</returns>
            public string UpdateCampaignDynamicParam(ProjectVM data)
            {
                try
                {
                    string query = @"UPDATE [dbo].[Project_Main] SET "; ;

                    List<SqlParameter> list = new List<SqlParameter>();
                    SqlParameter param;


                    if (data.id != 0)
                    {
                        //return ;//cant update project without id
                        query += @" [Project_Id] = @Project_Id ";
                        if (!string.IsNullOrEmpty(data.name))
                        {
                            query += @",[Name] = @Name";
                            param = new SqlParameter("@Name", SqlDbType.VarChar) { Value = data.name };
                            list.Add(param);
                        }
                        if (!string.IsNullOrEmpty(data.exit_date) && Convert.ToDateTime(data.exit_date) != DateTime.MinValue)
                        {
                            query += @",[Exit_Date] = @Exit_Date";
                            param = new SqlParameter("@Exit_Date", SqlDbType.Date) { Value = Convert.ToDateTime(data.exit_date).ToString("yyyy-MM-dd") };
                            list.Add(param);
                        }
                        if (data.project_cost != 0.0)
                        {
                            query += @",[Project_Cost] = @Project_Cost";
                            param = new SqlParameter("@Project_Cost", SqlDbType.Decimal) { Value = data.project_cost };
                            list.Add(param);
                        }
                        if (!string.IsNullOrEmpty(data.producer_name))
                        {
                            query += @",[Producer_Name] = @Producer_Name";
                            param = new SqlParameter("@Producer_Name", SqlDbType.VarChar) { Value = data.producer_name };
                            list.Add(param);
                        }
                        if (!string.IsNullOrEmpty(data.market))
                        {
                            query += @",[Market] = @Market";
                            param = new SqlParameter("@Market", SqlDbType.VarChar) { Value = data.market };
                            list.Add(param);
                        }
                        if (!string.IsNullOrEmpty(data.hosting_platform))
                        {
                            query += @",[Hosting_Platform] = @Hosting_Platform";
                            param = new SqlParameter("@Hosting_Platform", SqlDbType.VarChar) { Value = data.hosting_platform };
                            list.Add(param);
                        }
                        if (!string.IsNullOrEmpty(data.status))
                        {
                            query += @",[Phase_Status] = @Phase_Status";
                            param = new SqlParameter("@Phase_Status", SqlDbType.VarChar) { Value = data.status };
                            list.Add(param);
                        }
                        if (data.progress != 0.0)
                        {
                            query += @",[Progress] = @Progress";
                            param = new SqlParameter("@Progress", SqlDbType.Decimal) { Value = data.progress };
                            list.Add(param);
                        }
                        if (data.EstimatedTime != 0.0)
                        {
                            query += @",[EstimatedTime] = @EstimatedTime";
                            param = new SqlParameter("@EstimatedTime", SqlDbType.Decimal) { Value = data.EstimatedTime };
                            list.Add(param);
                        }
                        if (data.TotalTimeSpent != 0.0)
                        {
                            query += @",[TotalTimeSpent] = @TotalTimeSpent";
                            param = new SqlParameter("@TotalTimeSpent", SqlDbType.Decimal) { Value = data.TotalTimeSpent };
                            list.Add(param);
                        }
                        if (!string.IsNullOrEmpty(data.live_url))
                        {
                            query += @",[Live_Url] = @Live_Url";
                            param = new SqlParameter("@Live_Url", SqlDbType.VarChar) { Value = data.live_url };
                            list.Add(param);
                        }
                        if (data.development_start_date != null && Convert.ToDateTime(data.development_start_date) != DateTime.MinValue)
                        {
                            query += @",[Development_Start_Date] = @Development_Start_Date";
                            param = new SqlParameter("@Development_Start_Date", SqlDbType.Date) { Value = Convert.ToDateTime(data.development_start_date).ToString("yyyy-MM-dd") };
                            list.Add(param);
                        }
                        if (data.launch_date != null && Convert.ToDateTime(data.launch_date) != DateTime.MinValue)
                        {
                            query += @",[Launch_Date] = @Launch_Date";
                            param = new SqlParameter("@Launch_Date", SqlDbType.Date) { Value = Convert.ToDateTime(data.launch_date).ToString("yyyy-MM-dd") };
                            list.Add(param);
                        }
                        if (data.due_date != null && Convert.ToDateTime(data.due_date) != DateTime.MinValue)
                        {
                            query += @",[due_date] = @due_date";
                            param = new SqlParameter("@due_date", SqlDbType.Date) { Value = Convert.ToDateTime(data.due_date).ToString("yyyy-MM-dd") };
                            list.Add(param);
                        }
                        if (!string.IsNullOrEmpty(data.pending_with))
                        {
                            query += @",[pending_with] = @pending_with";
                            param = new SqlParameter("@pending_with", SqlDbType.VarChar) { Value = data.pending_with };
                            list.Add(param);
                        }
                        if (!string.IsNullOrEmpty(data.image_url))
                        {
                            query += @",[image_url] = @image_url";
                            param = new SqlParameter("@image_url", SqlDbType.VarChar) { Value = data.image_url };
                            list.Add(param);
                        }
                        if (!string.IsNullOrEmpty(data.qa_url))
                        {
                            query += @",[qa_url] = @qa_url";
                            param = new SqlParameter("@qa_url", SqlDbType.VarChar) { Value = data.qa_url };
                            list.Add(param);
                        }

                        query += @" WHERE [Project_Id] = @Project_Id";
                        param = new SqlParameter("@Project_Id", SqlDbType.BigInt) { Value = data.id };
                        list.Add(param);
                    }

                    SqlParameter[] sqlParam = list.ToArray();

                    DatabaseConnection dc = new DatabaseConnection();
                    if (dc.InsertUpdateDeleteDynamic(query, sqlParam))
                        return "Success";
                    else
                        return "Failed";


                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.Project_Main_Tbl.UpdateCampaignDynamicParam : " + ex.ToString());
                    return "Exception: " + ex.ToString();
                }
            }

            public DataTable CampaignTabs(string tabName, string redmineUserId)
            {
                try
                {
                    string query = @"SELECT distinct pm.[Project_Id] as id
                                      ,[Burndown] as burndown
	                                  ,[EstimatedTime] as EstimatedTime
                                      ,[TotalTimeSpent] as TotalTimeSpent
                                      ,[Development_Start_Date] as development_start_date
                                      ,[Exit_Date] as exit_date
                                      ,[Hosting_Platform] as hosting_platform
                                      ,[Launch_Date] as launch_date
                                      ,[Live_Url] as live_url
                                      ,[Market] as market
                                      ,[Name] as name
                                      ,[Producer_Name] as producer_name
                                      ,[Progress] as progress
                                      ,[Project_Cost] as project_cost
	                                  ,[Phase_Status] as status
                                      ,isnull((ist.Enabled),0) as SiteDown
                                      ,[pending_with]
                                      ,[due_date]
                                      ,[image_url]
                                      ,[qa_url]
                                   FROM [Project_Main]  pm  left outer join IssueStatusTracking ist on pm.Project_Id = ist.Project_id 
                                   join Memberships mem on pm.Project_Id = mem.Project_Id  join Users us on mem.RedmineUserId = us.RedmineUserId where us.RedmineUserId = '" + redmineUserId + "' and ";
                    #region // Estimate
                    if (tabName.Trim().ToUpper() == "Estimate".ToUpper())
                    {
                        query += @"  (Development_Start_Date >GETDATE()   or Development_Start_Date is null)";// and Launch_Date > GETDATE() ";
                        //query += @" where ( (GETDATE() < Development_Start_Date )
                        //and ( GETDATE()< Exit_Date ) and (GETDATE() < Launch_Date )  )or (Development_Start_Date is null and Exit_Date is null and Launch_Date is null)
                        // or Launch_Date is null or (Development_Start_Date is null and Launch_Date is null)";
                        DataTable dt = new DataTable();
                        dt = dc.GetDataTable(query);
                        return dt;

                    }
                    #endregion
                    #region // Development
                    if (tabName.Trim().ToUpper() == "Development".ToUpper())
                    {
                        query += @"  Development_Start_Date is not null and  ((Development_Start_Date <=GETDATE() and Launch_Date >  GETDATE()) or Launch_Date is null) ";

                        DataTable dt = new DataTable();
                        dt = dc.GetDataTable(query);
                        return dt;
                    }
                    #endregion
                    #region // Live
                    if (tabName.Trim().ToUpper() == "Live".ToUpper())
                    {
                        query += @" Development_Start_Date is not null and Launch_Date is not null and (  (Launch_Date <=GETDATE())and( Exit_Date >  GETDATE() or Exit_Date is null)) ";

                        DataTable dt = new DataTable();
                        dt = dc.GetDataTable(query);
                        return dt;
                    }
                    #endregion
                    #region // Archived
                    if (tabName.Trim().ToUpper() == "Archived".ToUpper())
                    {
                        query += @" Development_Start_Date is not null and Launch_Date is not null and Exit_Date is not null and GETDATE() >= Exit_Date ";

                        DataTable dt = new DataTable();
                        dt = dc.GetDataTable(query);
                        return dt;
                    }
                    #endregion
                    else
                        return null;
                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.Project_Main_Tbl.CampaignTabs : " + ex.ToString());
                    return null;
                }
            }

            public DataTable GetAllCampaigns(string redmineUserId)
            {
                try
                {
                    string query = @"SELECT distinct pm.[Project_Id] as id
                                      ,[Burndown] as burndown
	                                  ,[EstimatedTime] as EstimatedTime
                                      ,[TotalTimeSpent] as TotalTimeSpent
                                      ,[Development_Start_Date] as development_start_date
                                      ,[Exit_Date] as exit_date
                                      ,[Hosting_Platform] as hosting_platform
                                      ,[Launch_Date] as launch_date
                                      ,[Live_Url] as live_url
                                      ,[Market] as market
                                      ,[Name] as name
                                      ,[Producer_Name] as producer_name
                                      ,[Progress] as progress
                                      ,[Project_Cost] as project_cost
	                                  ,[Phase_Status] as status
                                      ,isnull((ist.Enabled),0) as SiteDown
                                      ,[pending_with]
                                      ,[due_date]
                                      ,[image_url] 
                                      ,[qa_url]
                                   FROM [Project_Main]  pm  left outer join IssueStatusTracking ist on pm.Project_Id = ist.Project_id 
                                   join Memberships mem on pm.Project_Id = mem.Project_Id  join Users us on mem.RedmineUserId = us.RedmineUserId where us.RedmineUserId = '" + redmineUserId + "'";

                    DataTable dt = new DataTable();
                    dt = dc.GetDataTable(query);

                    return dt;
                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.Project_Main_Tbl.GetAllCampaigns : " + ex.ToString());
                    return null;
                }
            }

            public DataTable PingCheck()
            {
                try
                {
                    string query = @"SELECT pm.[Project_Id] as id
                                      ,[Live_Url] as live_url
                                      ,[Name] as name
                                      FROM Project_Main pm  left outer join IssueStatusTracking ist on pm.Project_Id = ist.Project_id 
                                    where  (Launch_Date <=GETDATE())and( Exit_Date >=  GETDATE() or Exit_Date is null)";

                    DataTable dt = new DataTable();
                    dt = dc.GetDataTable(query);
                    return dt;
                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.Project_Main_Tbl.PingCheck : " + ex.ToString());
                    return null;
                }
            }

        }
        public class UsersAndMemberships_Tbl
        {
            DatabaseConnection dc = new DatabaseConnection();
            public bool InsertOrUpdateUserAndMembership(User users, List<Membership> membersList)
            {
                try
                {

                    int arrayLengthUser = 6;
                    string result = string.Empty;
                    string userIdDB = dc.GetSingleCell("Select [RedmineUserId] from [Users] where [RedmineUserId] = '" + users.id + "'");

                    #region  //paramNameSqlUser
                    string[] paramNameSqlUser = new string[arrayLengthUser];
                    paramNameSqlUser[0] = "@RedmineUserId";
                    paramNameSqlUser[1] = "@RedmineEmailId";
                    paramNameSqlUser[2] = "@Username";
                    paramNameSqlUser[3] = "@FirstName";
                    paramNameSqlUser[4] = "@LastName";
                    paramNameSqlUser[5] = "@RedmineApiKey";

                    #endregion

                    #region // sql dbtypeUser array
                    SqlDbType[] dbTypeUser = new SqlDbType[arrayLengthUser];
                    dbTypeUser[0] = SqlDbType.BigInt; //"@RedmineUserId";
                    dbTypeUser[1] = SqlDbType.VarChar; // "@RedmineEmailId";
                    dbTypeUser[2] = SqlDbType.VarChar; //"@Username";
                    dbTypeUser[3] = SqlDbType.VarChar; //"@FirstName";
                    dbTypeUser[4] = SqlDbType.VarChar; //"@LastName";
                    dbTypeUser[5] = SqlDbType.VarChar; //"@RedmineApiKey";

                    #endregion

                    #region// data values & paramValue array user
                    string[] paramValueUser = new string[arrayLengthUser];
                    paramValueUser[0] = users.id.ToString(); //"@RedmineUserId";
                    paramValueUser[1] = users.mail; // "@RedmineEmailId";
                    paramValueUser[2] = users.login; //"@Username";
                    paramValueUser[3] = users.firstname; //"@FirstName";
                    paramValueUser[4] = users.lastname; //"@LastName";
                    paramValueUser[5] = users.api_key; //"@RedmineApiKey";
                    #endregion

                    SqlParameter[] paramUser = dc.MySqlParamCreate(arrayLengthUser, paramNameSqlUser, dbTypeUser, paramValueUser);
                    #region//user Query
                    if (string.IsNullOrWhiteSpace(userIdDB))
                    {
                        string query = @" INSERT INTO [dbo].[Users] ([RedmineUserId] ,[RedmineEmailId] ,[Username]  ,[FirstName] ,[LastName] ,[RedmineApiKey]) VALUES (@RedmineUserId , @RedmineEmailId , @Username  , @FirstName , @LastName , @RedmineApiKey)";
                        if (!dc.InsertUpdateDelete(query, paramUser))
                            Logger.Debug("SqlQueries.UsersAndMemberships_Tbl.InsertOrUpdateUserAndMembership.User: Failed To Insert Query");
                    }
                    else
                    {
                        string query = @" UPDATE [dbo].[Users]
                                               SET [RedmineEmailId] = @RedmineEmailId
                                                  ,[Username] = @Username
                                                  ,[FirstName] = @FirstName
                                                  ,[LastName] = @LastName
                                                  ,[RedmineApiKey] = @RedmineApiKey
                                                  WHERE [RedmineUserId] = @RedmineUserId";
                        if (!dc.InsertUpdateDelete(query, paramUser))

                            Logger.Debug("SqlQueries.UsersAndMemberships_Tbl.InsertOrUpdateUserAndMembership.User: Failed To Insert Query");

                    }
                    #endregion
                    int arrayLengthMember = 4;

                    #region member query
                    foreach (Membership mem in membersList)
                    {
                        string membershipId = mem.id.ToString();
                        string redmineUserId = users.id.ToString();
                        string projectId = mem.project.id.ToString();
                        string projectIdCheck = dc.GetSingleCell("Select Id from [Project_Main] where Project_Id = '" + projectId + "'");
                        if (string.IsNullOrWhiteSpace(projectIdCheck))
                        {
                            continue;
                        }

                        foreach (Role rol in mem.roles)
                        {
                            string role = rol.name;
                            string membershipIdDB = dc.GetSingleCell("Select top 1 Id from [Memberships] where Id = '" + membershipId + "' and [RedmineUserId] = '" + redmineUserId + "' and [Role] = '" + role + "' and [Project_Id] = '" + projectId + "'");
                            string roleDB = dc.GetSingleCell("Select top 1 Role from [Memberships] where Id = '" + membershipId + "' and   [RedmineUserId] = '" + redmineUserId + "' and [Role] = '" + role + "' and [Project_Id] = '" + projectId + "'");

                            #region // sql dbtypeMember array
                            SqlDbType[] dbTypeMember = new SqlDbType[arrayLengthMember];
                            dbTypeMember[0] = SqlDbType.BigInt;//"@RedmineUserId";
                            dbTypeMember[1] = SqlDbType.VarChar;//"@Role";
                            dbTypeMember[2] = SqlDbType.BigInt;//"@Project_Id";
                            dbTypeMember[3] = SqlDbType.BigInt;//"@Id";
                            #endregion

                            #region  //paramNameSqlMember
                            string[] paramNameSqlMember = new string[arrayLengthMember];
                            paramNameSqlMember[0] = "@RedmineUserId";
                            paramNameSqlMember[1] = "@Role";
                            paramNameSqlMember[2] = "@Project_Id";
                            paramNameSqlMember[3] = "@Id";
                            #endregion

                            #region// data values & paramValue array member


                            string[] paramValueMember = new string[arrayLengthMember];
                            paramValueMember[0] = redmineUserId;//"@Project_Id";
                            paramValueMember[1] = role;// "@EstimatedTime";
                            paramValueMember[2] = projectId;// "@TotalTimeSpent";
                            paramValueMember[3] = string.IsNullOrWhiteSpace(membershipIdDB) ? membershipId : membershipIdDB;//"@membershipId";
                            #endregion

                            SqlParameter[] paramMember = dc.MySqlParamCreate(arrayLengthMember, paramNameSqlMember, dbTypeMember, paramValueMember);


                            if (string.IsNullOrWhiteSpace(membershipIdDB) && string.IsNullOrWhiteSpace(roleDB))
                            {
                                //insert into db
                                string query = @"INSERT INTO [dbo].[Memberships] ([Id],[RedmineUserId], [Role], [Project_Id] )
                                                VALUES (@Id , @RedmineUserId ,@Role ,@Project_Id )";
                                if (!dc.InsertUpdateDelete(query, paramMember))
                                    Logger.Debug("SqlQueries.UsersAndMemberships_Tbl.InsertOrUpdateUserAndMembership.InsertMember: Failed To Insert Query");
                            }
                            else
                            {
                                //update into row db

                                //                                string query = @"UPDATE [dbo].[Memberships]
                                //                                               SET [RedmineUserId] = @RedmineUserId
                                //                                                  ,[Role] = @Role
                                //                                                  ,[Project_Id] = @Project_Id
                                //                                                  WHERE [Id] = @Id";

                                //                                if (!dc.InsertUpdateDelete(query, paramMember))
                                //                                    Logger.Debug("SqlQueries.UsersAndMemberships_Tbl.InsertOrUpdateUserAndMembership.updateMember: Failed To Update Query");
                            }
                        }

                    }
                    #endregion

                    return true;
                }



                catch (Exception ex)
                {
                    return false;
                    Logger.Error("SqlQueries.UsersAndMemberships_Tbl.InsertOrUpdateUserAndMembership Exception : " + ex.ToString());
                }
            }

        }
        public class EmailNotification
        {
            DatabaseConnection dc = new DatabaseConnection();
            string daysBeforeReminderMailTrigger = ConfigurationManager.AppSettings["daysBeforeReminderMailTrigger"].Trim();
            #region //Daily scheduled mail
            /// <summary>
            /// <para>Returns ProjectID for following date type</para>
            /// <para>Date Type =  due_date , Launch_Date , Exit_Date</para>
            /// </summary>
            /// <param name="DateType">due_date , Launch_Date , Exit_Date</param>
            /// <returns></returns>
            public DataTable Reminder(string DateType)
            {
                try
                {
                    int noOfDays = 0;
                    int.TryParse(daysBeforeReminderMailTrigger, out noOfDays);
                    string query = @"SELECT distinct [Project_Id] , [Name]
                                   FROM [Project_Main] 
                                    where [" + DateType + "] = '" + DateTime.Now.AddDays(noOfDays).ToString("yyyy-MM-dd") + "' and [" + DateType + "] is not null";

                    DataTable dt = new DataTable();
                    dt = dc.GetDataTable(query);

                    return dt;
                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.EmailNotification.Reminder : " + ex.ToString());
                    return null;
                }
            }

            /// <summary>
            /// <para>Returns ProjectID for following date type</para>
            /// <para>Date Type =  due_date , Launch_Date , Exit_Date</para>
            /// </summary>
            /// <param name="DateType">due_date , Launch_Date , Exit_Date</param>
            /// <returns></returns>
            public DataTable MoveTabs(string DateType)
            {
                try
                {
                    string query = @"SELECT distinct [Project_Id]  , [Name]
                                   FROM [Project_Main]  
                                   where [" + DateType + "] = '" + DateTime.Now.ToString("yyyy-MM-dd") + "' and [" + DateType + "] is not null";

                    DataTable dt = new DataTable();
                    dt = dc.GetDataTable(query);

                    return dt;
                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.EmailNotification.MoveTabs : " + ex.ToString());
                    return null;
                }
            }

            /// <summary>
            /// Get Emails of users
            /// </summary>
            /// <param name="DateType">due_date , Launch_Date , Exit_Date</param>
            /// <returns></returns>
            public DataTable GetUserEmailIds(string projectId)
            {
                try
                {
                    string query = @"Select RedmineEmailId from Users join Memberships on Users.RedmineUserId = Memberships.RedmineUserId
                                    where Project_Id = " + projectId;

                    DataTable dt = new DataTable();
                    dt = dc.GetDataTable(query);

                    return dt;
                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.EmailNotification.GetUserEmailIds : " + ex.ToString());
                    return null;
                }
            }

            #endregion

            public DataTable GetMailQueue()
            {
                try
                {
                    string query = @"Select Id, MailBody , MailSubject , MailTo from MailQueue where MaxRetry >=RetryCount and QEnabled = 'true'";

                    DataTable dt = new DataTable();
                    dt = dc.GetDataTable(query);

                    return dt;
                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.EmailNotification.GetMailQueue : " + ex.ToString());
                    return null;
                }
            }
            public bool CreateMailQueTableIfNotExists()
            {
                try
                {
                    string query = @"if not exists(SELECT * FROM sys.tables WHERE  type = 'U' and name = N'MailQueue' )
                                        begin
	                                        Create table MailQueue 
	                                        (
	                                          Id bigint primary key identity(1,1) 
	                                        , MailBody varchar(max) 
	                                        , MailSubject varchar(1000) 
	                                        , MailTo varchar(500)
	                                        , RetryCount int
	                                        , MaxRetry int default(3)
	                                        , LastMod datetime default (GETDATE())
	                                        , QEnabled bit default(1)
	                                        )
                                        end ";
                    if (dc.InsertUpdateDelete(query))
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.EmailNotification.CreateMailQueTableIfNotExists : " + ex.ToString());
                    return false;
                }

            }
            public bool InsertMailQueue(string body, string subject, string mailTo)
            {
                try
                {
                    string query = @"if not exists(SELECT * FROM sys.tables WHERE  type = 'U' and name = N'MailQueue' )
                                        begin
	                                        Create table MailQueue 
	                                        (
	                                          Id bigint primary key identity(1,1) 
	                                        , MailBody varchar(max) 
	                                        , MailSubject varchar(1000) 
	                                        , MailTo varchar(500)
	                                        , RetryCount int
	                                        , MaxRetry int default(3)
	                                        , LastMod datetime default (GETDATE())
	                                        , QEnabled bit default(1)
	                                        )
                                        end 
                                        Insert into MailQueue (MailBody , MailSubject , MailTo , RetryCount) values(@MailBody , @MailSubject , @MailTo , @RetryCount )    "
                                        ;

                    SqlParameter[] param = new SqlParameter[4];
                    param[0] = new SqlParameter("@MailBody", SqlDbType.VarChar) { Value = body };
                    param[1] = new SqlParameter("@MailSubject", SqlDbType.VarChar) { Value = subject };
                    param[2] = new SqlParameter("@MailTo", SqlDbType.VarChar) { Value = mailTo };
                    param[3] = new SqlParameter("@RetryCount", SqlDbType.VarChar) { Value = 1 };

                    if (dc.InsertUpdateDelete(query, param))
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.EmailNotification.InsertMailQueue : " + ex.ToString());
                    return false;
                }
            }
            public bool IncreaseRetryCount(string id)
            {
                try
                {
                    string query = @"Update MailQueue set RetryCount = (Select RetryCount from MailQueue where Id = " + id + ")+1  , LastMod  = GETDATE() where Id = " + id;
                    if (dc.InsertUpdateDelete(query))
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.EmailNotification.IncreaseRetryCount : " + ex.ToString());
                    return false;
                }
            }
            public bool DisableMailQueue(string id)
            {
                try
                {
                    string query = @"Update MailQueue set QEnabled = 0  , LastMod  = GETDATE() where Id = " + id;
                    if (dc.InsertUpdateDelete(query))
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    Logger.Error("SqlQueries.EmailNotification.DisableMailQueue : " + ex.ToString());
                    return false;
                }
            }
        }

    }
}