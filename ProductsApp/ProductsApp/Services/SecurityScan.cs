﻿using ProductsApp.Models;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace ProductsApp.Services
{
    public class SecurityScan
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public SecurityScan() { }

        /// <summary>
        /// Create new scan request
        /// </summary>
        /// <returns></returns>
        public string CreateNewScanRequest(string projectId, string projectName, string url)
        {
            string responseIssueId = string.Empty;
            try
            {
                string tracekrIdZAP = ConfigurationManager.AppSettings["tracker_secuirtyscan:zap"];

                string tracekrIdSSL = ConfigurationManager.AppSettings["tracker_secuirtyscan:ssl"];

                Issue issueZAP = new Issue();
                issueZAP.subject = "ZAP Security Scan for Project:" + projectName;
                issueZAP.status_id = 1;//1 = new
                issueZAP.project_id = 65;//project id of deployment
                issueZAP.description = "ZAP Scan Created at:  " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                responseIssueId = PostData.PostMethod(issueZAP, "2", tracekrIdZAP) + ",";

                Issue issueSSL = new Issue();
                issueSSL.subject = "SSL Security Scan for Project:" + projectName;
                issueSSL.status_id = 1;//1 = new
                issueSSL.project_id = 65;//project id of deployment
                issueSSL.description = "SSL Scan Created at:  " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                responseIssueId += PostData.PostMethod(issueSSL, "2", tracekrIdSSL);

                return responseIssueId;
            }
            catch (Exception ex)
            {
                Logger.Error("SecurityScan.CreateNewScanRequest" + ex.ToString());
                return "Excepton: " + ex.ToString();
            }
        }

        /// <summary>
        /// <para>Start Scan on Regular Basis</para>
        /// <para>scanType String "ZAP" or "SSL"</para>
        /// </summary>
        /// <param name="scanType">"ZAP" or "SSL"</param>
        /// <returns>returns scan status</returns>
        public static string StartPeriodicScan(string scanType)
        {
            string tracekrIdZAP = ConfigurationManager.AppSettings["tracker_secuirtyscan:zap"];
            string tracekrIdSSL = ConfigurationManager.AppSettings["tracker_secuirtyscan:ssl"];
            string returnString = string.Empty;
            if (scanType.ToUpper() == "ZAP")
            {
                returnString = StartSecurityScanTracker(tracekrIdZAP, "ZAP");
            }
            if (scanType.ToUpper() == "SSL")
            {
                returnString = StartSecurityScanTracker(tracekrIdSSL, "SSL");
            }
            return returnString;
        }

        private static string StartSecurityScanTracker(string TrackerId, string ScanType)
        {
            try
            {

                // tracker 17 secuirtyscan:zap & status_id=8 In Development/ In Process
                string returnString = string.Empty;
                string trackerId = TrackerId;
                string statusIdInProgress = "8";
                string statusIdNew = "1";
                string key = ConfigurationManager.AppSettings["apiKey"];

                string url = ConfigurationManager.AppSettings["hostUrl"] + "projects/deployment-management/issues.json?limit=1&tracker_id=" + trackerId + "&status_id=" + statusIdInProgress + "&key=" + key;
                WebRequest request = WebRequest.Create(url);

                //NetworkCredential cre = new NetworkCredential("legouser", "lego123@1");
                //request.Credentials = cre;

                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    string str = reader.ReadToEnd();
                    RootObject root = (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);

                    if (root.issues.Count == 0)
                    {
                        url = ConfigurationManager.AppSettings["hostUrl"] + "projects/deployment-management/issues.json?limit=1&tracker_id=" + trackerId + "&status_id=" + statusIdNew + "&key=" + key;
                        WebRequest requestNew = WebRequest.Create(url);
                        WebResponse responseNew = requestNew.GetResponse();
                        using (Stream responseStreamNew = responseNew.GetResponseStream())
                        {
                            StreamReader readerNew = new StreamReader(responseStreamNew, Encoding.UTF8);
                            string strNew = readerNew.ReadToEnd();
                            RootObject rootNew = (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(strNew);


                            if (ScanType == "ZAP")
                            {
                                //Code for running powershell goes here 

                                if (RunPowerShellZAP())
                                {
                                    //Mark issue status to indevelopment
                                    PostData.UpdateIssue(rootNew.issues[0].id.ToString(), new string[] { statusIdInProgress }, new string[] { "status_id" });

                                    returnString = "ZAP Scan Started";
                                }
                            }

                            if (ScanType == "SSL")
                            {
                                //Code for running powershell goes here 

                                if (RunPowerShellSSL())
                                {
                                    //Mark issue status to indevelopment
                                    PostData.UpdateIssue(rootNew.issues[0].id.ToString(), new string[] { statusIdInProgress }, new string[] { "status_id" });
                                }
                                returnString = "SSL Scan Started";
                            }

                        }
                    }
                    else
                        returnString = "InProgress";

                }
                return returnString;
            }
            catch (Exception ex)
            {
                Logger.Error("SecurityScan.StartSecurityScanTracker" + ex.ToString());
                return "Exception: " + ex.ToString();
            }
        }

        private static bool RunPowerShellZAP()
        {
            string PowerShellScriptPath = ConfigurationManager.AppSettings["ZAPScriptPath"];
            if (RunPsScript(PowerShellScriptPath))
                return true;
            else
                return false;
        }

        private static bool RunPowerShellSSL()
        {
            string PowerShellScriptPath = ConfigurationManager.AppSettings["SSLScriptPath"];
            if (RunPsScript(PowerShellScriptPath))
                return true;
            else
                return false;
        }

        private static bool RunPsScript(string psScriptPath)
        {
            try
            {

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = @"powershell.exe";
                startInfo.Arguments = String.Format("-executionpolicy unrestricted -noexit -file \"{0}\" {1}", psScriptPath, "hcl.com");
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;
                Process process = new Process();
                process.StartInfo = startInfo;
                process.Start();



                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("SecurityScan.RunPsScript" + ex.ToString());
                Console.WriteLine(ex.ToString());
                return false;
            }
        }


    }
}