﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ProductsApp.Services
{
    public class MongoDBService
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static string dbName = "CampaignTracker";

        /// <summary>
        /// Connection string to the Mongo database server
        /// </summary>
        public static string ConnectionString
        {
            get
            {
                //return "";
                return ConfigurationManager.AppSettings["mongoDbConnectionString"];
            }
        }

        private static IMongoDatabase Connection()
        {
            try
            {
                MongoClientSettings settings = new MongoClientSettings();
                settings.WaitQueueSize = int.MaxValue;
                settings.WaitQueueTimeout = new TimeSpan(0, 2, 0);
                settings.MinConnectionPoolSize = 1;
                settings.MaxConnectionPoolSize = 25;
                settings.Server = new MongoServerAddress(ConnectionString);
                MongoClient client = new MongoClient(settings);
                IMongoDatabase myCompany = client.GetDatabase(dbName);
                return myCompany;
            }
            catch (Exception ex)
            {
                Logger.Error("MongoDBService.IMongoDatabase: " + ex.ToString());
                return null;

            }
        }

        /// <summary>
        /// Delete all data in  collectionName in  database
        /// </summary>
        async public static void Delete(string collectionName, BsonDocument filters)
        {
            try
            {
                IMongoDatabase myDb = Connection();
                IMongoCollection<BsonDocument> collections = myDb.GetCollection<BsonDocument>(collectionName);
                if (filters != null)
                    await collections.DeleteManyAsync(filters);
                else
                    await collections.DeleteManyAsync(_ => true);
            }
            catch (Exception ex)
            {
                Logger.Error("MongoDBService.Delete: " + ex.ToString());
            }
        }

        /// <summary>
        /// InsertManyAsync in the Collection
        /// </summary>
        /// <param name="departmentName"></param>
        /// <param name="headOfDepartmentId"></param>
        async private static void InsertManyAsync(string collectionName, List<BsonDocument> bsonDocuments)
        {
            IMongoDatabase myDB = Connection();
            IMongoCollection<BsonDocument> collections = myDB.GetCollection<BsonDocument>(collectionName);
            List<BsonDocument> documentList = bsonDocuments;
            await collections.InsertManyAsync(documentList);

        }

        /// <summary>
        /// InsertOneAsync in the Collection
        /// </summary>
        /// <param name="departmentName"></param>
        /// <param name="headOfDepartmentId"></param>
        async private static void InsertOneAsync(string collectionName, List<BsonElement> jsonElements)
        {
            IMongoDatabase myDB = Connection();
            IMongoCollection<BsonDocument> collections = myDB.GetCollection<BsonDocument>(collectionName);
            BsonDocument document = new BsonDocument();
            List<BsonElement> elements = jsonElements;
            document.AddRange(elements);
            await collections.InsertOneAsync(document);
        }

        /// <summary>
        /// InsertManyAsync in the Collection
        /// </summary>
        /// <param name="departmentName"></param>
        /// <param name="headOfDepartmentId"></param>
        async private static void InsertManyAsync(string collectionName, UpdateDefinition<BsonDocument> bsonDocuments, BsonDocument filters)
        {
            IMongoDatabase myDB = Connection();
            IMongoCollection<BsonDocument> collections = myDB.GetCollection<BsonDocument>(collectionName);
            UpdateDefinition<BsonDocument> documentList = bsonDocuments;
            await collections.UpdateManyAsync(filters , documentList);

        }


        /// <summary>
        /// UpdateOneAsync in the Collection
        /// </summary>
        /// <param name="departmentName"></param>
        /// <param name="headOfDepartmentId"></param>
        async private static void UpdateOneAsync(string collectionName, List<BsonElement> jsonElements, BsonDocument filters)
        {
            IMongoDatabase myDB = Connection();
            IMongoCollection<BsonDocument> collections = myDB.GetCollection<BsonDocument>(collectionName);
            BsonDocument document = new BsonDocument();
            List<BsonElement> elements = jsonElements;
            document.AddRange(elements);
            await collections.UpdateOneAsync(filters, document);
        }


        /// <summary>
        /// Retrieve Collection Table from database.
        /// </summary>
        /// <returns></returns>
        async public static Task<DataTable> GetCollectionData(string collectionName, BsonDocument filters)
        {
            List<BsonDocument> lst = new List<BsonDocument>();
            IMongoDatabase myDB = Connection();
            IMongoCollection<BsonDocument> departments = myDB.GetCollection<BsonDocument>(collectionName);
            IFindFluent<BsonDocument, BsonDocument> result = null;

            if (filters != null)
                result = departments.Find(filters);
            else
                result = departments.Find(_ => true);
            var ls = await result.ToListAsync<BsonDocument>();
            DataTable dt = new DataTable(); // Create empty datatable we will fill with data.
            List<BsonDocument> results = ls; // now it's here.
            foreach (BsonDocument obj in results) // Loop thru all Bson documents returned from the query.
            {
                DataRow dr = dt.NewRow(); // Add new row to datatable.
                ExecuteFillDataTable(obj, dt, dr, string.Empty); // Recursive method to loop thru all results json.
                dt.Rows.Add(dr); // Add the newly created datarow to the table
            }
            return dt;
        }

        private static void ExecuteFillDataTable(BsonDocument doc, DataTable dt, DataRow dr, string parent)
        {
            // arrays means 1:M relation to parent, meaning we will have to fake multi levels by adding 1 more row foreach item in array.
            // i created the here because i want to add all new array rows after our main row.
            List<KeyValuePair<string, BsonArray>> arrays = new List<KeyValuePair<string, BsonArray>>();

            foreach (string key in doc.Names) // this will loop thru all our json attributes.
            {
                object value = doc[key]; // get the value of the current json attribute.

                string x; // for my specific needs, i need all values to be save in datatable as strings. you can implument to match your needs.

                // if our attribute is BsonDocument, means relation is 1:1. we can add values to current datarow and call the data column "parent.current".
                // we will use this recursive method to run thru all the child document.
                if (value is BsonDocument)
                {
                    string newParent = string.IsNullOrEmpty(parent) ? key : parent + "." + key;
                    ExecuteFillDataTable((BsonDocument)value, dt, dr, newParent);
                }
                // if our attribute is BsonArray, means relation is 1:N. we will need to add new rows, but not now.
                // we will save it in queue for later use.
                else if (value is BsonArray)
                {
                    // Save array to queue for later loop.
                    arrays.Add(new KeyValuePair<string, BsonArray>(key, (BsonArray)value));


                }
                // if our attribute is datatime i needed it in a spesific string format.
                else if (value is BsonTimestamp)
                {
                    x = doc[key].AsBsonTimestamp.ToLocalTime().ToString("s");

                }
                // if our attribute is null, i needed it converted to string.empty.
                else if (value is BsonNull)
                {
                    x = string.Empty;

                }
                else
                {
                    // for all other cases, just .ToString() it.
                    x = value.ToString();

                    // Make sure our datatable already contains column with the right name. if not - add it.
                    string colName = string.IsNullOrEmpty(parent) ? key : parent + "." + key;
                    if (!dt.Columns.Contains(colName))
                        dt.Columns.Add(colName);

                    // Add the value to the datarow in the right column.
                    dr[colName] = value;

                }

            }

            // loop thru all arrays when finish with standart fields.
            foreach (KeyValuePair<string, BsonArray> array in arrays)
            {
                // create column name that contains the parent name + child name.
                string newParent = string.IsNullOrEmpty(parent) ? array.Key : parent + "." + array.Key;

                // save the old - we will need it so we can add it existing values to the new row.
                DataRow drOld = dr;

                // loop thru all the BsonDocuments in the array
                foreach (BsonDocument doc2 in array.Value)
                {
                    // Create new datarow for each item in array.
                    dr = dt.NewRow();
                    dr.ItemArray = drOld.ItemArray; // this will copy all the main row values to the new row - might not be needed for your use.
                    dt.Rows.Add(dr); // the the new row to the datatable
                    ExecuteFillDataTable(doc2, dt, dr, newParent); // fill the new datarow withh all the values for the BsonDocument in the array.
                }

                dr = drOld; // set the main data row back so we can use it values again.
            }
        }
    }
}