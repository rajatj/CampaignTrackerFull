﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace ProductsApp.Services
{
    public static class Encryption
    {
        #region Encryption

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static string AES256Key = ConfigurationManager.AppSettings["AES256Key"].ToString();
        private static string AES256IV = ConfigurationManager.AppSettings["AES256IV"].ToString();

        /// <summary>
        /// Encrypt
        /// </summary>
        /// 
        public static string Encrypt(string plainText)
        {
            try
            {
                if (String.IsNullOrEmpty(plainText))
                {
                    return "";
                }
                else
                {
                    byte[] Key = GetMD5Hash(AES256Key);
                    byte[] iv = GetMD5Hash(AES256IV);
                    // Encrypt the string to an array of bytes. 
                    byte[] encrypted = EncryptStringToBytes(plainText, Key, iv);

                    //return encrypted; 
                    return Convert.ToBase64String(encrypted);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("Encrypt", ex);
                return "";
            }

        }


        public static string Decrypt(string encryptedvalue)
        {
            try
            {
                if (String.IsNullOrEmpty(encryptedvalue))
                {
                    return "";
                }
                else
                {
                    byte[] encrypted = Convert.FromBase64String(encryptedvalue);

                    byte[] Key = GetMD5Hash(AES256Key);
                    byte[] iv = GetMD5Hash(AES256IV);
                    // Decrypt the bytes to a string. 
                    string DecryptValues = DecryptStringFromBytes(encrypted, Key, iv);

                    return DecryptValues;
                }
            }
            catch (Exception ex)
            {
                logger.Trace("Decrypt", ex);
                return "";
            }
        }

        public static string RandomStringGenerator()
        {
            Guid g = Guid.NewGuid();
            string guidString = Convert.ToBase64String(g.ToByteArray());
            guidString = guidString.Replace("=", "").Replace("+", "");

            return guidString;
        }

        static byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;
            // Create an Rijndael object 
            // with the specified key and IV. 
            using (Rijndael rijAlg = Rijndael.Create())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream. 
            return encrypted;

        }

        static string DecryptStringFromBytes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the string used to hold 
            // the decrypted text. 
            string plaintext = null;

            // Create an Rijndael object 
            // with the specified key and IV. 
            using (Rijndael rijAlg = Rijndael.Create())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for decryption. 
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream 
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }

        static byte[] GetMD5Hash(string data)
        {
            MD5 md5 = MD5CryptoServiceProvider.Create();
            return md5.ComputeHash(Encoding.UTF8.GetBytes(data));
        }

        #endregion
    }
}