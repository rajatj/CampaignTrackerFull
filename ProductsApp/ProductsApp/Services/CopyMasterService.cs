﻿using ProductsApp.Models;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace ProductsApp.Services
{
    public class CopyMasterService
    {
        public List<Issue> GetIssuesFromMaster()
        {
            RootObject root = new RootObject();
            List<Issue> IssueVM = new List<Issue>();
            List<Issue> issues_list = new List<Issue>();
            string str = "";
            string url = "http://10.97.85.87/redmine/issues.json?project_id=139";
            //string url_copy = ConfigurationManager.AppSettings["hostUrl"] + "issues.json?project_id=139&limit=100&key=" ConfigurationManager.AppSettings["apiKey"];
            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            using (Stream responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                str = reader.ReadToEnd();

                root = (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);
                issues_list = root.issues;

            }

            return issues_list;


        }
    }
}