﻿using ProductsApp.Models;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace ProductsApp.Services
{
    public class CheckSiteStatus
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        static DatabaseConnection dc;
        public CheckSiteStatus()
        { }

        public static string StartAsyncHttpResponse()
        {
            try
            {
                SqlQueries.Project_Main_Tbl selectDevTableObj = new SqlQueries.Project_Main_Tbl();
                DataTable dt = new DataTable();
                dt = selectDevTableObj.PingCheck();

                string[] urls = { "" };
                string[] projectIdToIgnoreFromPing = ConfigurationManager.AppSettings["projectsToIgnorePingCheck"].Replace(" ", "").Split(',');
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {

                        string returnStatus = string.Empty;
                        List<string> urlList = new List<string>();
                        List<string> projectNameList = new List<string>();
                        List<string> projectIdList = new List<string>();
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //Uri url = null;
                            string urlData = dt.Rows[i]["live_url"].ToString();
                            string projectName = dt.Rows[i]["name"].ToString();
                            string projectId = dt.Rows[i]["id"].ToString();

                            if (!string.IsNullOrEmpty(urlData) && !projectIdToIgnoreFromPing.Contains(projectId.Trim()))
                            {
                                urlList.Add(urlData);
                                projectNameList.Add(projectName);
                                projectIdList.Add(projectId);
                            }


                        }
                        Task[] taskList = new Task[urlList.Count];
                        for (int i = 0; i < urlList.Count; i++)
                        {
                            var index = i;
                            //GetAsync(urlList[index], projectNameList[index], projectIdList[index]);
                            taskList[i] = Task.Factory.StartNew(() => (GetAsync(urlList[index], projectNameList[index], projectIdList[index])));
                        }

                        try
                        {
                            Task.WaitAll(taskList);

                            returnStatus = "Completed";
                        }
                        catch (AggregateException ae)
                        {
                            ErrorLogging.WriteErrorLog(ae);
                            returnStatus = ae.ToString();
                        }
                        catch (Exception ex)
                        {
                            ErrorLogging.WriteErrorLog(ex);
                            returnStatus = ex.ToString();
                        }
                        return returnStatus;

                    }
                    else
                        //return "Completed";
                        return "No Live Campaigns";
                }
                else
                    return "No Live Campaigns";
            }
            catch (Exception exe)
            {
                ErrorLogging.WriteErrorLog(exe);
                return exe.ToString();

            }

        }

        private static Task<string> GetAsync(string url, string projectName, string projectId)
        {
            var tcs = new TaskCompletionSource<string>();
            try
            {
                Uri uri;
                try
                {
                    uri = new Uri(url);
                }
                catch (Exception ex)
                {
                    CheckStatus(400, projectName, url, projectId);
                    ErrorLogging.WriteErrorLog("Uri format Incorrect: " + ex.ToString() + " Url:" + url + " ProjectName:" + projectName + " ProjectId:" + projectId);
                    return tcs.Task;
                }

                var request = (HttpWebRequest)WebRequest.Create(uri);

                string proxyAddress = ConfigurationManager.AppSettings["proxyAddress"];
                string username = ConfigurationManager.AppSettings["username"];
                string password = ConfigurationManager.AppSettings["password"];
                int timeOut = 0;
                int.TryParse(ConfigurationManager.AppSettings["proxyTimeOut"], out timeOut);
                if ((!string.IsNullOrWhiteSpace(proxyAddress)) && (!string.IsNullOrWhiteSpace(username)) && (!string.IsNullOrWhiteSpace(password)))
                {
                    WebProxy proxy = new WebProxy();
                    proxy.Address = new Uri(proxyAddress);
                    proxy.Credentials = new NetworkCredential(username, password);
                    request.Proxy = proxy;
                }
                if (timeOut > 0)
                    request.Timeout = timeOut;
                else
                    request.Timeout = 15000;

                try
                {
                    #region non async approach of http calls
                    //HttpWebResponse response = null;
                    //try
                    //{
                    //    response = (HttpWebResponse)request.GetResponse();
                    //    //tcs.SetResult((Convert.ToInt32(response.StatusCode).ToString() + "," + request.RequestUri.ToString()));
                    //    CheckStatus(Convert.ToInt32(response.StatusCode), projectName, request.RequestUri.ToString(), projectId);
                    //}
                    //catch (Exception exc)
                    //{
                    //    ErrorLogging.WriteErrorLog(exc);
                    //    CheckStatus(Convert.ToInt32(HttpStatusCode.BadRequest), projectName, request.RequestUri.ToString(), projectId);
                    //    tcs.SetException(exc);
                    //}
                    //finally
                    //{
                    //    if (response != null) response.Close();
                    //}
                    #endregion
                    request.BeginGetResponse(iar =>
                    {
                        HttpWebResponse response = null;
                        try
                        {
                            response = (HttpWebResponse)request.EndGetResponse(iar);
                            string status = string.Empty;
                            status = CheckStatus(Convert.ToInt32(response.StatusCode), projectName, request.RequestUri.ToString(), projectId);

                        }
                        //catch (WebException we)
                        //{
                        //    int retryCount = 0;
                        //retryOnBadGateway:
                        //    try
                        //    {
                        //        retryCount++;
                        //        request.Timeout = 10000 * retryCount;
                        //        response = (HttpWebResponse)request.EndGetResponse(iar);
                        //        string status = string.Empty;
                        //        status = CheckStatus(Convert.ToInt32(response.StatusCode), projectName, request.RequestUri.ToString(), projectId);

                        //    }
                        //    catch (WebException exex)
                        //    {
                        //        Logger.Debug("CheckSiteStatus.StartAsyncHttpResponse : Retrying on Web Exception: " + exex.Message);
                        //        if (retryCount < 5)
                        //            goto retryOnBadGateway;
                        //    }
                        //}
                        catch (Exception exc)
                        {
                            ErrorLogging.WriteErrorLog("Exception GetAsync Thread:" + exc.ToString() + " Url:" + url + " ProjectName:" + projectName + " ProjectId:" + projectId);
                            CheckStatus(Convert.ToInt32(HttpStatusCode.BadRequest), projectName, request.RequestUri.ToString(), projectId);
                            tcs.SetException(exc);
                        }
                        finally
                        {
                            if (response != null) response.Close();
                        }
                    }, null);
                }
                catch (Exception exc)
                {
                    ErrorLogging.WriteErrorLog("Exception GetAsync Inner:" + exc.ToString() + " Url:" + url + " ProjectName:" + projectName + " ProjectId:" + projectId);
                    tcs.SetException(exc);
                }
            }
            catch (Exception e)
            {
                ErrorLogging.WriteErrorLog("Exception GetAsync Outer:" + e.ToString() + " Url:" + url + " ProjectName:" + projectName + " ProjectId:" + projectId);
                tcs.SetException(e);
            }
            return tcs.Task;
        }

        private static string CheckStatus(int responseCode, string projectName, string liveUrl, string projectId)
        {
            try
            {

                dc = new DatabaseConnection();
                string checkIssueIdQuery = "Select Isssue_id , Enabled, Project_id  from IssueStatusTracking where Project_id='" + projectId + "'";

                DataTable dt = new DataTable();
                dt = dc.GetDataTable(checkIssueIdQuery);

                int issue = 0;
                bool flag = false;
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        int.TryParse(dt.Rows[0]["Isssue_id"].ToString(), out issue);//IssueTrack[0] = issue id
                        bool.TryParse(dt.Rows[0]["Enabled"].ToString(), out flag);
                    }
                }

                if (responseCode != 200)//check ping status 
                {

                    if (issue <= 0)
                    {
                        //insert defect
                        //post call in redmine
                        string issueId = "";
                        issueId = CreateIssue(projectName, liveUrl, 1);
                        int createdIssueId = 0;
                        int.TryParse(issueId, out createdIssueId);

                        if (createdIssueId > 0)
                        {
                            string insertQuery = "Insert into IssueStatusTracking (Isssue_id, Enabled, Project_id) values ('" + issueId + "','" + true + "','" + projectId + "')";
                            dc.InsertUpdateDelete(insertQuery);
                            string watchersConfig = System.Configuration.ConfigurationManager.AppSettings["watchers"];
                            string[] watcherArray = watchersConfig.Split(',');
                            PostData.AddWatchers(issueId, watcherArray);

                            string[] data = { "Website ping failed: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss") + " Url: " + liveUrl };
                            string[] element = { "notes" };
                            PostData.UpdateIssue(createdIssueId.ToString(), data, element);

                        }

                    }
                    else if (issue > 0 && flag == true)
                    {
                        //update notes in redmine for previous issue
                        string[] data = { "Website ping continues to fail: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss") + " Url: " + liveUrl };
                        string[] element = { "notes" };
                        PostData.UpdateIssue(issue.ToString(), data, element);
                    }
                    else
                    {
                        string updateQuery = "Update IssueStatusTracking set Enabled='True' where Project_id='" + projectId + "'";
                        dc.InsertUpdateDelete(updateQuery);
                        string[] data = { "Website ping continues to fail: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss") + " Url: " + liveUrl };
                        string[] element = { "notes" };
                        PostData.UpdateIssue(issue.ToString(), data, element);
                    }


                }
                else if (responseCode == 200 && flag == true)
                {


                    //close issue in redmine
                    string[] data = { "5" };//status id for closed issue
                    string[] element = { "status_id" };
                    PostData.UpdateIssue(issue.ToString(), data, element);

                    //update issue in database
                    string updateQuery = "Update IssueStatusTracking set Enabled='False' where Project_id='" + projectId + "'";
                    dc.InsertUpdateDelete(updateQuery);

                }
                else if (responseCode == 200 && issue > 0 && flag == false)
                {
                    //delete issueId in sql
                    string deleteIssueQuery = "Delete from IssueStatusTracking where Isssue_id='" + issue + "' and Enabled='false'";
                    dc.InsertUpdateDelete(deleteIssueQuery);

                }


                return "Completed";
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteErrorLog("Exception CheckStatus: " + ex.ToString());
                return "Failed";
            }

        }

        private static string CreateIssue(string name, string url, int status)
        {
            try
            {
                Issue iv = new Issue();
                iv.subject = "Site Down for Project: " + name;
                Tracker t = new Tracker();
                t.id = 15;//15 for incident request
                t.name = "Incident Request";
                iv.tracker = t;//tracker for incident request
                iv.status_id = status;//1 = new , 5 = closed
                iv.project_id = 65;//project id of deployment
                iv.priority_id = 3;//High Priority
                iv.description = "Unable to ping website :" + url + ".  Website down at: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                string responseString = PostData.PostMethod(iv, "3", "15");
                return responseString;
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteErrorLog(ex);
                return string.Empty;
            }
        }
    }
    //class CheckStatusCode
    //{
    //    public static void StatusCheck(out List<string> returnUrl, out List<string> status)
    //    {
    //        SqlQueries.Project_Main_Tbl selectDevTableObj = new SqlQueries.Project_Main_Tbl();
    //        DataTable dt = new DataTable();
    //        dt = selectDevTableObj.CampaignTabs("Live");
    //        List<string> urls = new List<string>();
    //        returnUrl = null;
    //        status = null;

    //        List<string> urlList = new List<string>();
    //        List<string> statusList = new List<string>();

    //        if (dt != null)
    //        {
    //            if (dt.Rows.Count > 0)
    //            {
    //                for (int i = 0; i < dt.Rows.Count; i++)
    //                {
    //                    //Uri url = null;
    //                    string urlData = dt.Rows[i]["live_url"].ToString();
    //                    if (!string.IsNullOrEmpty(urlData))
    //                    {
    //                        //Uri.TryCreate(urlData, UriKind.RelativeOrAbsolute, out url);
    //                        urls.Add(urlData);
    //                    }
    //                }
    //            }
    //        }

    //        var tasks = urls.Select(MulitpleHttp.GetAsync).ToArray();
    //        try
    //        {
    //            var completed = Task.Factory.ContinueWhenAll(tasks,
    //                                completedTasks =>
    //                                {

    //                                    foreach (var result in completedTasks.Select(t => t.Result))
    //                                    {
    //                                        urlList.Add(result.Split(',')[1]);
    //                                        statusList.Add(result.Split(',')[0]);
    //                                    }
    //                                });
    //            completed.Wait();
    //        }
    //        catch (AggregateException ae)
    //        {

    //        }
    //        catch (Exception ex)
    //        {

    //        }

    //        returnUrl = urlList;
    //        status = statusList;
    //        #region // old code sequencial programming
    //        //try
    //        //{
    //        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

    //        //    WebProxy proxy = new WebProxy();
    //        //    proxy.Address = new Uri("http://10.97.32.18:8080");
    //        //    proxy.Credentials = new NetworkCredential("hcltech\\jangir.r", "Hcl@0615");

    //        //    request.Proxy = proxy;
    //        //    request.Timeout = 10000;
    //        //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

    //        //    return ((int)response.StatusCode).ToString();

    //        //}
    //        //catch (System.Net.WebException we)
    //        //{
    //        //    return we.ToString();
    //        //}
    //        //catch (Exception e)
    //        //{
    //        //    return e.ToString();

    //        //}
    //        #endregion

    //    }
    //}
}