﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProductsApp
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableCors();

            config.Routes.MapHttpRoute(
                name: "AlternateRoute",
                //routeTemplate: "api/{controller}/{id}",
                 routeTemplate: "api/Accounts/{action}/{param}",
                defaults: new { controller="Accounts", param = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
               name: "ProjectSubtasksRoute",
                //routeTemplate: "api/{controller}/{id}",
                routeTemplate: "api/ProjectSubtasks/{action}/{data}",
               defaults: new { controller = "ProjectSubtasks", data = RouteParameter.Optional }
           );
            config.Routes.MapHttpRoute(
               name: "AnalyticsRoute",
                //routeTemplate: "api/{controller}/{id}",
                routeTemplate: "api/Analytics/{action}/{param}",
               defaults: new { controller = "Analytics", param = RouteParameter.Optional }
           );
            config.Routes.MapHttpRoute(
               name: "NotificationsRoute",
                //routeTemplate: "api/{controller}/{id}",
                routeTemplate: "api/Notifications/{action}/{param}",
               defaults: new { controller = "Notifications", param = RouteParameter.Optional }
           );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            
           //  config.Routes.MapHttpRoute(
           //    name: "AlternateRoute",
           //     //routeTemplate: "api/{controller}/{id}",
           //     routeTemplate: "api/{controller}/{action}/{param}"
           //    //defaults: new { param = RouteParameter.Optional }
           //);
            
           
        }
    }
}
