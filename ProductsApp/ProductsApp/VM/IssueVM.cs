﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.VM
{
    public class IssueVM
    {
        
            public int id { get; set; }
            public string project_name { get; set; }
            public int project_id { get; set; }
            // public Project project { get; set; }
            public Tracker tracker { get; set; }
            public int status_id { get; set; }
         //   public Priority priority { get; set; }
         //   public Author author { get; set; }
            public int assigned_to_id { get; set; }
            public string subject { get; set; }
            public string description { get; set; }
            public string start_date { get; set; }
            public string due_date { get; set; }
            public int done_ratio { get; set; }
            public double estimated_hours { get; set; }
            //  public List<CustomField> custom_fields { get; set; }
            public int custom_fields_id { get; set; }
            public string custom_fields_value { get; set; }
            public string custom_fields_name { get; set; }
            public string created_on { get; set; }
            public string updated_on { get; set; }
        }
    
}