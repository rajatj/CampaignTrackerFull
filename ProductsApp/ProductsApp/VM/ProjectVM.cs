﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.VM
{
    public class ProjectVM : TimeVM
    {
        public int id { get; set; }
        public string name { get; set; }

        // public string identifier { get; set; }
        // public string description { get; set; }
        // public int status { get; set; }
        //  public List<CustomField> custom_fields { get; set; }
        //    public string created_on { get; set; }
        public string exit_date { get; set; }
        public double project_cost { get; set; }
        //  public string hosting_cost { get; set; }
        public bool burndown { get; set; }
        public string producer_name { get; set; }
        public string market { get; set; }
        public string hosting_platform { get; set; }
        //   public string ec2_url { get; set; }
        public string status { get; set; }
        public double progress { get; set; }
        //public string updated_on { get; set; }
        public Parent parent { get; set; }
        public string live_url { get; set; }
        public string qa_url { get; set; }
        public string development_start_date { get; set; }
        public string launch_date { get; set; }

        //public List<Membership> members { get; set; }

        public string due_date { get; set; }
        public string pending_with { get; set; }
        public string image_url { get; set; }
        
        //public Int64 ProjectCount { get;set; }

    }
}