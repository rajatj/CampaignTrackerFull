﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.Models
{
    public class AssignedTo
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}