﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.Models.Json
{
    public class JsonHelper
    {
        public List<RootObject> listJson { get; set; }

    }
    public class Parent
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class RootObject
    {
        public int id { get; set; }
        public string name { get; set; }
        public string exit_date { get; set; }
        public double project_cost { get; set; }
        public bool burndown { get; set; }
        public string producer_name { get; set; }
        public string market { get; set; }
        public string hosting_platform { get; set; }
        public string status { get; set; }
        public double progress { get; set; }
        public Parent parent { get; set; }
        public string live_url { get; set; }
        public string development_start_date { get; set; }
        public string launch_date { get; set; }
        public double TotalTimeSpent { get; set; }
        public double EstimatedTime { get; set; }
    }
}