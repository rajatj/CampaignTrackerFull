﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.Models
{
    public class Estimate
    {
        public int pro_id { get; set; }
        public double rate { get; set; }
        public double Rate_Dkk_Cost { get; set; }
        public double UX_Wireframes_cost { get; set; }
        public double Development_3D_cost { get; set; }
        public double QA_cost { get; set; }
        public double Setup_Deployment_cost { get; set; }
        public double Out_of_Pocket_cost { get; set; }
        public double Total_cost { get; set; }
        public double Account_Management_cost { get; set; }
        public double Producer_cost { get; set; }
        public double Design_Assistance_cost { get; set; }
        public double Development_cost { get; set; }
        public double Maintenance_cost { get; set; }
        public double Amazon_Hosting_cost { get; set; }
        public double Other_cost { get; set; }
        public double Risk { get; set; }
        public double total { get; set; }
        public string Comment { get; set; }
    }
}