﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.Models
{
    public class Issue
    {
        public int id { get; set; }
        public Project project { get; set; }
        public Tracker tracker { get; set; }
        public Status status { get; set; }
        public int project_id { get; set; }
        public int priority_id { get; set; }
        public int status_id { get; set; }
        public int tracker_id { get; set; }
        public Priority priority { get; set; }
        public Author author { get; set; }
        public AssignedTo assigned_to { get; set; }
        public string subject { get; set; }
        public string description { get; set; }
        public string start_date { get; set; }
        public string due_date { get; set; }
        public int done_ratio { get; set; }
        public double estimated_hours { get; set; }
        public List<CustomField> custom_fields { get; set; }
        public string created_on { get; set; }
        public string updated_on { get; set; }
    }
}