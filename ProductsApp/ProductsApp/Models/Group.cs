﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.Models
{
    public class Group
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<ProductsApp.Models.User> users { get; set; }
        public List<Membership> memberships { get; set; }
    }
}