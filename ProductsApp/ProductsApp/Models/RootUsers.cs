﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.Models
{
    public class RootUsers
    {
        public List<User> users { get; set; }
        public int total_count { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
    }
    public class RootSingleUser
    {
        public User user { get; set; }
    }
    public partial class User
    {
        public string login { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string mail { get; set; }
        public string api_key { get; set; }
        public List<Membership> memberships { get; set; }
    }
}