﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.Models
{
    public class UserDetails
    {
        public int RedmineUserId { get; set; }
        public int RedmineEmail { get; set; }
        public int Username { get; set; }
        public int Password { get; set; }
        public int IsFirstLogin { get; set; }
        public int Token { get; set; }

    }
}