﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.Models
{
    public class Membership
    {
        public int id { get; set; }
        public ProductsApp.Models.Memberships.Project project { get; set; }
        public User user { get; set; }
        public List<Role> roles { get; set; }
    }
   
}
namespace ProductsApp.Models.Memberships
{
    public class Project
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}