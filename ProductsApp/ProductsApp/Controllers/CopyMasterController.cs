﻿using ProductsApp.Models;
using ProductsApp.Services;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Xml;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CopyMasterController : ApiController
    {

        // Get api/copymaster
        public string Get()
        {
            return "value";
        }


        //   GET api/copymaster/154
        //to copy issues from master project to a child project 
        public HttpResponseMessage Get(int id)
        {
            try
            {
                int redmineUserId;
                AccountsServices ac = new AccountsServices();
                string auth = ac.CheckToken(Request);
                if (string.IsNullOrEmpty(auth))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Unauthorized access");
                else if (auth.Contains("Exception"))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authentication error!! Please try again");
                else
                    int.TryParse(auth, out redmineUserId);

                CopyMasterService cms = new CopyMasterService();
                List<Issue> get_issues = cms.GetIssuesFromMaster();
                string result = "Not Copied";
                foreach (var issue in get_issues)
                {
                    //   int childpro_id = 149;
                    CopyFromMasterToChildService CFM = new CopyFromMasterToChildService();
                    result = CFM.Copy(issue, id);
                }
                if (result == "Created")
                {
                    result = "copied";
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
       
        catch (Exception ex)
    {
        return Request.CreateResponse(HttpStatusCode.InternalServerError, "InternalServerError");
    }
        }
        //to create new issue in any project
        public HttpResponseMessage Post(Issue data)
        {
            try
            {
                int redmineUserId;
                AccountsServices ac = new AccountsServices();
                string auth = ac.CheckToken(Request);
                if (string.IsNullOrEmpty(auth))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Unauthorized access");
                else if (auth.Contains("Exception"))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authentication error!! Please try again");
                else
                    int.TryParse(auth, out redmineUserId);

                CreateUpdateIssue cup = new CreateUpdateIssue();
                var result = cup.CreateIssue(data, redmineUserId.ToString());
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "InternalServerError");
            }
            //    return finalString;

        }


        public HttpResponseMessage Put(Issue data)
        {
            try
            {
                int redmineUserId;
                AccountsServices ac = new AccountsServices();
                string auth = ac.CheckToken(Request);
                if (string.IsNullOrEmpty(auth))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Unauthorized access");
                else if (auth.Contains("Exception"))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authentication error!! Please try again");
                else
                    int.TryParse(auth, out redmineUserId);
                if (data.id != 0)
                {
                    CreateUpdateIssue cup = new CreateUpdateIssue();
                    var result = cup.UpdateIssue(data, redmineUserId.ToString());
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "InternalServerError");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "InternalServerError");
            }
        }

       

    }

}
