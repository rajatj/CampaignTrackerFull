﻿using ProductsApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SiteStatusController : ApiController
    {
        // GET api/sitestatus
        public string Get()
        {
            return CheckSiteStatus.StartAsyncHttpResponse();
        }

        // GET api/sitestatus/5
        public string Get(int id)
        {
           
            return "value";
        }

        // POST api/sitestatus
        public void Post([FromBody]string value)
        {
        }

        // PUT api/sitestatus/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/sitestatus/5
        public void Delete(int id)
        {
        }
    }
}
