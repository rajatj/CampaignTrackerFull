﻿using Newtonsoft.Json;
using ProductsApp.Models;
using ProductsApp.Services;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProjectSubtasksController : ApiController
    {
        // GET api/projectsubtasks
        [HttpPost]
        public HttpResponseMessage UpdateProjectDynamic(ProjectVM data)
        {



            string query = @"UPDATE [dbo].[Project_Main] SET "; ;

            List<SqlParameter> list = new List<SqlParameter>();
            SqlParameter param;


            if (data.id != 0)
            {
                //return ;//cant update project without id
                query += @" [Project_Id] = @Project_Id ";
                if (data.name != null)
                {
                    query += @",[Name] = @Name";
                    param = new SqlParameter("@Name", SqlDbType.VarChar) { Value = data.name };
                    list.Add(param);
                }
                if (data.exit_date != null)
                {
                    query += @",[Exit_Date] = @Exit_Date";
                    param = new SqlParameter("@Exit_Date", SqlDbType.Date) { Value = Convert.ToDateTime(data.exit_date).ToString("yyyy-MM-dd") };
                    list.Add(param);
                }
                if (data.project_cost != 0.0)
                {
                    query += @",[Project_Cost] = @Project_Cost";
                    param = new SqlParameter("@Project_Cost", SqlDbType.Decimal) { Value = data.project_cost };
                    list.Add(param);
                }
                if (data.producer_name != null)
                {
                    query += @",[Producer_Name] = @Producer_Name";
                    param = new SqlParameter("@Producer_Name", SqlDbType.VarChar) { Value = data.producer_name };
                    list.Add(param);
                }
                if (data.market != null)
                {
                    query += @",[Market] = @Market";
                    param = new SqlParameter("@Market", SqlDbType.VarChar) { Value = data.market };
                    list.Add(param);
                }
                if (data.hosting_platform != null)
                {
                    query += @",[Hosting_Platform] = @Hosting_Platform";
                    param = new SqlParameter("@Hosting_Platform", SqlDbType.VarChar) { Value = data.hosting_platform };
                    list.Add(param);
                }
                if (data.status != null)
                {
                    query += @",[Phase_Status] = @Phase_Status";
                    param = new SqlParameter("@Phase_Status", SqlDbType.VarChar) { Value = data.status };
                    list.Add(param);
                }
                if (data.progress != 0.0)
                {
                    query += @",[Progress] = @Progress";
                    param = new SqlParameter("@Progress", SqlDbType.Decimal) { Value = data.progress };
                    list.Add(param);
                }
                if (data.live_url != null)
                {
                    query += @",[Live_Url] = @Live_Url";
                    param = new SqlParameter("@Live_Url", SqlDbType.VarChar) { Value = data.live_url };
                    list.Add(param);
                }
                if (data.development_start_date != null)
                {
                    query += @",[Development_Start_Date] = @Development_Start_Date";
                    param = new SqlParameter("@Development_Start_Date", SqlDbType.Date) { Value = Convert.ToDateTime(data.development_start_date).ToString("yyyy-MM-dd") };
                    list.Add(param);
                }
                if (data.launch_date != null)
                {
                    query += @",[Launch_Date] = @Launch_Date";
                    param = new SqlParameter("@Launch_Date", SqlDbType.Date) { Value = Convert.ToDateTime(data.launch_date).ToString("yyyy-MM-dd") };
                    list.Add(param);
                }
                if (data.due_date != null)
                {
                    query += @",[due_date] = @due_date";
                    param = new SqlParameter("@due_date", SqlDbType.Date) { Value = Convert.ToDateTime(data.due_date).ToString("yyyy-MM-dd") };
                    list.Add(param);
                }
                if (data.pending_with != null)
                {
                    query += @",[pending_with] = @pending_with";
                    param = new SqlParameter("@pending_with", SqlDbType.VarChar) { Value = data.pending_with };
                    list.Add(param);
                }
                if (data.image_url != null)
                {
                    query += @",[image_url] = @image_url";
                    param = new SqlParameter("@image_url", SqlDbType.VarChar) { Value = data.image_url };
                    list.Add(param);
                }
                query += @" WHERE [Project_Id] = @Project_Id";
                param = new SqlParameter("@Project_Id", SqlDbType.BigInt) { Value = data.id };
                list.Add(param);
            }

            SqlParameter[] sqlParam = list.ToArray();

            DatabaseConnection dc = new DatabaseConnection();
            dc.InsertUpdateDeleteDynamic(query, sqlParam);

            //int id = 0;
            //string dateFormat = "yyyy-MM-dd";
            //string exit_date = string.Empty;
            //double project_cost = 0.0;
            //string producer_name = string.Empty;
            //string market = string.Empty;
            //string hosting_platform = string.Empty;
            //string status = string.Empty;
            //double progress = 0.0;
            //string live_url = string.Empty;
            //string development_start_date = string.Empty;
            //string launch_date = string.Empty;
            //string post_result = string.Empty;
            //string due_date = string.Empty;
            //string image_url = string.Empty;
            //double estimateTime = 0.0;


            //foreach (PropertyInfo property in properties)
            //{
            //    object propertyValue = property.GetValue(data, null);
            //    Type propType = property.PropertyType;
            //    string propName = property.Name;
            //    switch (propName)
            //    { 
            //        case "id":

            //            break;
            //    }

            //    //if (propType == typeof(int))
            //    //{
            //    //    Int64 interger = Convert.ToInt64(propertyValue);
            //    //    if (interger != 0)
            //    //        listOfProperties += property.Name.ToString() + ",";
            //    //}
            //    //if (propType == typeof(double))
            //    //{
            //    //    double doubleVal = Convert.ToDouble(propertyValue);
            //    //    if (doubleVal != Double.MinValue)
            //    //        listOfProperties += property.Name.ToString() + ",";
            //    //}
            //    //if (propType == typeof(string))
            //    //{
            //    //    string stringVal = Convert.ToString(propertyValue);
            //    //    if (!string.IsNullOrEmpty(stringVal))
            //    //        listOfProperties += property.Name.ToString() + ",";
            //    //}
            //}



            return Request.CreateResponse(HttpStatusCode.OK, "");//"Authentication error!! Please try again");
        }

        // GET api/projectsubtasks/5
        [HttpGet]
        public HttpResponseMessage testP(int data)
        {


            return Request.CreateResponse(HttpStatusCode.OK, "2");//"Authentication error!! Please try again");
        }

        // POST api/projectsubtasks
        public void Post([FromBody]string value)
        {
        }

        // PUT api/projectsubtasks/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/projectsubtasks/5
        public void Delete(int id)
        {
        }
    }
    public class test
    {
        public string id { get; set; }
    }
}
