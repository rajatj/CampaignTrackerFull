﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ProductsApp.Services;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AccountsController : ApiController
    {
        [HttpPost]
        [Route("Login")]
        public HttpResponseMessage Login(ProductsApp.Models.Accounts.Login param)
        {
            try
            {
                string Token = string.Empty;
                AccountsServices accService = new AccountsServices();
                string result = accService.Login(param, out Token);

                List<string> li = new List<string>();
                li.Add(Token);
                li.Add(result);
                if (string.IsNullOrEmpty(Token))
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, li);
                }
                if (result == "Login Successfull")
                {
                    return Request.CreateResponse(HttpStatusCode.OK, li);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new List<string>() { "", "Error" });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new List<string>() { "", "Api error!! Sorry!!" });
            }
        }

        [HttpPost]
        [Route("Logout")]
        public HttpResponseMessage Logout()
        {
            try
            {
                string Token = string.Empty;
                AccountsServices accService = new AccountsServices();
                string result = accService.CheckToken(Request);
                int redmineUserId;
                if (result.Contains("Exception") || string.IsNullOrEmpty(result))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Server Logout Unauthorized");
                else
                {
                    int.TryParse(result, out redmineUserId);
                    string deleteResult = accService.DeleteToken(redmineUserId.ToString());
                    if (deleteResult == "Successfully Logged Out")
                        return Request.CreateResponse(HttpStatusCode.OK, "Successfully Logged Out"); 
                    else
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, deleteResult); 
                }
                
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Sorry!! Server exception"); 
            }
        }

        //// GET api/accounts
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/accounts/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/accounts
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/accounts/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/accounts/5
        //public void Delete(int id)
        //{
        //}
    }
}
