﻿using ProductsApp.Models;
using ProductsApp.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EstimateController : ApiController
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        DatabaseConnection dc = new DatabaseConnection();
        // GET api/estimate
        public HttpResponseMessage Get()
        {
            try
            {
                int redmineUserId;
                AccountsServices ac = new AccountsServices();
                string auth = ac.CheckToken(Request);
                if (string.IsNullOrEmpty(auth))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Unauthorized access");
                else if (auth.Contains("Exception"))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authentication error!! Please try again");
                else
                    int.TryParse(auth, out redmineUserId);

                IssuesPerProject pds = new IssuesPerProject();

                double rate = 0.0;
                int p_id = 386;//estimate management project id
                int t_id = 19;
                int off = 0;
                int lim = 100;
                List<Estimate> get_estimate = new List<Estimate>();
                List<Issue> get_issues = pds.Details(p_id, t_id, off, lim, redmineUserId.ToString());

                foreach (var iss in get_issues)
                {
                    Estimate est = new Estimate();
                    foreach (var cf in iss.custom_fields)
                    {
                        if (cf.value != "")
                        {
                            if (cf.name == "Rate Dkk Cost")
                            { rate = Convert.ToDouble(cf.value); }
                            if (cf.name == "UX (Wireframes)")
                            { est.UX_Wireframes_cost = Convert.ToDouble(cf.value) * rate; }
                            if (cf.name == "3D Development")
                            { est.Development_3D_cost = Convert.ToDouble(cf.value) * rate; }
                            if (cf.name == "QA")
                            { est.QA_cost = Convert.ToDouble(cf.value) * rate; }
                            if (cf.name == "Setup & Deployment")
                            { est.Setup_Deployment_cost = Convert.ToDouble(cf.value) * rate; }
                            if (cf.name == "Out of Pocket")
                            { est.Out_of_Pocket_cost = Convert.ToDouble(cf.value) * rate; }
                            if (cf.name == "Account Management")
                            { est.Account_Management_cost = Convert.ToDouble(cf.value) * rate; }
                            if (cf.name == "Producer")
                            { est.Producer_cost = Convert.ToDouble(cf.value) * rate; }
                            if (cf.name == "Design Assistance")
                            { est.Design_Assistance_cost = Convert.ToDouble(cf.value) * rate; }
                            if (cf.name == "Development")
                            { est.Development_cost = Convert.ToDouble(cf.value) * rate; }
                            if (cf.name == "Maintenance")
                            { est.Maintenance_cost = Convert.ToDouble(cf.value) * rate; }
                            if (cf.name == "Amazon Hosting Cost")
                            { est.Amazon_Hosting_cost = Convert.ToDouble(cf.value) * rate; }
                            if (cf.name == "Other")
                            { est.Other_cost = Convert.ToDouble(cf.value) * rate; }
                            if (cf.name == "Risk")
                            { est.Risk = Convert.ToDouble(cf.value) * rate; }
                            if (cf.name == "Comment")
                            { est.Comment = cf.value; }
                            if (cf.name == "p_id")
                            { est.pro_id = Convert.ToInt32(cf.value); }
                            est.rate = rate;
                        }

                    }
                    est.total = est.UX_Wireframes_cost + est.Development_3D_cost + est.QA_cost + est.Setup_Deployment_cost + est.Out_of_Pocket_cost + est.Account_Management_cost + est.Producer_cost + est.Design_Assistance_cost + est.Development_cost + est.Maintenance_cost + est.Amazon_Hosting_cost + est.Other_cost;
                    get_estimate.Add(est);
                }

                return Request.CreateResponse(HttpStatusCode.OK, get_estimate);
            }
            catch (Exception ex)
            {
                Logger.Error("EstimateController.Get Exception" + ex.ToString());
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Exception Occuered While Retrieving Data");
            }
        }

        // GET api/estimate/5
        public HttpResponseMessage Get(string pid_Id , int pid_Value)
        {
            try
            {
                int redmineUserId;
                AccountsServices ac = new AccountsServices();
                string auth = ac.CheckToken(Request);
                if (string.IsNullOrEmpty(auth))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Unauthorized access");
                else if (auth.Contains("Exception"))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authentication error!! Please try again");
                else
                    int.TryParse(auth, out redmineUserId);

                string p_Value = pid_Value.ToString();
                RootObject root = new RootObject();
                List<Issue> issues_list = new List<Issue>();
                string str = "";
                string getUserApiKey = "Select [RedmineApiKey] from [Users] where [RedmineUserId] = '" + redmineUserId + "'";
                string UserApiKey = dc.GetSingleCell(getUserApiKey);
                string url = ConfigurationManager.AppSettings["hostUrl"] + "projects/estimate-management/issues.json?utf8=%E2%9C%93&set_filter=1&f%5B%5D=cf_" + pid_Id + "&op%5Bcf_" + pid_Id + "%5D=%3D&v%5Bcf_" + pid_Id + "%5D%5B%5D=" + p_Value + "&f%5B%5D=&c%5B%5D=tracker&c%5B%5D=due_date&c%5B%5D=status&c%5B%5D=priority&c%5B%5D=subject&c%5B%5D=assigned_to&c%5B%5D=updated_on&c%5B%5D=estimated_hours&c%5B%5D=author&group_by=&key=" + UserApiKey;  //+ ConfigurationManager.AppSettings["apiKey"];
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    str = reader.ReadToEnd();
                    root = (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);
                    issues_list = root.issues;
                }
                return Request.CreateResponse(HttpStatusCode.OK, issues_list);
            }
            catch (Exception ex)
            {
                Logger.Error("EstimateController.Get.Parameterized Exception" + ex.ToString());
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Exception Occuered While Retrieving Data");
            }
        }

        // POST api/estimate
        public HttpResponseMessage Post(Issue data)
        {
            try
            {
                int redmineUserId;
                AccountsServices ac = new AccountsServices();
                string auth = ac.CheckToken(Request);
                if (string.IsNullOrEmpty(auth))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Unauthorized access");
                else if (auth.Contains("Exception"))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authentication error!! Please try again");
                else
                    int.TryParse(auth, out redmineUserId);

                CreateUpdateIssue cup = new CreateUpdateIssue();
                var result = cup.CreateIssue(data, redmineUserId.ToString());
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                Logger.Error("EstimateController.Post Exception" + ex.ToString());
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "InternalServerError");
            }
            //    return finalString;

        }

        // PUT api/estimate/5
        public HttpResponseMessage Put(Issue data)
        {
            try
            {
                int redmineUserId;
                AccountsServices ac = new AccountsServices();
                string auth = ac.CheckToken(Request);
                if (string.IsNullOrEmpty(auth))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Unauthorized access");
                else if (auth.Contains("Exception"))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authentication error!! Please try again");
                else
                    int.TryParse(auth, out redmineUserId);
                if (data.id != 0)
                {
                    CreateUpdateIssue cup = new CreateUpdateIssue();
                    var result = cup.UpdateIssue(data, redmineUserId.ToString());
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "InternalServerError");
            }
            catch (Exception ex)
            {
                Logger.Error("EstimateController.Put Exception" + ex.ToString());
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "InternalServerError");
            }
        }

        // DELETE api/estimate/5
        public void Delete(int id)
        {
        }
    }
}
