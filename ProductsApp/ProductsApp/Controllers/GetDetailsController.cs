﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class GetDetailsController : ApiController
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        // GET api/getdetails
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/getdetails/project for more section
        public HttpResponseMessage GetDetails(int id)
        {
           
            try
            {
                RootObject root = new RootObject();
                Project proj = new Project();
                List<Project> pro_list = new List<Project>();
                string str = "";
                string url = ConfigurationManager.AppSettings["hostUrl"] + "projects/" + id + ".json?limit=100&key=" + ConfigurationManager.AppSettings["apiKey"];
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    str = reader.ReadToEnd();

                    root = (RootObject)Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);
                    proj = root.project;
                }
               
                return Request.CreateResponse(HttpStatusCode.OK, proj); 
            }
                catch(WebException we)
            {
                Logger.Error("GetDetailsController.GetDetails: " + we.ToString());
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Redmine Server Not Responding");
            }
            catch(Exception ex)
            {
                Logger.Error("GetDetailsController.GetDetails: " + ex.ToString());
                //return null;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Api Exception Occured");
            }
        }

        // POST api/getdetails
        public void Post([FromBody]string value)
        {
        }

        // PUT api/getdetails/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/getdetails/5
        public void Delete(int id)
        {
        }
    }
}
