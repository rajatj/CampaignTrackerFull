﻿using Newtonsoft.Json;
using ProductsApp.Services;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class testController : ApiController
    {
        //private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        // GET api/test
        public HttpResponseMessage Get()
        {
            AccountsServices ac = new AccountsServices();
            string auth = ac.CheckToken(Request);
            if(string.IsNullOrEmpty(auth))
                return Request.CreateResponse(HttpStatusCode.Unauthorized, "Unauthorized access");
            else if (auth.Contains("Exception"))
                return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authentication error!! Please try again");
            else
                return Request.CreateResponse(HttpStatusCode.OK, auth);
            //SqlQueries.Project_Main_Tbl sq = new SqlQueries.Project_Main_Tbl();
            //DataTable dt = sq.CampaignTabs("Live");
            //string returnString = JsonConvert.SerializeObject(dt);
            //return returnString;

            //return CheckSiteStatus.StartAsyncHttpResponse();

            //Logger.Trace("Tracing error");

            return Request.CreateResponse(HttpStatusCode.OK, "");
        }

        // GET api/test/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/test
        public void Post([FromBody]string value)
        {
        }

        // PUT api/test/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/test/5
        public void Delete(int id)
        {
        }
    }
}
