﻿using ProductsApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class JsonToSqlSyncController : ApiController
    {
        // GET api/jsontosqlsync
        public List<string> Get()
        {
            JsonToSQL jts = new JsonToSQL();
            return jts.SyncProjects();
        }

        // GET api/jsontosqlsync/5
        public List<string> Get(int id)
        {
            return JsonToSQL.SyncUsers();
        }

        // POST api/jsontosqlsync
        public void Post([FromBody]string value)
        {
        }

        // PUT api/jsontosqlsync/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/jsontosqlsync/5
        public void Delete(int id)
        {
        }
    }
}
