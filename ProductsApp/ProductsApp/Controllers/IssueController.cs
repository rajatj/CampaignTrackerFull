﻿using ProductsApp.Services;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class IssueController : ApiController
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        // GET api/issue
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/issue/5
        public HttpResponseMessage get(int id)
        {
            try
            {
                int redmineUserId;
                AccountsServices ac = new AccountsServices();
                string auth = ac.CheckToken(Request);
                if (string.IsNullOrEmpty(auth))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Unauthorized access");
                else if (auth.Contains("Exception"))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authentication error!! Please try again");
                else
                    int.TryParse(auth, out redmineUserId);
                IssuesPerProject pds = new IssuesPerProject();
               
                Return_list get_issues = pds.IssueDetails(id, redmineUserId.ToString());
                if (get_issues != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, get_issues); 
                }
                else
                {
                    Logger.Debug("IssueDetailPerProjectController.get: No issue found" );
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Internal Server Error"); 
                }
            }
            catch (Exception ex)
            {
                Logger.Error("IssueDetailPerProjectController.get: " + ex.ToString());
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Internal Server Error"); 
            }
        }

        // POST api/issue
        public void Post([FromBody]string value)
        {
        }

        // PUT api/issue/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/issue/5
        public void Delete(int id)
        {
        }
    }
}
