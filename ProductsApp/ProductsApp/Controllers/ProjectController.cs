﻿using ProductsApp.Models;
using ProductsApp.Services;
using ProductsApp.VM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;
using System.Xml;

namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProjectController : ApiController
    {
        // GET api/project
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public List<ProjectVM> GetTime()
        {

            ProjectService PS = new ProjectService();
            var result = PS.GetAllProjects();
            if (result == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return result;

        }


        // GET api/project/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/project
        public HttpResponseMessage Post(Project data)
        {
            try
            {
                int redmineUserId;
                AccountsServices ac = new AccountsServices();
                string auth = ac.CheckToken(Request);
                if (string.IsNullOrEmpty(auth))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Unauthorized access");
                else if (auth.Contains("Exception"))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authentication error!! Please try again");
                else
                    int.TryParse(auth, out redmineUserId);


                string result = CreateUpdateCampaign.CreateUpdate(data, redmineUserId.ToString());
                if (result == "Success")
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Campaign Created Successfully");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Sorry Exception Occured!!");
            }
        }




        // PUT api/project/5
        public void Put(Project data)
        {
            //    try
            //    {
            //        string reuesturi = System.Configuration.ConfigurationManager.AppSettings["hostUrl"] + "projects.xml?key=" + System.Configuration.ConfigurationManager.AppSettings["apiKey"];

            //        AddProService aps = new AddProService();

            //        XmlDocument xmlDoc = new XmlDocument();
            //        XmlNode rootNode = xmlDoc.CreateElement("project");
            //        xmlDoc.AppendChild(rootNode);

            //        XmlNode idNode = xmlDoc.CreateElement("id");
            //        idNode.InnerText = (data.id).ToString();
            //        rootNode.AppendChild(idNode);

            //        //XmlNode response_putNode = xmlDoc.CreateElement("response_put");
            //        //response_putNode.InnerText = (data.response_put).ToString();
            //        //rootNode.AppendChild(response_putNode);

            //        XmlNode nameNode = xmlDoc.CreateElement("name");
            //        nameNode.InnerText = data.name;
            //        rootNode.AppendChild(nameNode);

            //        //XmlNode identifierNode = xmlDoc.CreateElement("identifier");
            //        //identifierNode.InnerText = data.identifier;
            //        //rootNode.AppendChild(identifierNode);

            //        //XmlNode ParentNode = xmlDoc.CreateElement("parent");
            //        //XmlAttribute attribute_parent = xmlDoc.CreateAttribute("id");
            //        //attribute_parent.Value = data.parent.id.ToString();
            //        //ParentNode.Attributes.Append(attribute_parent);

            //        //XmlNode ParentNode = xmlDoc.CreateElement("parent_id");
            //        //ParentNode.InnerText = data.parent_id;
            //        //rootNode.AppendChild(ParentNode);

            //        //XmlAttribute attribute_parent1 = xmlDoc.CreateAttribute("name");
            //        //attribute_parent1.Value = data.parent.name;
            //        //ParentNode.Attributes.Append(attribute_parent1);


            //        //   rootNode.AppendChild(ParentNode);

            //        //XmlNode moduleNode = xmlDoc.CreateElement("enabled_module_names");
            //        //moduleNode.InnerText = "issue_tracking";
            //        //rootNode.AppendChild(moduleNode);


            //        //XmlNode moduleNode1 = xmlDoc.CreateElement("enabled_module_names");
            //        //moduleNode1.InnerText = "time_tracking";
            //        //rootNode.AppendChild(moduleNode1);

            //        //XmlNode trackerNode = xmlDoc.CreateElement("tracker_ids");
            //        //trackerNode.InnerText = "1";
            //        //rootNode.AppendChild(trackerNode);

            //        //XmlNode trackerNode1 = xmlDoc.CreateElement("tracker_ids");
            //        //trackerNode1.InnerText = "2";
            //        //rootNode.AppendChild(trackerNode1);

            //        //string[] module = ConfigurationManager.AppSettings["module"].Split(';');
            //        //string[] tracking = ConfigurationManager.AppSettings["tracking"].Split(';');

            //        ////  int i = module.Length;
            //        //for (int i = 0; i < module.Length; i++)
            //        //{
            //        //    XmlNode moduleNode = xmlDoc.CreateElement("enabled_module_names");
            //        //    moduleNode.InnerText = module[i];
            //        //    rootNode.AppendChild(moduleNode);
            //        //}

            //        //for (int j = 0; j < tracking.Length; j++)
            //        //{
            //        //    XmlNode trackerNode = xmlDoc.CreateElement("tracker_ids");
            //        //    trackerNode.InnerText = tracking[j];
            //        //    rootNode.AppendChild(trackerNode);
            //        //    }

            //        XmlNode custom_fieldsNode = xmlDoc.CreateElement("custom_fields");
            //        XmlAttribute attribute = xmlDoc.CreateAttribute("type");
            //        attribute.Value = "array";
            //        custom_fieldsNode.Attributes.Append(attribute);

            //        foreach (var v in data.custom_fields)
            //        {

            //            XmlNode custom_fieldNode = xmlDoc.CreateElement("custom_field");

            //            XmlAttribute attribute1 = xmlDoc.CreateAttribute("id");
            //            attribute1.Value = v.id.ToString();
            //            custom_fieldNode.Attributes.Append(attribute1);

            //            XmlAttribute attribute2 = xmlDoc.CreateAttribute("name");
            //            attribute2.Value = v.name;
            //            custom_fieldNode.Attributes.Append(attribute2);

            //            XmlNode valueNode = xmlDoc.CreateElement("value");
            //            valueNode.InnerText = v.value;
            //            custom_fieldNode.AppendChild(valueNode);

            //            custom_fieldsNode.AppendChild(custom_fieldNode);
            //        }

            //        rootNode.AppendChild(custom_fieldsNode);

            //        string XmlizedString = "";
            //        using (StringWriter sw = new StringWriter())
            //        {
            //            using (XmlTextWriter tx = new XmlTextWriter(sw))
            //            {
            //                xmlDoc.WriteTo(tx);
            //                XmlizedString = sw.ToString();
            //            }
            //        }
            //        //  String XmlizedString = TestProduct.xmlDocToString(xmlDoc);


            //        String finalString = XmlizedString.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
            //        //    string addedstr = "<enabled_module_names>time_tracking</enabled_module_names>";
            //        //    string newfinalstr = finalString + addedstr;
            //        //     string final2 = "<project><id>0</id><name>test2</name><identifier>test2-37587</identifier><parent_id>23</parent_id><custom_fields type=\"array\"><custom_field id=\"2\" name=\"Launch Date\"><value>2015-05-11</value></custom_field><custom_field id=\"3\" name=\"Estimates Shared with Client\"><value>123</value></custom_field><custom_field id=\"23\" name=\"Market\"><value>vdsv</value></custom_field><custom_field id=\"17\" name=\"Project Technology\"><value>PimCore</value></custom_field><custom_field id=\"28\" name=\"Producer\"><value>vgv</value></custom_field><custom_field id=\"31\" name=\"Live URL\"><value>ffdd</value></custom_field><custom_field id=\"16\" name=\"Project Type\"><value>Campaign Factory</value></custom_field></custom_fields<enabled_module_names>agile</enabled_module_names></project>";
            //        if (data.response_put == false)
            //        {
            //            //aps.postXMLData(reuesturi, finalString);
            //        }
            //        else
            //        {
            //            int idchange = data.id;
            //            //    string reuesturi_put = "http://10.97.85.87/redmine/projects/" + idchange + ".xml?key=a45d28736e6c5b578afd55bc8ddd5568d705834f";
            //            string requesturi_put = ConfigurationManager.AppSettings["hostUrl"] + "projects/" + idchange + ".xml?key=" + ConfigurationManager.AppSettings["apiKey"];
            //            //aps.putXMLData(requesturi_put, finalString);
            //        }
            //    }
            //    catch
            //    {
            //    }
        }

        // DELETE api/project/5
        public void Delete(int id)
        {
        }
    }
}
