﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
namespace ProductsApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DoubleTestController : ApiController
    {
        DoubleTest[] products = new DoubleTest[] 
        { 
            new DoubleTest { Id = 1, Name = "india", Category = "Groceries", Price = 1 }, 
            new DoubleTest { Id = 2, Name = "usa", Category = "Toys", Price = 3.75M }, 
            new DoubleTest { Id = 3, Name = "Hammer", Category = "Hardware", Price = 16.99M } 
        };

        public IEnumerable<DoubleTest> GetAllProducts()
        {
            return products;
        }

        public IHttpActionResult GetProduct(int id)
        {
            var product = products.FirstOrDefault((p) => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }

    }
}
