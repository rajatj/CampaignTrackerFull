﻿using ProductsApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProductsApp.Controllers
{
    public class NotificationsController : ApiController
    {
        [HttpGet]
        // GET api/notifications
        public HttpResponseMessage DailyNotifications()
        {
            EmailService emailService = new EmailService();
            bool result = emailService.DailyMailNotifications();
            if (result)
                return Request.CreateResponse(HttpStatusCode.OK , true);
            else
                return Request.CreateResponse(HttpStatusCode.InternalServerError , false);
        }

        [HttpGet]
        // GET api/notifications
        public HttpResponseMessage ExecuteMailQueue()
        {
            EmailService emailService = new EmailService();
            bool result = emailService.ExecuteMailQueue();
            if (result)
                return Request.CreateResponse(HttpStatusCode.OK, true);
            else
                return Request.CreateResponse(HttpStatusCode.InternalServerError, false);
        }

        [HttpPost]
        // GET api/notifications
        public HttpResponseMessage SetMailQueue(string body, string subject, List<string> mailTo)
        {
            EmailService emailService = new EmailService();
            bool result = emailService.SetMailQueue(body , subject , mailTo);
            if (result)
                return Request.CreateResponse(HttpStatusCode.OK, true);
            else
                return Request.CreateResponse(HttpStatusCode.InternalServerError, false);
        }
        // GET api/notifications/5
        public string Get(int param)
        {
            return "value";
        }

        // POST api/notifications
        public void Post([FromBody]string value)
        {
        }

        // PUT api/notifications/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/notifications/5
        public void Delete(int id)
        {
        }
    }
}
